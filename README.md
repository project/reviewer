# Reviewer

Reviewer automates reviews of Drupal configuration to help developers catch 
build issues and decrease the time spent checking configuration forms.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/reviewer).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/reviewer).

## Requirements

Reviewer requires [Drush 12](https://www.drush.org) to run reviews.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Reviewer requires no configuration. It is recommended to enable reviewer on
development environments only using
[Configuration Split](https://www.drupal.org/project/config_split).

## Usage

Reviews can be run through Drush or reviewed at `/admin/reports/reviewer` when
the `reviewer_ui` submodule is enabled.

Reviewer provides the following Drush commands:

- `reviewer:list` 
- `reviewer:run`

Use the `--help` flag to see available options.

Enable the `reviewer_test` submodule to install test content and reviews to
try out Reviewer!

### reviewer:list

Lists all available reviews.

### reviewer:run

Runs reviews. By default, running reviews will output all failures (tests which 
did not pass) and errors (tests which threw an exception).

You may specify specific reviews to run by their ID. For example, 
`reviewer:run test_node test_taxonomy` runs the "Test Node" and 
"Test Taxonomy" reviews. For reviews that support bundles you can specify
specific bundles to run. `reviewer:run test_node:test_fail` runs the "Test Node" 
review on the "test_fail" content type only.

#### Options

- `--review`: Interactively review, prompting to ignore failures and errors.
- `--review-new`: Interactively review, prompting to ignore new failures and 
  errors only.
- `--review-reset`: Interactively review, prompting to ignore all failures and 
  errors on the reviews run. Any previously ignored items will be forgotten and
  will need to be ignored again.
- `--show-passed`: Display passed tests in addition to failures and errors.
- `--show-not-run`: Display tests which were not run in addition to failures 
  and errors.
- `--show-ignored`: Display ignored tests in addition to failures and errors.
- `--show-all`: Display ignored and passed tests in addition to failures and 
  errors.
- `--window-width`: Set the window width to constrain the output table size.
  Defaults to 80.

## Creating Reviews, Checklists, and Tasks

Reviews are plugins which represent a logical grouping of items to review. For
example, a `node` review which checks configuration of all node bundles, or a
`site_settings` review which checks various `system.*` configuration.

Each review must reference at least one checklist plugin. Checklists are 
smaller groupings of tasks which can be reused across reviews. For example, a 
`search_index` checklist which ensures that the search index display mode is
configured and that it has all field labels set to hidden. The `search_index`
checklist is then added to the `node`, `taxonomy_term`, and `paragraph` reviews,
checking that search index view modes are configured correctly across the three
entity types.

Checklists contain any number of tasks. Tasks are the individual items run in 
each checklist. Tasks contain a closure check which must return either a boolean
(for single results), an array of booleans, or null if the test was not run.

The `reviewer_test` module has examples of review and checklists plugins. 

### Reviews

Create review plugins in the `\Drupal\my_module\Plugin\reviewer\Review`
namespace. The `\Drupal\reviewer\Attribute\Review` attribute is used to
configure reviews.

The review attribute allows for configuring the review in the following ways:

- `checklists`: An array containing checklist IDs to be run by the review. This
  attribute value is required.
- `entity_id`: A configuration entity type ID to load and pass to the task check
  closures.
- `entity_type`: An entity type to load and pass to the task check closures. If
  the entity type supports bundles then a review will be created for every 
  bundle.
- `bundles`: If the review specifies an entity type which supports bundles then
  only bundles specified here will be reviewed. Leave blank to review all 
  bundles.
- `configuration`: An array of configuration IDs to load during the review. IDs
  can be either simple config or configuration entity IDs. The configuration is
  passed to task check closures.
- `ignored`: An multidimensional array of result IDs, keyed by `id`, and 
  reasons, keyed by `reason` to permanently ignore during the running of the 
  review.

Here is an example of a review which loads `system.site` configuration and runs
the `example_checklist` while ignoring the front page result:

```php
#[Review(
  id: 'example_review',
  label: 'Example Review',
  checklists: [
    'example_checklist',
  ],
  configuration: [
    'system.site',
  ],
  ignored: [
    [
      'id' => 'example_review.example_checklist.system_pages.front',
      'reason' => 'Reason for ignoring the frontpage check.',
    ],
  ],
)]
final class TestSimpleConfigurationReview extends ReviewBase {

}
```

The following is an example of a review which runs for only the article node 
type:

```php
#[Review(
  id: 'example_node_review',
  label: 'Example Node Review',
  checklists: [
    'example_node_checklist',
  ],
  entity_type: 'node_type',
  bundles: [
    'article',
  ]
)]
final class TestSimpleConfigurationReview extends ReviewBase {

}
```

### Checklists & Tasks

Create checklist plugins in the `\Drupal\my_module\Plugin\reviewer\Checklist`
namespace. The `\Drupal\reviewer\Attribute\Checklist` attribute is used to
configure checklists. Every checklist must implement the 
`\Drupal\reviewer\Reviewer\Task\TaskProviderInterface::tasks` method, which
returns an array of `\Drupal\reviewer\Reviewer\Task\TaskInterface` objects. Use
the `\Drupal\reviewer\Plugin\reviewer\Checklist\ChecklistBase::$taskFactory`
service in the checklist plugin to ensure tasks are created with the needed
dependencies.

The task factory take three arguments to create a task:
1. A unique task ID.
2. The closure which runs the task checks.
3. A message to display if the check returns a failure. The message may contain
   the `@failures` placeholder which, if the task returns an array, will be 
   replaced by a comma separated list of the array keys whose value (result) is
   false.

Every task is passed one parameter of the type
`\Drupal\reviewer\Reviewer\Task\TaskConfigInterface` which contains 
configuration loaded by the review.

Here is an example of a checklist which checks that nodes are set for the 403,
404, and front page fields on `/admin/config/system/site-information`:

```php
#[Checklist(id: 'example_checklist', label: 'Example Checklist')]
final class ExampleChecklist extends ChecklistBase {

  /**
   * {@inheritdoc}
   */
  public function tasks(): array {
    return [
      $this->taskFactory->create(
        'system_pages',
         $this->systemPages(...),
        'Nodes are not set for system pages: @failures',
      ),
    ];
  }

  /**
   * Test that the 403, 404, and front pages are set.
   * 
   * @return array<string, bool>|true|null
   *
   * @throws \ErrorException
   */
  private function systemPages(TaskConfigInterface $config): array|true|null {
    // If the node module does not exist, do not run this test.
    if (!\Drupal::moduleHandler()->moduleExists('node')) {
      return NULL;
    }
    
    $config = $config->getConfigOfTypeImmutableById('system.site');
    $results = [];
    foreach (['403', '404', 'front'] as $page) {
      if (!$config->get("page.$page")) {
        $results[$page] = FALSE;
      }
    }
    return $results ?: TRUE;
  }

}
```

## Maintainers

Current maintainer:
- [Benjamin Baird (benabaird)](https://www.drupal.org/u/benabaird)
