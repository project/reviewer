<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Unit\Status;

use Drupal\reviewer\Reviewer\Status\Status;
use Drupal\reviewer\Reviewer\Status\StatusFactory;
use Drupal\reviewer\Reviewer\Status\StatusFactoryInterface;
use Drupal\Tests\reviewer\Traits\MockResultTrait;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the status factory.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Status\StatusFactory
 *
 * @group reviewer
 */
final class StatusFactoryTest extends UnitTestCase {

  use MockResultTrait;

  private readonly StatusFactoryInterface $statusFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->statusFactory = new StatusFactory();
  }

  /**
   * Test ignoring statuses.
   *
   * @covers ::createIgnored
   */
  public function testCreateIgnored(): void {
    self::assertEquals(
      Status::IgnoredFailure,
      $this->statusFactory->createIgnored(Status::Fail),
    );
    self::assertEquals(
      Status::IgnoredError,
      $this->statusFactory->createIgnored(Status::Error),
    );

    self::assertEquals(
      Status::IgnoredFailure,
      $this->statusFactory->createIgnored(Status::IgnoredFailure),
    );
    self::assertEquals(
      Status::IgnoredError,
      $this->statusFactory->createIgnored(Status::IgnoredError),
    );
    self::assertEquals(
      Status::NotRun,
      $this->statusFactory->createIgnored(Status::NotRun),
    );
    self::assertEquals(
      Status::Pass,
      $this->statusFactory->createIgnored(Status::Pass),
    );
  }

  /**
   * Test unignoring statuses.
   *
   * @covers ::createUnignored
   */
  public function testCreateUnignored(): void {
    self::assertEquals(
      Status::Fail,
      $this->statusFactory->createUnignored(Status::IgnoredFailure),
    );
    self::assertEquals(
      Status::Error,
      $this->statusFactory->createUnignored(Status::IgnoredError),
    );

    self::assertEquals(
      Status::Fail,
      $this->statusFactory->createUnignored(Status::Fail),
    );
    self::assertEquals(
      Status::Error,
      $this->statusFactory->createUnignored(Status::Error),
    );
    self::assertEquals(
      Status::NotRun,
      $this->statusFactory->createUnignored(Status::NotRun),
    );
    self::assertEquals(
      Status::Pass,
      $this->statusFactory->createUnignored(Status::Pass),
    );
  }

  /**
   * Test creating an error status.
   *
   * @covers ::createError
   */
  public function testCreateError(): void {
    self::assertEquals(Status::Error, $this->statusFactory->createError());
  }

  /**
   * Test creating a status from a boolean.
   *
   * @covers ::createFromBool
   */
  public function testCreateFromBool(): void {
    self::assertEquals(Status::Fail, $this->statusFactory->createFromBool(FALSE));
    self::assertEquals(Status::Pass, $this->statusFactory->createFromBool(TRUE));
  }

  /**
   * Test creating the most severe status from an array of results.
   *
   * @covers ::createMostSevere
   */
  public function testCreateMostSevere(): void {
    self::assertEquals(
      Status::NotRun,
      $this->statusFactory->createMostSevere([
        $this->mockResult(status: Status::NotRun),
      ]),
    );

    self::assertEquals(
      Status::Pass,
      $this->statusFactory->createMostSevere([
        $this->mockResult(status: Status::Pass),
        $this->mockResult(status: Status::Pass),
      ]),
    );

    self::assertEquals(
      Status::IgnoredFailure,
      $this->statusFactory->createMostSevere([
        $this->mockResult(status: Status::Pass),
        $this->mockResult(status: Status::Pass),
        $this->mockResult(status: Status::IgnoredFailure),
        $this->mockResult(status: Status::IgnoredFailure),
        $this->mockResult(status: Status::NotRun),
        $this->mockResult(status: Status::Pass),
        $this->mockResult(status: Status::Pass),
      ]),
    );

    self::assertEquals(
      Status::IgnoredError,
      $this->statusFactory->createMostSevere([
        $this->mockResult(status: Status::Pass),
        $this->mockResult(status: Status::IgnoredError),
        $this->mockResult(status: Status::IgnoredFailure),
        $this->mockResult(status: Status::IgnoredFailure),
        $this->mockResult(status: Status::NotRun),
        $this->mockResult(status: Status::Pass),
        $this->mockResult(status: Status::Pass),
      ]),
    );

    self::assertEquals(
      Status::Fail,
      $this->statusFactory->createMostSevere([
        $this->mockResult(status: Status::Fail),
        $this->mockResult(status: Status::IgnoredError),
        $this->mockResult(status: Status::IgnoredFailure),
        $this->mockResult(status: Status::IgnoredFailure),
        $this->mockResult(status: Status::NotRun),
        $this->mockResult(status: Status::Pass),
        $this->mockResult(status: Status::Pass),
      ]),
    );

    self::assertEquals(
      Status::Error,
      $this->statusFactory->createMostSevere([
        $this->mockResult(status: Status::Error),
        $this->mockResult(status: Status::Fail),
        $this->mockResult(status: Status::IgnoredError),
        $this->mockResult(status: Status::IgnoredFailure),
        $this->mockResult(status: Status::NotRun),
        $this->mockResult(status: Status::Pass),
        $this->mockResult(status: Status::Pass),
      ]),
    );
  }

}
