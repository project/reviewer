<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Unit\Status;

use Drupal\reviewer\Reviewer\Status\Status;
use Drupal\reviewer\Reviewer\Status\StatusEvaluator;
use Drupal\reviewer\Reviewer\Status\StatusEvaluatorInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the status evaluator.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Status\StatusEvaluator
 *
 * @group reviewer
 */
final class StatusEvaluatorTest extends UnitTestCase {

  private readonly StatusEvaluatorInterface $statusEvaluator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->statusEvaluator = new StatusEvaluator();
  }

  /**
   * Test checking a status is a failure.
   *
   * @covers ::isFailure
   */
  public function testIsFailure(): void {
    self::assertTrue($this->statusEvaluator->isFailure(Status::Fail));
    self::assertTrue($this->statusEvaluator->isFailure(Status::IgnoredFailure));
    self::assertNotTrue($this->statusEvaluator->isFailure(Status::Error));
    self::assertNotTrue($this->statusEvaluator->isFailure(Status::IgnoredError));
    self::assertNotTrue($this->statusEvaluator->isFailure(Status::NotRun));
    self::assertNotTrue($this->statusEvaluator->isFailure(Status::Pass));
  }

  /**
   * Test checking a status is an error.
   *
   * @covers ::isError
   */
  public function testIsError(): void {
    self::assertTrue($this->statusEvaluator->isError(Status::Error));
    self::assertTrue($this->statusEvaluator->isError(Status::IgnoredError));
    self::assertNotTrue($this->statusEvaluator->isError(Status::Fail));
    self::assertNotTrue($this->statusEvaluator->isError(Status::IgnoredFailure));
    self::assertNotTrue($this->statusEvaluator->isError(Status::NotRun));
    self::assertNotTrue($this->statusEvaluator->isError(Status::Pass));
  }

  /**
   * Test checking a status is ignored.
   *
   * @covers ::isIgnored
   */
  public function testIsIgnored(): void {
    self::assertTrue($this->statusEvaluator->isIgnored(Status::IgnoredFailure));
    self::assertTrue($this->statusEvaluator->isIgnored(Status::IgnoredError));
    self::assertNotTrue($this->statusEvaluator->isIgnored(Status::Fail));
    self::assertNotTrue($this->statusEvaluator->isIgnored(Status::Error));
    self::assertNotTrue($this->statusEvaluator->isIgnored(Status::NotRun));
    self::assertNotTrue($this->statusEvaluator->isIgnored(Status::Pass));
  }

  /**
   * Test checking a status is not run.
   *
   * @covers ::isNotRun
   */
  public function testIsNotRun(): void {
    self::assertTrue($this->statusEvaluator->isNotRun(Status::NotRun));
    self::assertNotTrue($this->statusEvaluator->isNotRun(Status::Fail));
    self::assertNotTrue($this->statusEvaluator->isNotRun(Status::IgnoredFailure));
    self::assertNotTrue($this->statusEvaluator->isNotRun(Status::Error));
    self::assertNotTrue($this->statusEvaluator->isNotRun(Status::IgnoredError));
    self::assertNotTrue($this->statusEvaluator->isNotRun(Status::Pass));
  }

  /**
   * Test checking a status is a pass.
   *
   * @covers ::isPass
   */
  public function testIsPass(): void {
    self::assertTrue($this->statusEvaluator->isPass(Status::Pass));
    self::assertNotTrue($this->statusEvaluator->isPass(Status::Fail));
    self::assertNotTrue($this->statusEvaluator->isPass(Status::IgnoredFailure));
    self::assertNotTrue($this->statusEvaluator->isPass(Status::Error));
    self::assertNotTrue($this->statusEvaluator->isPass(Status::IgnoredError));
    self::assertNotTrue($this->statusEvaluator->isPass(Status::NotRun));
  }

}
