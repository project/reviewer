<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Unit\Task;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\reviewer\Reviewer\Task\TaskConfig;
use Drupal\Tests\UnitTestCase;

/**
 * Test config value object methods.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Task\TaskConfig
 *
 * @group reviewer
 */
final class TaskConfigTest extends UnitTestCase {

  private readonly ImmutableConfig $one;

  private readonly ImmutableConfig $two;

  private readonly ConfigEntityInterface $entity;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->one = $this
      ->getMockBuilder(ImmutableConfig::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->two = $this
      ->getMockBuilder(ImmutableConfig::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->entity = $this
      ->getMockBuilder(ConfigEntityInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
  }

  /**
   * Test getting config.
   *
   * @covers ::getConfig
   */
  public function testGetConfig(): void {
    $configuration = [
      'one' => $this->one,
      'two' => $this->two,
      'entity' => $this->entity,
    ];
    $config = new TaskConfig($configuration);
    self::assertSame($configuration, $config->getConfig());
  }

  /**
   * Test getting only immutable config.
   *
   * @covers ::getConfigOfTypeImmutable
   */
  public function testGetConfigOfTypeImmutable(): void {
    $configuration = [
      'one' => $this->one,
      'two' => $this->two,
      'entity' => $this->entity,
    ];
    $config = new TaskConfig($configuration);
    self::assertSame(
      ['one' => $this->one, 'two' => $this->two],
      $config->getConfigOfTypeImmutable(),
    );
  }

  /**
   * Test getting only config entities.
   *
   * @covers ::getConfigOfTypeEntity
   */
  public function testGetConfigOfTypeEntity(): void {
    $configuration = [
      'one' => $this->one,
      'two' => $this->two,
      'entity' => $this->entity,
    ];
    $config = new TaskConfig($configuration);
    self::assertSame(
      ['entity' => $this->entity],
      $config->getConfigOfTypeEntity(),
    );
  }

  /**
   * Test getting config by ID.
   *
   * @covers ::getConfigById
   *
   * @throws \ErrorException
   */
  public function testGetConfigById(): void {
    $configuration = [
      'one' => $this->one,
      'two' => $this->two,
      'entity' => $this->entity,
    ];
    $config = new TaskConfig($configuration);
    self::assertSame($configuration['one'], $config->getConfigById('one'));
    self::assertSame($configuration['entity'], $config->getConfigById('entity'));
    self::expectException(\ErrorException::class);
    $config->getConfigById('three');
  }

  /**
   * Test getting immutable config by ID.
   *
   * @covers ::getConfigOfTypeImmutableById
   *
   * @throws \ErrorException
   */
  public function testGetConfigOfTypeImmutableById(): void {
    $configuration = [
      'one' => $this->one,
      'two' => $this->two,
      'entity' => $this->entity,
    ];
    $config = new TaskConfig($configuration);
    self::assertSame($configuration['one'], $config->getConfigOfTypeImmutableById('one'));
    self::expectException(\ErrorException::class);
    $config->getConfigOfTypeImmutableById('entity');
  }

  /**
   * Test getting a config entity by ID.
   *
   * @covers ::getConfigOfTypeEntityById
   *
   * @throws \ErrorException
   */
  public function testGetConfigOfTypeEntityById(): void {
    $configuration = [
      'one' => $this->one,
      'two' => $this->two,
      'entity' => $this->entity,
    ];
    $config = new TaskConfig($configuration);
    self::assertSame($configuration['entity'], $config->getConfigOfTypeEntityById('entity'));
    self::expectException(\ErrorException::class);
    $config->getConfigOfTypeEntityById('one');
  }

  /**
   * Test getting the config entity.
   *
   * @covers ::getEntity
   *
   * @throws \ErrorException
   */
  public function testGetEntity(): void {
    $config = new TaskConfig([]);
    $this->expectException(\ErrorException::class);
    $config->getEntity();

    $config = new TaskConfig([], $this->entity);
    self::assertSame($this->entity, $config->getEntity());
  }

}
