<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Unit\Result;

use Drupal\reviewer\Reviewer\Result\Type;
use Drupal\reviewer\Reviewer\Result\TypeFactory;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the type factory.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Result\TypeFactory
 *
 * @group reviewer
 */
final class TypeFactoryTest extends UnitTestCase {

  private readonly TypeFactory $typeFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->typeFactory = new TypeFactory();
  }

  /**
   * Test creating an individual type.
   *
   * @covers ::createIndividual
   */
  public function testCreateIndividual(): void {
    self::assertSame(Type::Individual, $this->typeFactory->createIndividual());
  }

  /**
   * Test creating a summary type.
   *
   * @covers ::createSummary
   */
  public function testCreateSummary(): void {
    self::assertSame(Type::Summary, $this->typeFactory->createSummary());
  }

}
