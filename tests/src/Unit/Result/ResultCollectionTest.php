<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Unit\Result;

use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\Result\MultipleResultsInterface;
use Drupal\reviewer\Reviewer\Result\ResultCollectionFactory;
use Drupal\reviewer\Reviewer\Result\ResultCollectionFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultCollectionInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\RunnableInterface;
use Drupal\Tests\reviewer\Traits\MockResultCollectionTrait;
use Drupal\Tests\reviewer\Traits\MockResultTrait;
use Drupal\Tests\reviewer\Traits\MockReviewTrait;
use Drupal\Tests\UnitTestCase;

/**
 * Test result collection creation and methods.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Result\ResultCollection
 *
 * @group reviewer
 */
final class ResultCollectionTest extends UnitTestCase {

  use MockResultTrait;
  use MockResultCollectionTrait;
  use MockReviewTrait;

  private readonly ResultCollectionFactoryInterface $collectionFactory;

  private readonly ResultCollectionInterface $collection;

  private readonly MultipleResultsInterface & RunnableInterface & IgnoredTasksInterface $ran;

  private readonly ResultInterface $result;

  /**
   * Results.
   *
   * @var array<string, \Drupal\reviewer\Reviewer\Result\ResultCollectionInterface|\Drupal\reviewer\Reviewer\Result\ResultInterface>
   */
  private readonly array $results;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->collectionFactory = new ResultCollectionFactory();
    $this->collection = $this->collectionFactory->create(
      $this->ran = $this->mockReview(),
      $this->result = $this->mockResult(),
      $this->results = ['results' => $this->mockResultCollection()],
    );
  }

  /**
   * Test getting the ran item.
   *
   * @covers ::ran
   */
  public function testRan(): void {
    self::assertSame($this->ran, $this->collection->ran());
  }

  /**
   * Test getting the ran item.
   *
   * @covers ::result
   */
  public function testResult(): void {
    self::assertSame($this->result, $this->collection->result());
  }

  /**
   * Test getting the ran item.
   *
   * @covers ::results
   */
  public function testResults(): void {
    self::assertSame($this->results, $this->collection->results());
  }

  /**
   * Test getting the ran item.
   *
   * @covers ::individualResults
   */
  public function testIndividualResults(): void {
    $root_a = $this->collectionFactory->create(
      $this->ran,
      $this->result,
      [
        'root_a_one' => $this->mockResult('root_a_one'),
        'root_a_two' => $this->mockResult('root_a_two'),
        'root_a_three' => $this->mockResult('root_a_three'),
      ],
    );
    $root_b = $this->collectionFactory->create(
      $this->ran,
      $this->result,
      [
        'root_b_one' => $this->mockResult('root_b_one'),
        'root_b_two' => $this->mockResult('root_b_two'),
        'root_b_three' => $this->mockResult('root_b_three'),
      ],
    );
    $root_c = $this->collectionFactory->create(
      $this->ran,
      $this->result,
      [
        'root_c_one' => $this->mockResult('root_c_one'),
        'root_c_two' => $this->mockResult('root_c_two'),
        'root_c_three' => $this->mockResult('root_c_three'),
      ],
    );
    $root_d = $this->collectionFactory->create(
      $this->ran,
      $this->result,
      [
        'root_d_one' => $this->mockResult('root_d_one'),
        'root_d_two' => $this->mockResult('root_d_two'),
        'root_d_three' => $this->mockResult('root_d_three'),
      ],
    );

    $mid_one = $this->collectionFactory->create(
      $this->ran,
      $this->result,
      ['root_a' => $root_a, 'root_b' => $root_b],
    );
    $mid_two = $this->collectionFactory->create(
      $this->ran,
      $this->result,
      ['root_c' => $root_c, 'root_d' => $root_d],
    );

    $top = $this->collectionFactory->create(
      $this->ran,
      $this->result,
      ['mid_one' => $mid_one, 'mid_two' => $mid_two],
    );

    self::assertSame($root_a->results(), $root_a->individualResults());
    self::assertEquals($top->individualResults(), array_merge(
      $root_a->individualResults(),
      $root_b->individualResults(),
      $root_c->individualResults(),
      $root_d->individualResults(),
    ));
    self::assertEquals(
      array_merge($mid_one->individualResults(), $mid_two->individualResults()),
      array_merge(
        $root_a->individualResults(),
        $root_b->individualResults(),
        $root_c->individualResults(),
        $root_d->individualResults(),
      ),
    );
  }

}
