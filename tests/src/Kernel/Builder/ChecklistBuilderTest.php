<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Kernel\Builder;

use Drupal\KernelTests\KernelTestBase;
use Drupal\reviewer\Plugin\ChecklistManagerInterface;
use Drupal\reviewer\Reviewer\Builder\Checklist\ChecklistBuilderInterface;
use Drupal\reviewer\Reviewer\Checklist\ChecklistInterface;
use Drupal\reviewer\Reviewer\Result\ResultFactoryInterface;
use Drupal\Tests\reviewer\Traits\MockChecklistPluginTrait;
use Drupal\Tests\reviewer\Traits\MockTaskTrait;

/**
 * Tests the checklist builder.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Builder\Checklist\ChecklistBuilder
 *
 * @group reviewer
 */
final class ChecklistBuilderTest extends KernelTestBase {

  use MockChecklistPluginTrait;
  use MockTaskTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer',
  ];

  private readonly ChecklistBuilderInterface $builder;

  /**
   * Test checklist plugins.
   *
   * @var array<string, \Drupal\reviewer\Plugin\reviewer\Checklist\ChecklistPluginInterface>
   */
  private readonly array $checklists;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->checklists = [
      'one' => $this->mockChecklistPlugin(
        plugin_id: 'one',
        label: 'One',
        tasks: [
          $this->mockTask(),
          $this->mockTask(),
          $this->mockTask(),
        ],
      ),
      'two' => $this->mockChecklistPlugin(
        plugin_id: 'two',
        label: 'Two',
        tasks: [
          $this->mockTask(),
          $this->mockTask(),
          $this->mockTask(),
        ],
      ),
    ];

    $this->container->set(
      'reviewer.factory.result',
      $this->createMock(ResultFactoryInterface::class),
    );

    $definitions = [];
    foreach ($this->checklists as $checklist) {
      $definitions[$checklist->getPluginId()] = [
        'id' => $checklist->getPluginId(),
        'label' => $checklist->getLabel(),
      ];
    }
    $plugin_manager = $this
      ->getMockBuilder(ChecklistManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $plugin_manager
      ->method('getDefinition')
      ->willReturnOnConsecutiveCalls(...$definitions);
    $plugin_manager
      ->method('createInstance')
      ->willReturnOnConsecutiveCalls(...$this->checklists);
    $this->container->set('plugin.manager.reviewer.checklist', $plugin_manager);

    $this->builder = $this->container->get('reviewer.builder.checklist');
  }

  /**
   * Test building a checklist from a plugin ID.
   *
   * @covers ::fromId
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testFromId(): void {
    $checklist = $this->builder->fromId($this->checklists['one']->getPluginId());
    self::assertSame(['one'], array_keys($checklist));
    self::assertContainsOnlyInstancesOf(ChecklistInterface::class, $checklist);
  }

  /**
   * Test building a checklist from an array of plugin IDs.
   *
   * @covers ::fromIds
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testFromIds(): void {
    $checklists = $this->builder->fromIds(array_keys($this->checklists));
    self::assertSame(['one', 'two'], array_keys($checklists));
    self::assertContainsOnlyInstancesOf(ChecklistInterface::class, $checklists);
  }

  /**
   * Test building a checklist from a plugin.
   *
   * @covers ::fromPlugin
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testFromPlugin(): void {
    $checklist = $this->builder->fromPlugin($this->checklists['one']);
    self::assertSame(['one'], array_keys($checklist));
    self::assertContainsOnlyInstancesOf(ChecklistInterface::class, $checklist);
  }

  /**
   * Test building a checklist from an array of plugins.
   *
   * @covers ::fromPlugins
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testFromPlugins(): void {
    $checklists = $this->builder->fromPlugins($this->checklists);
    self::assertSame(['one', 'two'], array_keys($checklists));
    self::assertContainsOnlyInstancesOf(ChecklistInterface::class, $checklists);
  }

}
