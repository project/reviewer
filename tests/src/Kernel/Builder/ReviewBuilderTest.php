<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Kernel\Builder;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\reviewer\Plugin\ReviewManagerInterface;
use Drupal\reviewer\Reviewer\Builder\Review\ReviewBuilderInterface;
use Drupal\reviewer\Reviewer\Result\ResultFactoryInterface;
use Drupal\reviewer\Reviewer\Review\ReviewInterface;
use Drupal\reviewer\Reviewer\Task\TaskConfig;
use Drupal\reviewer\Reviewer\Task\TaskConfigFactoryInterface;
use Drupal\Tests\reviewer\Traits\MockChecklistPluginTrait;
use Drupal\Tests\reviewer\Traits\MockReviewPluginTrait;

/**
 * Tests the review builder.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Builder\Review\ReviewBuilder
 *
 * @group reviewer
 */
final class ReviewBuilderTest extends KernelTestBase {

  use MockChecklistPluginTrait;
  use MockReviewPluginTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config',
    'field',
    'link',
    'node',
    'reviewer',
    'reviewer_test',
    'system',
    'taxonomy',
    'text',
    'user',
  ];

  private readonly ReviewBuilderInterface $builder;

  /**
   * Test review plugins.
   *
   * @var array<string, \Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface>
   */
  private readonly array $reviews;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->reviews = [
      'one' => $this->mockReviewPlugin(
        plugin_id: 'one',
        label: 'One',
        checklists: ['test_entity_view_display_checklist'],
      ),
      'two' => $this->mockReviewPlugin(
        plugin_id: 'two',
        label: 'Two',
        checklists: [
          'test_entity_view_display_checklist',
          'test_return_values_checklist',
        ],
      ),
      'three' => $this->mockReviewPlugin(
        plugin_id: 'three',
        label: 'Three',
        checklists: [
          'test_entity_view_display_checklist',
          'test_return_values_checklist',
          'test_simple_configuration_checklist',
        ],
        entity_type: 'type',
        bundles: ['a', 'b'],
      ),
    ];

    $config_entity = $this
      ->getMockBuilder(ConfigEntityInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $config_entity
      ->method('label')
      ->willReturn('Entity Label');
    $entity_storage = $this
      ->getMockBuilder(EntityStorageInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entity_storage
      ->method('load')
      ->willReturn($config_entity);
    $entity_type_manager = $this
      ->getMockBuilder(EntityTypeManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entity_type_manager
      ->method('getStorage')
      ->willReturn($entity_storage);
    $this->container->set('entity_type.manager', $entity_type_manager);

    $config_factory = $this
      ->getMockBuilder(TaskConfigFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $config_factory
      ->method('create')
      ->willReturn(new TaskConfig([]));
    $this->container->set('reviewer.factory.config', $config_factory);

    $this->container->set(
      'reviewer.factory.result',
      $this->createMock(ResultFactoryInterface::class),
    );

    $definitions = [];
    foreach ($this->reviews as $review) {
      $definitions[$review->getPluginId()] = [
        'id' => $review->getPluginId(),
        'label' => $review->getLabel(),
        'bundles' => $review->getBundles(),
      ];
    }
    $plugin_manager = $this
      ->getMockBuilder(ReviewManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $plugin_manager
      ->method('getDefinition')
      ->willReturnOnConsecutiveCalls(...$definitions);
    $plugin_manager
      ->method('createInstance')
      ->willReturnOnConsecutiveCalls(...$this->reviews);
    $this->container->set('plugin.manager.reviewer.review', $plugin_manager);

    $this->builder = $this->container->get('reviewer.builder.review');
  }

  /**
   * Test building a review from a plugin ID.
   *
   * @covers ::fromId
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   */
  public function testFromId(): void {
    /** @var \Drupal\reviewer\Reviewer\Review\ReviewInterface[] $review */
    $review = $this->builder->fromId($this->reviews['one']->getPluginId());
    self::assertSame(['one'], array_keys($review));
    self::assertContainsOnlyInstancesOf(ReviewInterface::class, $review);
    self::assertSame('One', $review['one']->getLabel());
    self::assertCount(1, $review['one']->getChecklists());

    $review = $this->builder->fromId($this->reviews['two']->getPluginId());
    self::assertSame(['two'], array_keys($review));
    self::assertContainsOnlyInstancesOf(ReviewInterface::class, $review);
    self::assertCount(2, $review['two']->getChecklists());

    /** @var \Drupal\reviewer\Reviewer\Review\ReviewInterface[] $reviews */
    $reviews = $this->builder->fromId($this->reviews['three']->getPluginId());
    self::assertSame(['three.a', 'three.b'], array_keys($reviews));
    self::assertContainsOnlyInstancesOf(ReviewInterface::class, $review);
    self::assertSame('Three: Entity Label', $reviews['three.a']->getLabel());
    self::assertSame('Three: Entity Label', $reviews['three.b']->getLabel());
    self::assertCount(3, $reviews['three.a']->getChecklists());
    self::assertCount(3, $reviews['three.b']->getChecklists());
  }

  /**
   * Test building a review from an array of plugin IDs.
   *
   * @covers ::fromIds
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testFromIds(): void {
    $reviews = $this->builder->fromIds(array_keys($this->reviews));
    self::assertSame(['one', 'two', 'three.a', 'three.b'], array_keys($reviews));
    self::assertContainsOnlyInstancesOf(ReviewInterface::class, $reviews);
  }

  /**
   * Test building a review from a plugin.
   *
   * @covers ::fromPlugin
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testFromPlugin(): void {
    $review = $this->builder->fromPlugin($this->reviews['one']);
    self::assertSame(['one'], array_keys($review));
    self::assertContainsOnlyInstancesOf(ReviewInterface::class, $review);

    $reviews = $this->builder->fromPlugin($this->reviews['three']);
    self::assertSame(['three.a', 'three.b'], array_keys($reviews));
    self::assertContainsOnlyInstancesOf(ReviewInterface::class, $reviews);
  }

  /**
   * Test building a review from an array of plugins.
   *
   * @covers ::fromPlugins
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testFromPlugins(): void {
    $reviews = $this->builder->fromPlugins($this->reviews);
    self::assertSame(['one', 'two', 'three.a', 'three.b'], array_keys($reviews));
    self::assertContainsOnlyInstancesOf(ReviewInterface::class, $reviews);
  }

}
