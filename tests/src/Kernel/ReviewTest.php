<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Kernel\Review;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\reviewer\Exception\NotRunException;
use Drupal\reviewer\Reviewer\IgnorerInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\Result\Type;
use Drupal\reviewer\Reviewer\Review\ReviewInterface;
use Drupal\reviewer\Reviewer\Status\Status;
use Drupal\reviewer\Reviewer\Task\TaskConfig;
use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;
use Drupal\Tests\reviewer\Traits\MockChecklistTrait;
use Drupal\Tests\reviewer\Traits\MockResultCollectionTrait;
use Drupal\Tests\reviewer\Traits\MockResultTrait;

/**
 * Test review methods.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Review\Review
 * @covers \Drupal\reviewer\Reviewer\Review\ReviewFactory::create
 *
 * @ingroup reviewer
 */
final class ReviewTest extends KernelTestBase {

  use MockChecklistTrait;
  use MockResultTrait;
  use MockResultCollectionTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer',
  ];

  private readonly ReviewInterface $review;

  private readonly string $id;

  private readonly string $label;

  /**
   * Checklists.
   *
   * @var array<string, \Drupal\reviewer\Reviewer\Checklist\ChecklistInterface>
   */
  private readonly array $checklists;

  private readonly TaskConfigInterface $configuration;

  /**
   * Ignored IDs.
   *
   * @var array{id: string, reason: string}[]
   */
  private readonly array $ignored;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $review_factory = $this->container->get('reviewer.factory.review');
    $this->review = $review_factory->create(
      $this->id = 'review',
      $this->label = 'Review',
      $this->checklists = [
        'one' => $this->mockChecklist(id: 'one', result: $this->mockResultCollection(), configuration: new TaskConfig([])),
        'two' => $this->mockChecklist(id: 'two', result: $this->mockResultCollection(), configuration: new TaskConfig([])),
        'three' => $this->mockChecklist(id: 'three', result: $this->mockResultCollection(), configuration: new TaskConfig([])),
      ],
      $this->configuration = new TaskConfig([
        'one' => $this->createMock(ImmutableConfig::class),
        'two' => $this->createMock(ImmutableConfig::class),
        'three' => $this->createMock(ImmutableConfig::class),
      ]),
      $this->ignored = [
        ['id' => 'one', 'reason' => 'One'],
        ['id' => 'two', 'reason' => 'Two'],
        ['id' => 'three', 'reason' => 'Three'],
      ],
    );
  }

  /**
   * Test running the review.
   *
   * @covers ::run
   *
   * @throws \Exception
   */
  public function testRun(): void {
    $this->container->get('kernel')->rebuildContainer();

    $ignorer = $this
      ->getMockBuilder(IgnorerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $ignorer->method('isIgnored')->willReturn(FALSE);
    $this->container->set('reviewer.ignorer', $ignorer);
    $review_factory = $this->container->get('reviewer.factory.review');

    $review = $review_factory->create(
      $this->id,
      $this->label,
      [
        'one' => $this->mockChecklist(
          id: 'one',
          result: $this->mockResultCollection(
            result: $this->mockResult('pass', Status::Pass),
            results: [
              $this->mockResult(id: 'pass_one', status: Status::Pass),
              $this->mockResult(id: 'pass_two', status: Status::Pass),
            ],
          ),
          configuration: new TaskConfig([]),
        ),
        'two' => $this->mockChecklist(
          id: 'two',
          result: $this->mockResultCollection(
            result: $this->mockResult('fail', Status::Fail),
            results: [
              $this->mockResult(id: 'fail_one', status: Status::Fail),
              $this->mockResult(id: 'fail_two', status: Status::Fail),
            ],
          ),
          configuration: new TaskConfig([]),
        ),
      ],
      $this->configuration,
      $this->ignored,
    );
    $review->run();

    self::assertSame(Type::Summary, $review->result()->getType());
    self::assertNotSame($review->results()->results(), $review->results()->individualResults());
    self::assertSame(['one', 'two'], array_keys($review->results()->results()));
    self::assertSame(
      'Failed',
      $review->result()->message(),
    );
    self::assertContainsOnlyInstancesOf(
      ResultInterface::class,
      $review->results()->individualResults(),
    );
  }

  /**
   * Test getting the review ID.
   *
   * @covers ::getId
   */
  public function testGetId(): void {
    self::assertSame($this->id, $this->review->getId());
  }

  /**
   * Test getting the review label.
   *
   * @covers ::getLabel
   */
  public function testGetLabel(): void {
    self::assertSame($this->label, $this->review->getLabel());
  }

  /**
   * Test getting the review checklists.
   *
   * @covers ::getChecklists
   */
  public function testGetChecklists(): void {
    self::assertSame($this->checklists, $this->review->getChecklists());
  }

  /**
   * Test getting the review configuration.
   *
   * @covers ::getConfiguration
   */
  public function testGetConfiguration(): void {
    self::assertSame($this->configuration, $this->review->getConfiguration());
  }

  /**
   * Test getting the review ignored items.
   *
   * @covers ::getIgnored
   */
  public function testGetIgnored(): void {
    self::assertSame($this->ignored, $this->review->getIgnored());
  }

  /**
   * Test that getting the result without the result set throws an exception.
   *
   * @covers ::result
   *
   * @throws \Drupal\reviewer\Exception\NotRunException
   * @throws \ReflectionException
   */
  public function testResult(): void {
    try {
      $this->review->result();
      self::fail(NotRunException::class . ' not thrown.');
    }
    catch (NotRunException $e) {
    }

    $reflection = new \ReflectionClass($this->review);
    $property = $reflection->getProperty('result');
    $result = $this->mockResult();
    $collection = $this->mockResultCollection(result: $result);
    $property->setValue($this->review, $collection);
    self::assertSame($result, $this->review->result());
  }

  /**
   * Test that getting the results without the results set throws an exception.
   *
   * @covers ::results
   *
   * @throws \Drupal\reviewer\Exception\NotRunException
   * @throws \ReflectionException
   */
  public function testResults(): void {
    try {
      $this->review->result();
      self::fail(NotRunException::class . ' not thrown.');
    }
    catch (NotRunException $e) {
    }

    $reflection = new \ReflectionClass($this->review);
    $property = $reflection->getProperty('result');
    $collection = $this->mockResultCollection();
    $property->setValue($this->review, $collection);
    self::assertSame($collection, $this->review->results());
  }

}
