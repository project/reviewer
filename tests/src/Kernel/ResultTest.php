<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Kernel\Result;

use Drupal\KernelTests\KernelTestBase;
use Drupal\reviewer\Plugin\ReviewManagerInterface;
use Drupal\reviewer\Reviewer\IgnorerInterface;
use Drupal\reviewer\Reviewer\Result\ResultFactory;
use Drupal\reviewer\Reviewer\Result\ResultFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\Result\Type;
use Drupal\reviewer\Reviewer\Status\Status;
use Drupal\reviewer\Reviewer\Task\TaskInterface;
use Drupal\Tests\reviewer\Traits\MockReviewPluginTrait;
use Drupal\Tests\reviewer\Traits\MockTaskTrait;

/**
 * Test result methods.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Result\Result
 *
 * @ingroup reviewer
 */
final class ResultTest extends KernelTestBase {

  use MockReviewPluginTrait;
  use MockTaskTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer',
  ];

  private readonly ResultFactoryInterface $resultFactory;

  private readonly ResultInterface $result;

  private readonly string $id;

  private readonly Type $type;

  private readonly Status $status;

  private readonly TaskInterface $task;

  private readonly string $message;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->container->get('kernel')->rebuildContainer();

    $review_manager = $this
      ->getMockBuilder(ReviewManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $review_manager
      ->method('createAllInstances')
      ->willReturn([$this->mockReviewPlugin()]);
    $this->container->set('plugin.manager.reviewer.review', $review_manager);

    $this->resultFactory = $this->container->get('reviewer.factory.result');
    $this->result = $this->resultFactory->create(
      $this->id = 'result',
      $this->type = Type::Individual,
      $this->status = Status::Fail,
      $this->task = $this->mockTask(),
      $this->message = 'Review',
    );
  }

  /**
   * Test creating tasks.
   *
   * @covers \Drupal\reviewer\Reviewer\Result\ResultFactory::create
   *
   * @throws \Exception
   */
  public function testCreate(): void {
    $ignorer = $this
      ->getMockBuilder(IgnorerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $ignorer->method('isIgnored')->willReturn(TRUE);
    $this->container->set('reviewer.ignorer', $ignorer);
    $result_factory = new ResultFactory(
      $ignorer,
      $this->container->get('reviewer.factory.status'),
    );
    $result = $result_factory->create(
      $this->id,
      $this->type,
      $this->status,
      $this->task,
    );
    self::assertSame(Status::IgnoredFailure, $result->getStatus());
  }

  /**
   * Test getting the result ID.
   *
   * @covers ::getId
   */
  public function testGetId(): void {
    self::assertSame($this->id, $this->result->getId());
  }

  /**
   * Test getting the result type.
   *
   * @covers ::getType
   */
  public function testGetType(): void {
    self::assertSame($this->type, $this->result->getType());
  }

  /**
   * Test getting the result status.
   *
   * @covers ::getStatus
   */
  public function testGetStatus(): void {
    self::assertSame($this->status, $this->result->getStatus());
  }

  /**
   * Test setting the override status.
   *
   * @covers ::overrideStatus
   */
  public function testOverrideStatus(): void {
    $this->result->overrideStatus(Status::NotRun);
    self::assertSame(Status::NotRun, $this->result->getStatus());
  }

  /**
   * Test getting the message.
   *
   * @covers ::message
   *
   * @throws \Exception
   */
  public function testMessage(): void {
    self::assertSame($this->message, $this->result->message());

    $no_message_result = $this->resultFactory->create(
      'no_message',
      Type::Individual,
      Status::Fail,
      $this->mockTask(),
    );
    self::assertSame(Status::Fail->label(), $no_message_result->message());

    $no_message_result->setOverrideMessage('override');
    self::assertSame('override', $no_message_result->message());
  }

  /**
   * Test getting the result message.
   *
   * @covers ::getOriginalMessage
   *
   * @throws \Exception
   */
  public function testGetOriginalMessage(): void {
    self::assertSame($this->message, $this->result->getOriginalMessage());

    $no_message_result = $this->resultFactory->create(
      'message',
      Type::Individual,
      Status::Fail,
      $this->mockTask(),
      'message',
    );
    self::assertSame('message', $no_message_result->getOriginalMessage());
  }

  /**
   * Test setting and getting the override message.
   *
   * @covers ::getOverrideMessage
   * @covers ::setOverrideMessage
   */
  public function testSetOverrideMessage(): void {
    $this->result->setOverrideMessage('override');
    self::assertSame('override', $this->result->getOverrideMessage());
  }

}
