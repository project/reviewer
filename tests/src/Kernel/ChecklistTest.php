<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Kernel\Checklist;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\reviewer\Exception\CannotOverridePropertyException;
use Drupal\reviewer\Exception\NotRunException;
use Drupal\reviewer\Reviewer\Checklist\ChecklistInterface;
use Drupal\reviewer\Reviewer\IgnorerInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\Result\Type;
use Drupal\reviewer\Reviewer\Review\ReviewInterface;
use Drupal\reviewer\Reviewer\Status\Status;
use Drupal\reviewer\Reviewer\Task\TaskConfig;
use Drupal\Tests\reviewer\Traits\MockResultCollectionTrait;
use Drupal\Tests\reviewer\Traits\MockResultTrait;
use Drupal\Tests\reviewer\Traits\MockReviewTrait;
use Drupal\Tests\reviewer\Traits\MockTaskTrait;

/**
 * Test checklist methods.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Checklist\Checklist
 * @covers \Drupal\reviewer\Reviewer\Checklist\ChecklistFactory::create
 *
 * @ingroup reviewer
 */
final class ChecklistTest extends KernelTestBase {

  use MockResultTrait;
  use MockResultCollectionTrait;
  use MockReviewTrait;
  use MockTaskTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer',
  ];

  private readonly ChecklistInterface $checklist;

  private readonly string $id;

  private readonly string $label;

  /**
   * Tasks.
   *
   * @var \Drupal\reviewer\Reviewer\Task\TaskInterface[]
   */
  private readonly array $tasks;

  private readonly ReviewInterface $review;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $checklist_factory = $this->container->get('reviewer.factory.checklist');
    $this->checklist = $checklist_factory->create(
      $this->id = 'checklist',
      $this->label = 'Checklist',
      $this->tasks = [
        $this->mockTask(id: 'one', check: fn() => TRUE, result: $this->mockResultCollection()),
        $this->mockTask(id: 'two', check: fn() => TRUE, result: $this->mockResultCollection()),
        $this->mockTask(id: 'three', check: fn() => TRUE, result: $this->mockResultCollection()),
      ],
    );
    $this->review = $this->mockReview(
      id: 'review',
      configuration: new TaskConfig([
        'one' => $this->createMock(ImmutableConfig::class),
        'two' => $this->createMock(ImmutableConfig::class),
        'three' => $this->createMock(ImmutableConfig::class),
      ]),
      ignored: [
        ['id' => 'one', 'reason' => 'One'],
        ['id' => 'two', 'reason' => 'Two'],
        ['id' => 'three', 'reason' => 'Three'],
      ],
    );
  }

  /**
   * Test running the checklist.
   *
   * @covers ::run
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   * @throws \Exception
   */
  public function testRun(): void {
    $this->container->get('kernel')->rebuildContainer();
    $ignorer = $this
      ->getMockBuilder(IgnorerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $ignorer->method('isIgnored')->willReturn(FALSE);
    $this->container->set('reviewer.ignorer', $ignorer);
    $checklist_factory = $this->container->get('reviewer.factory.checklist');

    $checklist = $checklist_factory->create(
      $this->id,
      $this->label,
      [
        $this->mockTask(id: 'pass', result: $this->mockResultCollection(
          result: $this->mockResult('pass', Status::Pass),
          results: [
            $this->mockResult(id: 'pass_one', status: Status::Pass),
            $this->mockResult(id: 'pass_two', status: Status::Pass),
          ],
        )),
        $this->mockTask(id: 'fail', result: $this->mockResultCollection(
          result: $this->mockResult('fail', Status::Fail),
          results: [
            $this->mockResult(id: 'fail_one', status: Status::Fail),
            $this->mockResult(id: 'fail_two', status: Status::Fail),
          ],
        )),
      ],
      $this->review,
    );
    $checklist->run();

    self::assertSame(Type::Summary, $checklist->result()->getType());
    self::assertSame($checklist->results()->results(), $checklist->results()->individualResults());
    self::assertSame(
      ['pass_one', 'pass_two', 'fail_one', 'fail_two'],
      array_keys($checklist->results()->individualResults()),
    );
    self::assertSame(
      'Failed',
      $checklist->result()->message(),
    );
    self::assertContainsOnlyInstancesOf(
      ResultInterface::class,
      $checklist->results()->individualResults(),
    );
  }

  /**
   * Test getting the checklist ID.
   *
   * @covers ::getId
   */
  public function testGetId(): void {
    self::assertSame($this->id, $this->checklist->getId());
  }

  /**
   * Test getting the checklist label.
   *
   * @covers ::getLabel
   */
  public function testGetLabel(): void {
    self::assertSame($this->label, $this->checklist->getLabel());
  }

  /**
   * Test getting the checklist configuration.
   *
   * @covers ::getConfiguration
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testGetConfiguration(): void {
    $this->checklist->setReview($this->review);
    self::assertSame(
      $this->review->getConfiguration(),
      $this->checklist->getConfiguration(),
    );
  }

  /**
   * Test that attempting to re-set a review throws an exception.
   *
   * @covers ::setReview
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testSetReview(): void {
    $this->checklist->setReview($this->review);
    $this->expectException(CannotOverridePropertyException::class);
    $this->checklist->setReview($this->review);
  }

  /**
   * Test getting the checklist tasks.
   *
   * @covers ::tasks
   */
  public function testTasks(): void {
    self::assertSame($this->tasks, $this->checklist->tasks());
  }

  /**
   * Test that getting the result without the result set throws an exception.
   *
   * @covers ::result
   *
   * @throws \Drupal\reviewer\Exception\NotRunException
   * @throws \ReflectionException
   */
  public function testResult(): void {
    try {
      $this->checklist->result();
      self::fail(NotRunException::class . ' not thrown.');
    }
    catch (NotRunException $e) {
    }

    $reflection = new \ReflectionClass($this->checklist);
    $property = $reflection->getProperty('result');
    $result = $this->mockResult();
    $collection = $this->mockResultCollection(result: $result);
    $property->setValue($this->checklist, $collection);
    self::assertSame($result, $this->checklist->result());
  }

  /**
   * Test that getting the results without the results set throws an exception.
   *
   * @covers ::results
   *
   * @throws \Drupal\reviewer\Exception\NotRunException
   * @throws \ReflectionException
   */
  public function testResults(): void {
    try {
      $this->checklist->result();
      self::fail(NotRunException::class . ' not thrown.');
    }
    catch (NotRunException $e) {
    }

    $reflection = new \ReflectionClass($this->checklist);
    $property = $reflection->getProperty('result');
    $collection = $this->mockResultCollection();
    $property->setValue($this->checklist, $collection);
    self::assertSame($collection, $this->checklist->results());
  }

  /**
   * Test getting the checklist ignored items.
   *
   * @covers ::getIgnored
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testGetIgnored(): void {
    $this->checklist->setReview($this->review);
    self::assertSame($this->review->getIgnored(), $this->checklist->getIgnored());
  }

}
