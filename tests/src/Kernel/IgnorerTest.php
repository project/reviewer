<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Kernel;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\reviewer\Exception\NotIgnoredException;
use Drupal\reviewer\Plugin\ReviewManagerInterface;
use Drupal\reviewer\Reviewer\Ignorer;
use Drupal\reviewer\Reviewer\IgnorerInterface;
use Drupal\reviewer\Reviewer\Status\Status;
use Drupal\Tests\reviewer\Traits\MockResultTrait;
use Drupal\Tests\reviewer\Traits\MockReviewPluginTrait;

/**
 * Tests the ignorer.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Ignorer
 *
 * @group reviewer
 */
final class IgnorerTest extends KernelTestBase {

  use MockResultTrait;
  use MockReviewPluginTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer',
  ];

  private readonly ConfigFactoryInterface $configFactory;

  /**
   * Ignored test data.
   *
   * @var array{id: string, reason: string}[]
   */
  private readonly array $ignored;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->configFactory = $this->container->get('config.factory');
    $this->ignored = [
      1 => ['id' => 'one', 'reason' => 'reason one'],
      2 => ['id' => 'two', 'reason' => 'reason two'],
      3 => ['id' => 'three', 'reason' => 'reason three'],
      4 => ['id' => 'four', 'reason' => ''],
      5 => ['id' => 'five', 'reason' => ''],
    ];
  }

  /**
   * Test getting ignored items.
   *
   * @covers ::getIgnored
   *
   * @throws \Exception
   */
  public function testGetIgnored(): void {
    $ignorer = $this->mockIgnorer(
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
      [$this->ignored[5]],
    );
    self::assertSame(
      $ignorer->getIgnored(),
      [$this->ignored[5], $this->ignored[1], $this->ignored[3], $this->ignored[2]],
    );
  }

  /**
   * Test getting ignored items.
   *
   * @covers ::getConfigIgnored
   *
   * @throws \Exception
   */
  public function testGetConfigIgnored(): void {
    $ignorer = $this->mockIgnorer(
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
      [$this->ignored[5]],
    );
    self::assertSame(
      $ignorer->getConfigIgnored(),
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
    );
  }

  /**
   * Test getting ignored items.
   *
   * @covers ::getPluginIgnored
   *
   * @throws \Exception
   */
  public function testGetPluginIgnored(): void {
    $ignorer = $this->mockIgnorer(
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
      [$this->ignored[5]],
    );
    self::assertSame(
      $ignorer->getPluginIgnored(),
      [$this->ignored[5]],
    );
  }

  /**
   * Test checking a result is ignored.
   *
   * @covers ::isIgnored
   *
   * @throws \Exception
   */
  public function testIsIgnored(): void {
    $ignorer = $this->mockIgnorer(
      [$this->ignored[1], $this->ignored[2]],
      [$this->ignored[3], $this->ignored[4]],
    );
    self::assertTrue($ignorer->isIgnored($this->mockResult(id: 'one')));
    self::assertTrue($ignorer->isIgnored('two'));
    self::assertTrue($ignorer->isIgnored($this->mockResult('three')));
    self::assertTrue($ignorer->isIgnored('four'));
    self::assertNotTrue($ignorer->isIgnored($this->mockResult('five')));
    self::assertNotTrue($ignorer->isIgnored('six'));
  }

  /**
   * Test checking a result is ignored in configuration.
   *
   * @covers ::isConfigIgnored
   *
   * @throws \Exception
   */
  public function testIsConfigIgnored(): void {
    $ignorer = $this->mockIgnorer([$this->ignored[1], $this->ignored[2]]);
    self::assertTrue($ignorer->isConfigIgnored($this->mockResult(id: 'one')));
    self::assertTrue($ignorer->isConfigIgnored('two'));
    self::assertNotTrue($ignorer->isConfigIgnored($this->mockResult('three')));
    self::assertNotTrue($ignorer->isConfigIgnored('four'));
  }

  /**
   * Test checking a result is ignored in the plugin definition.
   *
   * @covers ::isPluginIgnored
   *
   * @throws \Exception
   */
  public function testIsPluginIgnored(): void {
    $ignorer = $this->mockIgnorer([], [$this->ignored[3], $this->ignored[4]]);
    self::assertTrue($ignorer->isPluginIgnored($this->mockResult('three')));
    self::assertTrue($ignorer->isPluginIgnored('four'));
    self::assertNotTrue($ignorer->isPluginIgnored($this->mockResult('five')));
    self::assertNotTrue($ignorer->isPluginIgnored('six'));
  }

  /**
   * Test retrieving ignored results from a set of results.
   *
   * @covers ::ignoredResults
   *
   * @throws \Exception
   */
  public function testIgnoredResults(): void {
    $ignorer = $this->mockIgnorer();

    $ignored_failure = $this->mockResult(status: Status::IgnoredFailure);
    $ignored_error = $this->mockResult(status: Status::IgnoredError);
    $pass = $this->mockResult(status: Status::Pass);
    $fail = $this->mockResult(status: Status::Fail);
    $error = $this->mockResult(status: Status::Error);

    self::assertSame(
      ['ignored_failure' => $ignored_failure],
      $ignorer->ignoredResults(['ignored_failure' => $ignored_failure]),
    );
    self::assertSame(
      ['ignored_failure' => $ignored_failure, 'ignored_error' => $ignored_error],
      $ignorer->ignoredResults([
        'ignored_failure' => $ignored_failure,
        'ignored_error' => $ignored_error,
      ]),
    );
    self::assertSame(
      ['ignored_failure' => $ignored_failure, 'ignored_error' => $ignored_error],
      $ignorer->ignoredResults([
        'ignored_failure' => $ignored_failure,
        'ignored_error' => $ignored_error,
        'pass' => $pass,
        'fail' => $fail,
        'error' => $error,
      ]),
    );
    self::assertSame(
      [],
      $ignorer->ignoredResults(['pass' => $pass, 'fail' => $fail, 'error' => $error]),
    );
    self::assertSame(
      [],
      $ignorer->ignoredResults([]),
    );
  }

  /**
   * Test retrieving the ignored reason for results.
   *
   * @covers ::ignoredReason
   *
   * @throws \Drupal\reviewer\Exception\NotIgnoredException
   * @throws \Exception
   */
  public function testIgnoredReason(): void {
    $result_one = $this->mockResult('one');
    $result_four = $this->mockResult('four');

    $ignorer = $this->mockIgnorer(
      [$this->ignored[1], $this->ignored[4]],
      [$this->ignored[2], $this->ignored[5]],
    );

    self::assertSame($this->ignored[1]['reason'], $ignorer->ignoredReason($result_one));
    self::assertSame($this->ignored[2]['reason'], $ignorer->ignoredReason('two'));
    self::assertSame($this->ignored[4]['reason'], $ignorer->ignoredReason($result_four));
    self::assertSame($this->ignored[5]['reason'], $ignorer->ignoredReason('five'));
    self::expectException(NotIgnoredException::class);
    $ignorer->ignoredReason('three');
  }

  /**
   * Test retrieving unignored results from a set of results.
   *
   * @covers ::unignoredResults
   *
   * @throws \Exception
   */
  public function testUnignoredResults(): void {
    $ignorer = $this->mockIgnorer();

    $ignored_failure = $this->mockResult(status: Status::IgnoredFailure);
    $ignored_error = $this->mockResult(status: Status::IgnoredError);
    $pass = $this->mockResult(status: Status::Pass);
    $fail = $this->mockResult(status: Status::Fail);
    $error = $this->mockResult(status: Status::Error);

    self::assertSame(
      [],
      $ignorer->unignoredResults(['ignored_failure' => $ignored_failure]),
    );
    self::assertSame(
      [],
      $ignorer->unignoredResults([
        'ignored_failure' => $ignored_failure,
        'ignored_error' => $ignored_error,
      ]),
    );
    self::assertSame(
      ['pass' => $pass, 'fail' => $fail, 'error' => $error],
      $ignorer->unignoredResults([
        'ignored_failure' => $ignored_failure,
        'ignored_error' => $ignored_error,
        'pass' => $pass,
        'fail' => $fail,
        'error' => $error,
      ]),
    );
    self::assertSame(
      ['pass' => $pass, 'fail' => $fail, 'error' => $error],
      $ignorer->unignoredResults(['pass' => $pass, 'fail' => $fail, 'error' => $error]),
    );
    self::assertSame([], $ignorer->unignoredResults([]));
  }

  /**
   * Test ignoring results.
   *
   * @covers ::ignore
   *
   * @throws \Exception
   */
  public function testIgnore(): void {
    $result_four = $this->mockResult('four');
    $result_five = $this->mockResult('five');

    $ignorer = $this->mockIgnorer(
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
      [$this->ignored[5]],
    );
    self::assertSame(
      $this->getIgnored(),
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
    );

    $ignorer->ignore($result_five);
    self::assertSame(
      $this->getIgnored(),
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
    );
    $ignorer->ignore($result_four->getId());
    self::assertSame(
      $this->getIgnored(),
      [$this->ignored[4], $this->ignored[1], $this->ignored[3], $this->ignored[2]],
    );
    $ignorer->ignore($result_four);
    self::assertSame(
      $this->getIgnored(),
      [$this->ignored[4], $this->ignored[1], $this->ignored[3], $this->ignored[2]],
    );
    $ignorer->ignore($this->mockResult('unused'), 'Reason');
    self::assertSame(
      $this->getIgnored(),
      [
        $this->ignored[4],
        $this->ignored[1],
        $this->ignored[3],
        $this->ignored[2],
        ['id' => 'unused', 'reason' => 'Reason'],
      ],
    );
  }

  /**
   * Test unignoring results.
   *
   * @covers ::unignore
   *
   * @throws \Exception
   */
  public function testUnignore(): void {
    $result_one = $this->mockResult('one');
    $result_two = $this->mockResult('two');
    $result_four = $this->mockResult('four');
    $result_five = $this->mockResult('five');
    $result_unused = $this->mockResult('unused');

    $ignorer = $this->mockIgnorer(
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
      [$this->ignored[5]],
    );
    self::assertSame(
      $this->getIgnored(),
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
    );

    $ignorer->unignore($result_one);
    self::assertSame(
      $this->getIgnored(),
      [$this->ignored[3], $this->ignored[2]],
    );
    $ignorer->unignore([$result_four, $result_two->getId()]);
    self::assertSame(
      $this->getIgnored(),
      [$this->ignored[3]],
    );
    $ignorer->unignore($result_five);
    self::assertSame(
      $this->getIgnored(),
      [$this->ignored[3]],
    );
    $ignorer->unignore($result_unused);
    self::assertSame(
      $this->getIgnored(),
      [$this->ignored[3]],
    );
  }

  /**
   * Test unignoring all results.
   *
   * @covers ::unignoreAll
   *
   * @throws \Exception
   */
  public function testUnignoreAll(): void {
    $ignorer = $this->mockIgnorer(
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
      [$this->ignored[5]],
    );
    self::assertSame(
      $this->getIgnored(),
      [$this->ignored[1], $this->ignored[3], $this->ignored[2]],
    );

    $ignorer->unignoreAll();
    self::assertSame([], $this->getIgnored());
  }

  /**
   * Get the ignorer with specific ignored configuration IDs.
   *
   * @param array{id: string, reason: string}[] $config_ignored
   * @param array{id: string, reason: string}[] $plugin_ignored
   *
   * @throws \Exception
   */
  private function mockIgnorer(
    array $config_ignored = [],
    array $plugin_ignored = [],
  ): IgnorerInterface {
    $this
      ->configFactory
      ->getEditable('reviewer.settings')
      ->set('ignored', $config_ignored)
      ->save();

    if ($plugin_ignored) {
      $this->container->get('kernel')->rebuildContainer();

      $review_manager = $this
        ->getMockBuilder(ReviewManagerInterface::class)
        ->disableOriginalConstructor()
        ->getMock();
      $review_manager
        ->method('createAllInstances')
        ->willReturn([$this->mockReviewPlugin(ignored: $plugin_ignored)]);
      $this->container->set('plugin.manager.reviewer.review', $review_manager);
    }

    return new Ignorer(
      $this->configFactory,
      $this->container->get('plugin.manager.reviewer.review'),
      $this->container->get('reviewer.evaluator.status'),
    );
  }

  /**
   * Get ignored configuration.
   *
   * @return array{id: string, reason: string}[]
   *
   * @throws \Exception
   */
  private function getIgnored(): array {
    return $this->configFactory->get('reviewer.settings')->get('ignored');
  }

}
