<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Kernel;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\reviewer\Reviewer\Review\ReviewInterface;
use Drupal\Tests\reviewer\Traits\MockReviewPluginTrait;
use Drupal\Tests\reviewer\Traits\MockReviewTrait;

/**
 * Tests the bundle evaluator.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\BundleEvaluator
 *
 * @group reviewer
 */
class BundleEvaluatorTest extends KernelTestBase {

  use MockReviewPluginTrait;
  use MockReviewTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer',
  ];

  /**
   * Test getting all available bundles from an entity review plugin.
   *
   * @covers ::allBundles
   *
   * @throws \Exception
   */
  public function testAllBundles(): void {
    $entity_type = $this
      ->getMockBuilder(EntityTypeInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entity_type->method('getBundleOf')->willReturn('review');
    $entity_storage = $this
      ->getMockBuilder(EntityStorageInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entity_storage->method('getEntityType')->willReturn($entity_type);
    $entity_type_manager = $this
      ->getMockBuilder(EntityTypeManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entity_type_manager->method('getStorage')->willReturn($entity_storage);
    $this->container->set('entity_type.manager', $entity_type_manager);
    $entity_bundle_info = $this
      ->getMockBuilder(EntityTypeBundleInfoInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entity_bundle_info
      ->method('getBundleInfo')
      ->willReturn(['one' => [], 'two' => []], ['review' => []]);
    $this->container->set('entity_type.bundle.info', $entity_bundle_info);

    $review = $this->mockReviewPlugin(
      plugin_id: 'review',
      entity_type: 'review',
    );
    $bundle_review = $this->mockReviewPlugin(
      plugin_id: 'review',
      entity_type: 'review',
      bundles: ['one'],
    );
    $evaluator = $this->container->get('reviewer.evaluator.bundle');
    self::assertSame(['one'], $evaluator->allBundles($bundle_review));
    self::assertSame(['one', 'two'], $evaluator->allBundles($review));
    self::assertSame([], $evaluator->allBundles($review));
  }

  /**
   * Test checking that a review has a bundle.
   *
   * @covers ::reviewHasBundle
   *
   * @throws \Exception
   */
  public function testReviewHasBundle(): void {
    $evaluator = $this->container->get('reviewer.evaluator.bundle');
    $review = $this->mockReview('review');
    $bundle = $this->mockReview('review.one');

    self::assertFalse($evaluator->reviewHasBundle($review, []));
    self::assertFalse($evaluator->reviewHasBundle($review, ['two']));
    self::assertFalse($evaluator->reviewHasBundle($bundle, []));
    self::assertFalse($evaluator->reviewHasBundle($bundle, ['two']));
    self::assertFalse($evaluator->reviewHasBundle($bundle, ['two', 'three']));
    self::assertTrue($evaluator->reviewHasBundle($bundle, ['one', 'two']));
  }

  /**
   * Test getting that a reviews matching a set of bundles.
   *
   * @covers ::reviewsOfBundles
   *
   * @throws \Exception
   */
  public function testReviewsOfBundles(): void {
    $evaluator = $this->container->get('reviewer.evaluator.bundle');
    $reviews = [
      'review' => $this->mockReview('review'),
      'review.one' => $this->mockReview('review.one'),
      'review.two' => $this->mockReview('review.two'),
      'review.three' => $this->mockReview('review.three'),
    ];

    self::assertSame(
      ['review.one' => $reviews['review.one'], 'review.two' => $reviews['review.two']],
      $evaluator->reviewsOfBundles($reviews, ['one', 'two']),
    );
    self::assertContainsOnlyInstancesOf(
      ReviewInterface::class,
      $evaluator->reviewsOfBundles($reviews, ['one', 'two']),
    );
    self::assertSame($reviews, $evaluator->reviewsOfBundles($reviews, []));
  }

}
