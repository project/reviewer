<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Kernel\Task;

use Drupal\KernelTests\KernelTestBase;
use Drupal\reviewer\Exception\CannotOverridePropertyException;
use Drupal\reviewer\Exception\NotRunException;
use Drupal\reviewer\Reviewer\Checklist\ChecklistInterface;
use Drupal\reviewer\Reviewer\IgnorerInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\Result\Type;
use Drupal\reviewer\Reviewer\Status\Status;
use Drupal\reviewer\Reviewer\Task\TaskConfig;
use Drupal\reviewer\Reviewer\Task\TaskFactoryInterface;
use Drupal\reviewer\Reviewer\Task\TaskInterface;
use Drupal\Tests\reviewer\Traits\MockChecklistTrait;
use Drupal\Tests\reviewer\Traits\MockResultCollectionTrait;
use Drupal\Tests\reviewer\Traits\MockResultTrait;

/**
 * Test task methods.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Task\Task
 * @covers \Drupal\reviewer\Reviewer\Task\TaskFactory::create
 *
 * @ingroup reviewer
 */
final class TaskTest extends KernelTestBase {

  use MockChecklistTrait;
  use MockResultTrait;
  use MockResultCollectionTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer',
  ];

  private readonly TaskFactoryInterface $taskFactory;

  private readonly TaskInterface $task;

  private readonly string $id;

  private \Closure $check;

  private readonly string $failureMessage;

  private readonly ChecklistInterface $checklist;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->taskFactory = $this->container->get('reviewer.factory.task');
    $this->task = $this->taskFactory->create(
      $this->id = 'task',
      $this->check = fn() => TRUE,
      $this->failureMessage = 'Task',
      $this->checklist = $this->mockChecklist(
        id: 'checklist',
        configuration: new TaskConfig([]),
      ),
    );
  }

  /**
   * Test running the task.
   *
   * @covers ::run
   *
   * @throws \Exception
   */
  public function testRun(): void {
    $this->container->get('kernel')->rebuildContainer();

    $ignorer = $this
      ->getMockBuilder(IgnorerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $ignorer->method('isIgnored')->willReturn(FALSE);
    $this->container->set('reviewer.ignorer', $ignorer);
    $task_factory = $this->container->get('reviewer.factory.task');

    $task_not_run = $task_factory->create(
      $this->id,
      fn() => NULL,
      $this->failureMessage,
      $this->checklist,
    );
    $task_not_run->run();
    self::assertSame(Status::NotRun->label(), $task_not_run->result()->message());

    $task_pass = $task_factory->create(
      $this->id,
      fn() => TRUE,
      $this->failureMessage,
      $this->checklist,
    );
    $task_pass->run();
    self::assertSame(Status::Pass->label(), $task_pass->result()->message());

    $task_fail = $task_factory->create(
      $this->id,
      fn() => FALSE,
      $this->failureMessage,
      $this->checklist,
    );
    $task_fail->run();
    self::assertSame($this->failureMessage, $task_fail->result()->message());

    $task_fail_array = $task_factory->create(
      $this->id,
      fn() => [
        'pass_one' => TRUE,
        'pass_two' => TRUE,
        'fail_one' => FALSE,
        'fail_two' => FALSE,
      ],
      'Task: @failures',
      $this->checklist,
    );
    $task_fail_array->run();
    self::assertSame(Type::Summary, $task_fail_array->result()->getType());
    self::assertSame($task_fail_array->results()->results(), $task_fail_array->results()->individualResults());
    self::assertSame(
      ['checklist.task.pass_one', 'checklist.task.pass_two', 'checklist.task.fail_one', 'checklist.task.fail_two'],
      array_keys($task_fail_array->results()->individualResults()),
    );
    foreach ($task_fail_array->results()->individualResults() as $result) {
      self::assertSame(Type::Individual, $result->getType());
    }
    self::assertSame(
      'Task: fail_one, fail_two',
      $task_fail_array->result()->message(),
    );

    $task_error_array = $task_factory->create(
      $this->id,
      fn() => [
        'pass_one' => TRUE,
        'pass_two' => TRUE,
        'fail_one' => FALSE,
        'fail_two' => FALSE,
        'error' => throw new \Exception('Exception thrown'),
      ],
      $this->failureMessage,
      $this->checklist,
    );
    // phpcs:disable Squiz.PHP.NonExecutableCode.Unreachable
    $task_error_array->run();
    self::assertSame('Exception thrown', $task_error_array->result()->message());
    self::assertSame(Status::Error, $task_error_array->result()->getStatus());
    self::assertInstanceOf(ResultInterface::class, $task_error_array->results()->results()['checklist.task']);
    self::assertSame(
      Status::Error,
      $task_error_array->results()->results()['checklist.task']->getStatus(),
    );

    $task_return_error = $task_factory->create(
      $this->id,
      fn() => 'string',
      $this->failureMessage,
      $this->checklist,
    );
    $task_return_error->run();
    self::assertSame(
      'Tasks must return an array, boolean value, or null. Drupal\Tests\reviewer\Kernel\Task\TaskTest::Drupal\Tests\reviewer\Kernel\Task\{closure}() returned string instead.',
      $task_return_error->result()->message(),
    );

    $task_return_array_error = $task_factory->create(
      $this->id,
      fn() => ['string'],
      $this->failureMessage,
      $this->checklist,
    );
    $task_return_array_error->run();
    self::assertSame(
      'Tasks must returning an array must have with boolean values, Drupal\Tests\reviewer\Kernel\Task\TaskTest::Drupal\Tests\reviewer\Kernel\Task\{closure}() returned type string for key "0" instead.',
      $task_return_array_error->result()->message(),
    );

    $this->container->get('kernel')->rebuildContainer();
    $ignorer = $this
      ->getMockBuilder(IgnorerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $ignorer->method('isIgnored')->willReturn(TRUE);
    $ignorer->method('ignoredReason')->willReturn('Reason');
    $this->container->set('reviewer.ignorer', $ignorer);

    $task_ignored = $this->container->get('reviewer.factory.task')->create(
      $this->id,
      fn() => [
        'pass_one' => TRUE,
        'pass_two' => TRUE,
        'fail_one' => FALSE,
        'fail_two' => FALSE,
      ],
      'Task: @failures',
      $this->checklist,
    );
    $task_ignored->run();
    self::assertSame(Status::IgnoredFailure, $task_ignored->result()->getStatus());
    self::assertSame('Reason', $task_ignored->result()->message());
    $count = 1;
    foreach ($task_ignored->results()->results() as $result) {
      self::assertInstanceOf(ResultInterface::class, $result);
      self::assertSame($count++ < 3 ? Status::Pass : Status::IgnoredFailure, $result->getStatus());
      self::assertSame('Reason', $result->message());
    }
    // phpcs:enable Squiz.PHP.NonExecutableCode.Unreachable
  }

  /**
   * Test result ID generation.
   *
   * @covers ::resultId
   *
   * @throws \Exception
   */
  public function testResultId(): void {
    self::assertSame(
      'checklist.task',
      $this->task->resultId('task'),
    );
    self::assertSame(
      'checklist.task',
      $this->task->resultId('checklist.task'),
    );
    self::assertSame(
      'checklist.task.subtask',
      $this->task->resultId('task.subtask'),
    );
  }

  /**
   * Test that the task ID is generated correctly.
   *
   * @covers ::getId
   */
  public function testGetId(): void {
    self::assertSame(
      "{$this->checklist->getId()}.$this->id",
      $this->task->getId(),
    );
  }

  /**
   * Test getting the check closure.
   *
   * @covers ::getCheck
   */
  public function testGetCheck(): void {
    self::assertSame($this->check, $this->task->getCheck());
  }

  /**
   * Test getting the check closure.
   *
   * @covers ::getFailureMessage
   */
  public function testGetFailureMessage(): void {
    self::assertSame($this->failureMessage, $this->task->getFailureMessage());
  }

  /**
   * Test that attempting to re-set a checklist throws an exception.
   *
   * @covers ::setChecklist
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   */
  public function testSetChecklist(): void {
    $this->expectException(CannotOverridePropertyException::class);
    $this->task->setChecklist($this->checklist);
  }

  /**
   * Test that getting the result without the result set throws an exception.
   *
   * @covers ::result
   *
   * @throws \Drupal\reviewer\Exception\NotRunException
   * @throws \ReflectionException
   */
  public function testResult(): void {
    try {
      $this->task->result();
      self::fail(NotRunException::class . ' not thrown.');
    }
    catch (NotRunException $e) {
    }

    $reflection = new \ReflectionClass($this->task);
    $property = $reflection->getProperty('result');
    $result = $this->mockResult();
    $collection = $this->mockResultCollection(result: $result);
    $property->setValue($this->task, $collection);
    self::assertSame($result, $this->task->result());
  }

  /**
   * Test that getting the results without the results set throws an exception.
   *
   * @covers ::results
   *
   * @throws \Drupal\reviewer\Exception\NotRunException
   * @throws \ReflectionException
   */
  public function testResults(): void {
    try {
      $this->task->result();
      self::fail(NotRunException::class . ' not thrown.');
    }
    catch (NotRunException $e) {
    }

    $reflection = new \ReflectionClass($this->task);
    $property = $reflection->getProperty('result');
    $collection = $this->mockResultCollection();
    $property->setValue($this->task, $collection);
    self::assertSame($collection, $this->task->results());
  }

  /**
   * Test getting ignored items.
   *
   * @covers ::getIgnored
   */
  public function testGetIgnored(): void {
    $task = $this->taskFactory->create(
      $this->id,
      $this->check,
      $this->failureMessage,
      $this->mockChecklist(
        ignored: [
          ['id' => 'one', 'reason' => 'One Reason'],
          ['id' => 'Two', 'reason' => 'Two Reason'],
        ],
        configuration: new TaskConfig([]),
      ),
    );
    self::assertSame(
      [
        ['id' => 'one', 'reason' => 'One Reason'],
        ['id' => 'Two', 'reason' => 'Two Reason'],
      ],
      $task->getIgnored(),
    );
  }

}
