<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Kernel\Task;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\reviewer\Reviewer\Task\TaskConfigFactory;
use Drupal\reviewer\Reviewer\Task\TaskConfigFactoryInterface;
use Drupal\Tests\reviewer\Traits\MockReviewPluginTrait;

/**
 * Test the config factory.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Task\TaskConfigFactory
 *
 * @group reviewer
 */
final class TaskConfigFactoryTest extends KernelTestBase {

  use MockReviewPluginTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer',
    'reviewer_test',
  ];

  private readonly ImmutableConfig $configOne;

  private readonly ImmutableConfig $configTwo;

  private readonly ImmutableConfig $configEmpty;

  private readonly ConfigEntityInterface $configEntity;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->configOne = $this
      ->getMockBuilder(ImmutableConfig::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->configOne
      ->method('getRawData')
      ->willReturn(['config_one']);
    $this->configTwo = $this
      ->getMockBuilder(ImmutableConfig::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->configTwo
      ->method('getRawData')
      ->willReturn(['config_two']);
    $this->configEmpty = $this
      ->getMockBuilder(ImmutableConfig::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->configEmpty
      ->method('getRawData')
      ->willReturn([]);
    $this->configEntity = $this
      ->getMockBuilder(ConfigEntityInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
  }

  /**
   * Test loading configuration and an entity type to create a config object.
   *
   * @covers ::create
   *
   * @throws \Exception
   * @throws \InvalidArgumentException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   */
  public function testCreate(): void {
    $review_plugin = $this->mockReviewPlugin(
      entity_type: 'node_fail',
      configuration: ['config_one', 'config_two', 'config_entity'],
    );
    $config_factory = $this
      ->getMockBuilder(ConfigFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $config_factory
      ->method('get')
      ->willReturn($this->configOne, $this->configTwo, $this->configEmpty);
    $config_manager = $this
      ->getMockBuilder(ConfigManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $config_manager
      ->method('getConfigFactory')
      ->willReturn($config_factory);
    $config_manager
      ->method('loadConfigEntityByName')
      ->willReturn($this->configEntity, $this->configEntity);
    $this->container->set('config.manager', $config_manager);
    $entity_type_manager = $this
      ->getMockBuilder(EntityTypeManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entity_storage = $this
      ->getMockBuilder(EntityStorageInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entity_storage
      ->method('load')
      ->willReturn($this->configEntity);
    $entity_type_manager
      ->method('getStorage')
      ->willReturn($entity_storage);
    $this->container->set('entity_type.manager', $entity_type_manager);
    $config = $this->configFactory()->create($review_plugin, 'bundle');
    self::assertSame(
      $config->getConfig(),
      [
        'config_one' => $this->configOne,
        'config_two' => $this->configTwo,
        'config_entity' => $this->configEntity,
      ],
    );
    self::assertSame($config->getEntity(), $this->configEntity);
  }

  /**
   * Get the config builder.
   *
   * @throws \Exception
   * @throws \InvalidArgumentException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   */
  private function configFactory(): TaskConfigFactoryInterface {
    return new TaskConfigFactory(
      $this->container->get('config.manager'),
      $this->container->get('entity_type.manager'),
    );
  }

}
