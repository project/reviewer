<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\reviewer\Exception\NotRunException;
use Drupal\reviewer\Reviewer\Review\ReviewInterface;
use Drupal\reviewer\Reviewer\RunnerInterface;

/**
 * Test runner methods.
 *
 * @coversDefaultClass \Drupal\reviewer\Reviewer\Runner
 *
 * @group reviewer
 */
class RunnerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config',
    'field',
    'link',
    'node',
    'reviewer',
    'reviewer_test',
    'system',
    'taxonomy',
    'text',
    'user',
  ];

  private readonly RunnerInterface $runner;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('node');
    $this->installConfig(static::$modules);
    $this->runner = $this->container->get('reviewer.runner');
  }

  /**
   * Test running review plugins from review plugin IDs.
   *
   * @covers ::run
   * @covers ::runIds
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   */
  public function testRunIds(): void {
    self::assertSame(
      ['test_node.node_pass', 'test_node.node_fail'],
      array_keys($this->runner->runIds(['test_node'])),
    );
    self::assertSame(
      ['test_node.node_pass'],
      array_keys($this->runner->runIds(['test_node'], ['node_pass'])),
    );
    self::assertSame(
      ['test_taxonomy.taxonomy_pass'],
      array_keys($this->runner->runIds(['test_taxonomy'], ['taxonomy_pass'])),
    );
    self::assertSame(
      ['node.type.node_fail'],
      array_keys($this->runner->runIds(['test_entity_id'])),
    );
    self::assertSame(
      ['test_simple_configuration'],
      array_keys($this->runner->runIds(['test_simple_configuration'])),
    );
    self::assertSame(
      ['test_return_values'],
      array_keys($this->runner->runIds(['test_return_values'])),
    );

    $reviews = $this->runner->runIds();
    self::assertCount(7, $reviews);
    self::assertContainsOnlyInstancesOf(ReviewInterface::class, $reviews);
    foreach ($reviews as $review) {
      try {
        $review->result();
      }
      catch (NotRunException $e) {
        self::fail("Review {$review->getId()} not run");
      }
    }
  }

  /**
   * Test running review plugins.
   *
   * @covers ::runPlugins
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   * @throws \Exception
   */
  public function testRunPlugins(): void {
    $review_manager = $this->container->get('plugin.manager.reviewer.review');
    $plugins = $review_manager->createAllInstances();

    self::assertSame(
      ['test_node.node_pass', 'test_node.node_fail'],
      array_keys($this->runner->runPlugins([$plugins['test_node']])),
    );
    self::assertSame(
      ['test_node.node_pass'],
      array_keys($this->runner->runPlugins([$plugins['test_node']], ['node_pass'])),
    );
    self::assertSame(
      ['test_taxonomy.taxonomy_pass'],
      array_keys($this->runner->runPlugins([$plugins['test_taxonomy']], ['taxonomy_pass'])),
    );
    self::assertSame(
      ['node.type.node_fail'],
      array_keys($this->runner->runPlugins([$plugins['test_entity_id']])),
    );
    self::assertSame(
      ['test_simple_configuration'],
      array_keys($this->runner->runPlugins([$plugins['test_simple_configuration']])),
    );
    self::assertSame(
      ['test_return_values'],
      array_keys($this->runner->runPlugins([$plugins['test_return_values']])),
    );

    $reviews = $this->runner->runPlugins();
    self::assertCount(7, $reviews);
    self::assertContainsOnlyInstancesOf(ReviewInterface::class, $reviews);
    foreach ($reviews as $review) {
      try {
        $review->result();
      }
      catch (NotRunException $e) {
        self::fail("Review {$review->getId()} not run");
      }
    }
  }

}
