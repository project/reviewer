<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Traits;

use Drupal\reviewer\Reviewer\Checklist\ChecklistInterface;
use Drupal\reviewer\Reviewer\Result\ResultCollectionInterface;
use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;

/**
 * Trait that provides a method to mock checklist.
 */
trait MockChecklistTrait {

  /**
   * Mock a checklist to return specific values.
   *
   * @param array{id: string, reason: string}[] $ignored
   */
  private function mockChecklist(
    string|null $id = NULL,
    string|null $label = NULL,
    array|null $ignored = NULL,
    ResultCollectionInterface|null $result = NULL,
    TaskConfigInterface|null $configuration = NULL,
  ): ChecklistInterface {
    $checklist = $this
      ->getMockBuilder(ChecklistInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    if (!\is_null($id)) {
      $checklist->method('getId')->willReturn($id);
    }
    if (!\is_null($label)) {
      $checklist->method('getLabel')->willReturn($label);
    }
    if (!\is_null($ignored)) {
      $checklist->method('getIgnored')->willReturn($ignored);
    }
    if (!\is_null($result)) {
      $checklist->method('result')->willReturn($result->result());
      $checklist->method('results')->willReturn($result);
    }
    $checklist->method('run')->willReturn($checklist);
    $checklist->method('getConfiguration')->willReturn($configuration ?? []);
    return $checklist;
  }

}
