<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Traits;

use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\Result\Type;
use Drupal\reviewer\Reviewer\Status\Status;

/**
 * Trait that provides a method to mock results.
 */
trait MockResultTrait {

  /**
   * Mock a result to return specific values.
   */
  private function mockResult(
    string|null $id = NULL,
    Status|null $status = NULL,
    Type|null $type = NULL,
  ): ResultInterface {
    $result = $this
      ->getMockBuilder(ResultInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    if (!\is_null($id)) {
      $result->method('getId')->willReturn($id);
    }
    if (!\is_null($status)) {
      $result->method('getStatus')->willReturn($status);
    }
    if (!\is_null($type)) {
      $result->method('getType')->willReturn($type);
    }
    return $result;
  }

}
