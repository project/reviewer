<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Traits;

use Drupal\reviewer\Reviewer\Result\ResultCollectionInterface;
use Drupal\reviewer\Reviewer\Task\TaskInterface;

/**
 * Trait that provides a method to mock tasks.
 */
trait MockTaskTrait {

  /**
   * Mock a task to return specific values.
   */
  private function mockTask(
    string|null $id = NULL,
    \Closure|null $check = NULL,
    string|null $failure_message = NULL,
    ResultCollectionInterface|null $result = NULL,
  ): TaskInterface {
    $task = $this
      ->getMockBuilder(TaskInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    if (!\is_null($id)) {
      $task->method('getId')->willReturn($id);
    }
    if (!\is_null($check)) {
      $task->method('getCheck')->willReturn($check);
    }
    if (!\is_null($failure_message)) {
      $task->method('getFailureMessage')->willReturn($failure_message);
    }
    if (!\is_null($result)) {
      $task->method('result')->willReturn($result->result());
      $task->method('results')->willReturn($result);
    }
    $task->method('run')->willReturn($task);
    return $task;
  }

}
