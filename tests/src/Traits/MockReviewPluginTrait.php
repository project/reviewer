<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Traits;

use Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface;

/**
 * Trait that provides methods to mock review plugins.
 */
trait MockReviewPluginTrait {

  /**
   * Mock a review plugin to return specific values.
   *
   * @param string[] $checklists
   * @param string[] $bundles
   * @param string[] $configuration
   * @param array{id: string, reason: string}[] $ignored
   * @param class-string $class
   */
  private function mockReviewPlugin(
    string|null $plugin_id = NULL,
    string|null $label = NULL,
    array|null $checklists = NULL,
    string|null $entity_id = NULL,
    string|null $entity_type = NULL,
    array|null $bundles = NULL,
    array|null $configuration = NULL,
    array|null $ignored = NULL,
    string $class = ReviewPluginInterface::class,
  ): ReviewPluginInterface {
    /** @var \Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface&\PHPUnit\Framework\MockObject\MockObject $review */
    $review = $this
      ->getMockBuilder($class)
      ->disableOriginalConstructor()
      ->getMock();
    if (!\is_null($plugin_id)) {
      $review->method('getPluginId')->willReturn($plugin_id);
    }
    if (!\is_null($label)) {
      $review->method('getLabel')->willReturn($label);
    }
    if (!\is_null($checklists)) {
      $review->method('getChecklists')->willReturn($checklists);
    }
    if (!\is_null($entity_id)) {
      $review->method('getEntityId')->willReturn($entity_id);
    }
    if (!\is_null($entity_type)) {
      $review->method('getEntityType')->willReturn($entity_type);
    }
    if (!\is_null($bundles)) {
      $review->method('getBundles')->willReturn($bundles);
    }
    if (!\is_null($configuration)) {
      $review->method('getConfiguration')->willReturn($configuration);
    }
    if (!\is_null($ignored)) {
      $review->method('getIgnored')->willReturn($ignored);
    }
    return $review;
  }

}
