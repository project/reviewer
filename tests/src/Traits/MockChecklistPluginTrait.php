<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Traits;

use Drupal\reviewer\Plugin\reviewer\Checklist\ChecklistPluginInterface;

/**
 * Trait that provides a method to mock checklist plugins.
 */
trait MockChecklistPluginTrait {

  /**
   * Mock a checklist plugin to return specific values.
   *
   * @param \Drupal\reviewer\Reviewer\Task\TaskInterface[] $tasks
   */
  private function mockChecklistPlugin(
    string|null $plugin_id = NULL,
    string|null $label = NULL,
    array|null $tasks = NULL,
  ): ChecklistPluginInterface {
    $checklist = $this
      ->getMockBuilder(ChecklistPluginInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    if (!\is_null($plugin_id)) {
      $checklist->method('getPluginId')->willReturn($plugin_id);
    }
    if (!\is_null($label)) {
      $checklist->method('getLabel')->willReturn($label);
    }
    if (!\is_null($tasks)) {
      $checklist->method('tasks')->willReturn($tasks);
    }
    return $checklist;
  }

}
