<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Traits;

use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\Result\MultipleResultsInterface;
use Drupal\reviewer\Reviewer\Result\ResultCollectionInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\RunnableInterface;

/**
 * Trait that provides a method to mock result collections.
 */
trait MockResultCollectionTrait {

  /**
   * Mock a result collection to return specific values.
   *
   * @param \Drupal\reviewer\Reviewer\Result\ResultInterface[]|null $results
   */
  private function mockResultCollection(
    (RunnableInterface & MultipleResultsInterface & IgnoredTasksInterface)|null $ran = NULL,
    ResultInterface|null $result = NULL,
    array|null $results = NULL,
  ): ResultCollectionInterface {
    $collection = $this
      ->getMockBuilder(ResultCollectionInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    if (!\is_null($ran)) {
      $collection->method('ran')->willReturn($ran);
    }
    if (!\is_null($result)) {
      $collection->method('result')->willReturn($result);
    }
    if (!\is_null($results)) {
      $collection->method('results')->willReturn($results);
      $collection->method('individualResults')->willReturn($results);
    }
    return $collection;
  }

}
