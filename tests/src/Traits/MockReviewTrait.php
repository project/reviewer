<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Traits;

use Drupal\reviewer\Reviewer\Review\ReviewInterface;
use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;

/**
 * Trait that provides a method to mock reviews.
 */
trait MockReviewTrait {

  /**
   * Mock a review to return specific values.
   *
   * @param array{id: string, reason: string}[] $ignored
   */
  private function mockReview(
    string|null $id = NULL,
    TaskConfigInterface|null $configuration = NULL,
    array|null $ignored = NULL,
  ): ReviewInterface {
    $review = $this
      ->getMockBuilder(ReviewInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    if (!\is_null($id)) {
      $review->method('getId')->willReturn($id);
      $review->method('resultId')->willReturn($id);
    }
    if (!\is_null($configuration)) {
      $review->method('getConfiguration')->willReturn($configuration);
    }
    if (!\is_null($ignored)) {
      $review->method('getIgnored')->willReturn($ignored);
    }
    return $review;
  }

}
