<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test reviewer.
 *
 * @group reviewer
 */
class ReviewerTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that uninstalling the test module removes test entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function testUninstall(): void {
    $entity_type_manager = $this->container->get('entity_type.manager');

    self::assertCount(2, $entity_type_manager->getStorage('node_type')->loadMultiple());
    self::assertCount(2, $entity_type_manager->getStorage('taxonomy_vocabulary')->loadMultiple());

    $this->container->get('module_installer')->uninstall(['reviewer_test']);
    self::assertCount(0, $entity_type_manager->getStorage('node_type')->loadMultiple());
    self::assertCount(0, $entity_type_manager->getStorage('taxonomy_vocabulary')->loadMultiple());
  }

}
