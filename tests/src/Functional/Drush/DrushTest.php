<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\Functional\Drush;

use Drupal\Tests\BrowserTestBase;
use Drush\TestTraits\DrushTestTrait;

/**
 * Test Drush commands.
 *
 * @coversDefaultClass \Drupal\reviewer\Drush\Commands\ReviewerCommands
 *
 * @group reviewer
 */
class DrushTest extends BrowserTestBase {

  use DrushTestTrait {
    drush as drushTrait;
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer',
    'reviewer_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the reviewer:run command.
   *
   * @todo Figure out how to test interactive input.
   *
   * @covers ::run
   */
  public function testRun(): void {
    $this->drush('reviewer:run');
    $output = $this->getOutput();
    $this->assertStringContainsString('Test Simple Configuration', $output);
    $this->assertStringContainsString('Test Return Values', $output);
    $this->assertStringContainsString('Test Node: Node (Fail Review)', $output);
    $this->assertStringContainsString('Test Taxonomy: Taxonomy (Fail Review)', $output);
    $this->assertStringNotContainsString('Test Node: Taxonomy (Pass Review)', $output);
    $this->assertStringNotContainsString('Test Taxonomy: Taxonomy (Pass Review)', $output);
    $this->assertStringContainsString('Error', $output);
    $this->assertStringContainsString('Failed', $output);
    $this->assertStringNotContainsString('Not Run', $output);
    $this->assertStringNotContainsString('Pass', $output);
    $this->assertStringNotContainsString('Ignored', $output);

    $this->drush('reviewer:run --show-not-run');
    $output = $this->getOutput();
    $this->assertStringContainsString('Not Run', $output);
    $this->assertStringNotContainsString('Test Node: Node (Pass Review)', $output);
    $this->assertStringNotContainsString('Test Taxonomy: Taxonomy (Pass Review)', $output);
    $this->assertStringNotContainsString('Pass', $output);
    $this->assertStringNotContainsString('Ignored', $output);

    $this->drush('reviewer:run --show-passed');
    $output = $this->getOutput();
    $this->assertStringContainsString('Test Node: Node (Pass Review)', $output);
    $this->assertStringContainsString('Test Taxonomy: Taxonomy (Pass Review)', $output);
    $this->assertStringContainsString('Pass', $output);
    $this->assertStringNotContainsString('Not Run', $output);
    $this->assertStringNotContainsString('Ignored', $output);

    $this->drush('reviewer:run --show-ignored');
    $output = $this->getOutput();
    $this->assertStringContainsString('Ignored Error', $output);
    $this->assertStringContainsString('Ignored Failure', $output);
    $this->assertStringNotContainsString('Not Run', $output);
    $this->assertStringNotContainsString('Pass', $output);

    $this->drush('reviewer:run --show-all');
    $output = $this->getOutput();
    $this->assertStringContainsString('Test Node: Node (Pass Review)', $output);
    $this->assertStringContainsString('Test Taxonomy: Taxonomy (Pass Review)', $output);
    $this->assertStringContainsString('Not Run', $output);
    $this->assertStringContainsString('Pass', $output);
    $this->assertStringContainsString('Ignored Error', $output);
    $this->assertStringContainsString('Ignored Failure', $output);

    $this->drush('reviewer:run test_node:node_fail --show-all');
    $output = $this->getOutput();
    $this->assertStringContainsString('Test Node: Node (Fail Review)', $output);
    $this->assertStringNotContainsString('Test Node: Node (Pass Review)', $output);
    $this->assertStringNotContainsString('Test Taxonomy: Taxonomy (Fail Review)', $output);
    $this->assertStringNotContainsString('Test Taxonomy: Taxonomy (Pass Review)', $output);
    $this->assertStringNotContainsString('Test Simple Configuration', $output);
    $this->assertStringNotContainsString('Test Return Values', $output);

    $this->drush('reviewer:run test_node:node_fail,node_pass test_taxonomy:taxonomy_pass test_return_values --show-all');
    $output = $this->getOutput();
    $this->assertStringContainsString('Test Return Values', $output);
    $this->assertStringContainsString('Test Node: Node (Fail Review)', $output);
    $this->assertStringContainsString('Test Node: Node (Pass Review)', $output);
    $this->assertStringContainsString('Test Taxonomy: Taxonomy (Pass Review)', $output);
    $this->assertStringNotContainsString('Test Taxonomy: Taxonomy (Fail Review)', $output);
    $this->assertStringNotContainsString('Test Simple Configuration', $output);
  }

  /**
   * Test the reviewer:list command.
   *
   * @covers ::list
   */
  public function testList(): void {
    $this->drush('reviewer:list');
    $output = $this->getOutput();
    $this->assertStringContainsString('ID', $output);
    $this->assertStringContainsString('Label', $output);
    $this->assertStringContainsString('test_simple_configuration', $output);
    $this->assertStringContainsString('Test Simple Configuration', $output);
    $this->assertStringContainsString('test_return_values', $output);
    $this->assertStringContainsString('Test Return Values', $output);
    $this->assertStringContainsString('test_taxonomy', $output);
    $this->assertStringContainsString('Test Taxonomy', $output);
    $this->assertStringContainsString('test_node', $output);
    $this->assertStringContainsString('Test Node', $output);

    $this->container->get('module_installer')->uninstall(['reviewer_test']);
    $this->drush('reviewer:list');
    $output = $this->getOutput();
    $this->assertStringContainsString('ID', $output);
    $this->assertStringContainsString('Label', $output);
    $this->assertStringNotContainsString('test_simple_configuration', $output);
    $this->assertStringNotContainsString('Test Simple Configuration', $output);
    $this->assertStringNotContainsString('test_return_values', $output);
    $this->assertStringNotContainsString('Test Return Values', $output);
    $this->assertStringNotContainsString('test_taxonomy', $output);
    $this->assertStringNotContainsString('Test Taxonomy', $output);
    $this->assertStringNotContainsString('test_node', $output);
    $this->assertStringNotContainsString('Test Node', $output);
  }

}
