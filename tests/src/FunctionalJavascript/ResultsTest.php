<?php

declare(strict_types=1);

namespace Drupal\Tests\reviewer\FunctionalJavascript;

use Behat\Mink\Element\NodeElement;
use Behat\Mink\Exception\DriverException;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test the review admin page.
 *
 * @coversDefaultClass \Drupal\reviewer_ui\Form\Results
 *
 * @group reviewer
 *
 * cSpell:ignoreRegExp /dGV[0-9a-zA-Z]+/
 */
class ResultsTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reviewer',
    'reviewer_ui',
    'reviewer_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->container->get('config.factory')
      ->getEditable('system.performance')
      ->set('cache.page.max_age', 0)
      ->save();
    $this->container->get('config.factory')
      ->getEditable('system.site')
      ->set('slogan', '')
      ->save();
    if ($user = $this->drupalCreateUser(['reviewer view results page'])) {
      $this->drupalLogin($user);
    }
  }

  /**
   * Test that the page exists and the expected elements are present.
   */
  public function testElementsPresent(): void {
    $html = $this->getPage();
    self::assertStringContainsString('css/reviewer-ui.css', $html);
    self::assertStringContainsString('js/reviewer-ui.js', $html);
    self::assertStringContainsString('Reviewer Results', $html);
    self::assertStringContainsString('data-drupal-selector="edit-filters"', $html);
    self::assertStringContainsString('data-drupal-selector="edit-test-simple-configuration"', $html);
    self::assertStringContainsString('data-drupal-selector="edit-ignore"', $html);
    self::assertStringContainsString('data-drupal-selector="edit-unignore-all"', $html);
    self::assertStringContainsString('<table class="reviewer-results-table', $html);
    self::assertStringContainsString('Test Simple Configuration', $html);
    self::assertStringContainsString('test_return_values.test_return_values_checklist.array_error', $html);
    self::assertStringContainsString('Tasks must returning an array must have with boolean values, Drupal\reviewer_test\Plugin\reviewer\Checklist\TestReturnValues::arrayError() returned type integer for key "one" instead.', $html);
    self::assertStringContainsString('<span>Passed</span>', $html);
    self::assertStringContainsString('<span>Failed</span>', $html);
    self::assertStringContainsString('<span>Ignored Error</span>', $html);
    self::assertStringContainsString('<span>Errored</span>', $html);
    self::assertStringContainsString('<span>Not Run</span>', $html);
  }

  /**
   * Test that failures show on load.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testFailuresCheckedOnLoad(): void {
    $this->getPage();
    $session = $this->assertSession();
    $session->checkboxChecked('show[failed]');
    $session->checkboxNotChecked('show[all]');
    $session->checkboxNotChecked('show[ignored]');
    $session->checkboxNotChecked('show[passed]');
    $session->checkboxNotChecked('show[not-run]');
  }

  /**
   * Test the interactions between the all checkboxes and others.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testFilterInteractions(): void {
    $this->getPage();
    $session = $this->assertSession();

    $all = $session->elementExists('css', '[name="show[all]"]');
    $failed = $session->elementExists('css', '[name="show[failed]"]');
    $ignored = $session->elementExists('css', '[name="show[ignored]"]');
    $passed = $session->elementExists('css', '[name="show[passed]"]');
    $not_run = $session->elementExists('css', '[name="show[not-run]"]');

    $all->click();
    $session->checkboxChecked('show[all]');
    $session->checkboxChecked('show[failed]');
    $session->checkboxChecked('show[ignored]');
    $session->checkboxChecked('show[passed]');
    $session->checkboxChecked('show[not-run]');

    $failed->click();
    $session->checkboxNotChecked('show[all]');
    $session->checkboxNotChecked('show[failed]');
    $session->checkboxChecked('show[ignored]');
    $session->checkboxChecked('show[passed]');
    $session->checkboxChecked('show[not-run]');

    $failed->click();
    $session->checkboxChecked('show[all]');
    $session->checkboxChecked('show[failed]');
    $session->checkboxChecked('show[ignored]');
    $session->checkboxChecked('show[passed]');
    $session->checkboxChecked('show[not-run]');

    $ignored->click();
    $passed->click();
    $session->checkboxNotChecked('show[all]');
    $session->checkboxChecked('show[failed]');
    $session->checkboxNotChecked('show[ignored]');
    $session->checkboxNotChecked('show[passed]');
    $session->checkboxChecked('show[not-run]');

    $all->click();
    $session->checkboxChecked('show[all]');
    $session->checkboxChecked('show[failed]');
    $session->checkboxChecked('show[ignored]');
    $session->checkboxChecked('show[passed]');
    $session->checkboxChecked('show[not-run]');

    $all->click();
    $session->checkboxNotChecked('show[all]');
    $session->checkboxNotChecked('show[failed]');
    $session->checkboxNotChecked('show[ignored]');
    $session->checkboxNotChecked('show[passed]');
    $session->checkboxNotChecked('show[not-run]');

    $failed->click();
    $ignored->click();
    $passed->click();
    $not_run->click();
    $session->checkboxChecked('show[all]');
    $session->checkboxChecked('show[failed]');
    $session->checkboxChecked('show[ignored]');
    $session->checkboxChecked('show[passed]');
    $session->checkboxChecked('show[not-run]');
  }

  /**
   * Test that the reason field is focused when the ignore checkbox is checked.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function testReasonFocusedOnIgnore(): void {
    $this->getPage();
    $session = $this->assertSession();

    $session->elementExists(
      'css',
      '[data-drupal-selector="edit-test-simple-configuration"]',
    )->click();
    $session->elementExists(
      'css',
      '[name="ignored[dGVzdF9zaW1wbGVfY29uZmlndXJhdGlvbi50ZXN0X3NpbXBsZV9jb25maWd1cmF0aW9uX2NoZWNrbGlzdC5zbG9nYW4=]"]'
    )->click();
    $this->assertJsCondition('document.activeElement === document.querySelector("[name=\"reasons[dGVzdF9zaW1wbGVfY29uZmlndXJhdGlvbi50ZXN0X3NpbXBsZV9jb25maWd1cmF0aW9uX2NoZWNrbGlzdC5zbG9nYW4=]\"]")');
  }

  /**
   * Test that the filters hide and show the correct results.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function testFilter(): void {
    $display = 'display: none;';
    $not_run_class = 'reviewer--not-run';
    $pass_class = 'reviewer--pass';
    $ignored_fail_class = 'reviewer--ignored-failure';
    $ignored_error_class = 'reviewer--ignored-error';
    $fail_class = 'reviewer--fail';
    $error_class = 'reviewer--error';

    $this->getPage();
    $session = $this->assertSession();

    $all = $session->elementExists('css', '[name="show[all]"]');
    $failed = $session->elementExists('css', '[name="show[failed]"]');
    $ignored = $session->elementExists('css', '[name="show[ignored]"]');
    $passed = $session->elementExists('css', '[name="show[passed]"]');
    $not_run = $session->elementExists('css', '[name="show[not-run]"]');

    $results = $this->getSession()->getPage()->findAll('css', '.reviewer-result');
    foreach ($results as $result) {
      if ($result->hasClass($fail_class) || $result->hasClass($error_class)) {
        self::assertFalse($result->hasAttribute('style'));
        if ($details = $this->getDetails($result)) {
          self::assertSame('', $details->getAttribute('style') ?? '');
        }
      }
      else {
        self::assertSame($display, $result->getAttribute('style'));
      }
    }

    $failed->click();
    $results = $this->getSession()->getPage()->findAll('css', '.reviewer-result');
    foreach ($results as $result) {
      self::assertSame($display, $result->getAttribute('style'));
      if ($details = $this->getDetails($result)) {
        self::assertSame($display, $details->getAttribute('style'));
      }
    }

    $all->click();
    $results = $this->getSession()->getPage()->findAll('css', '.reviewer-result');
    foreach ($results as $result) {
      self::assertSame('', $result->getAttribute('style'));
      if ($details = $this->getDetails($result)) {
        self::assertSame('', $details->getAttribute('style'));
      }
    }

    $all->click();
    $ignored->click();
    $results = $this->getSession()->getPage()->findAll('css', '.reviewer-result');
    foreach ($results as $result) {
      if ($result->hasClass($ignored_error_class) || $result->hasClass($ignored_fail_class)) {
        self::assertSame('', $result->getAttribute('style'));
        if ($details = $this->getDetails($result)) {
          self::assertSame('', $details->getAttribute('style'));
        }
      }
      else {
        self::assertSame($display, $result->getAttribute('style'));
      }
    }

    $ignored->click();
    $passed->click();
    $results = $this->getSession()->getPage()->findAll('css', '.reviewer-result');
    foreach ($results as $result) {
      if ($result->hasClass($pass_class)) {
        self::assertSame('', $result->getAttribute('style'));
        if ($details = $this->getDetails($result)) {
          self::assertSame('', $details->getAttribute('style'));
        }
      }
      else {
        self::assertSame($display, $result->getAttribute('style'));
      }
    }

    $passed->click();
    $not_run->click();
    $results = $this->getSession()->getPage()->findAll('css', '.reviewer-result');
    foreach ($results as $result) {
      if ($result->hasClass($not_run_class)) {
        self::assertSame('', $result->getAttribute('style'));
        if ($details = $this->getDetails($result)) {
          self::assertSame('', $details->getAttribute('style'));
        }
      }
      else {
        self::assertSame($display, $result->getAttribute('style'));
      }
    }
  }

  /**
   * Test that submitting the form keeps the filter state.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testSubmitKeepsFilterState(): void {
    $this->getPage();
    $session = $this->assertSession();

    $session->checkboxChecked('show[failed]');
    $session
      ->elementExists('css', '[name="show[ignored]"]')
      ->click();
    $session
      ->elementExists('css', '[name="ignore"]')
      ->click();
    $session->waitForText('Reviewer Results');
    $session->checkboxChecked('show[failed]');
    $session->checkboxChecked('show[ignored]');

    $session
      ->elementExists('css', '[name="unignore_all"]')
      ->click();
    $session->waitForText('Reviewer Results');
    $session->checkboxChecked('show[failed]');
    $session->checkboxChecked('show[ignored]');
  }

  /**
   * Test ignoring and unignoring all items.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testIgnoring(): void {
    $this->getPage();
    $session = $this->assertSession();

    $session->elementExists(
      'css',
      '[data-drupal-selector="edit-test-simple-configuration"]',
    )->click();
    $session->elementExists(
      'css',
      '[name="ignored[dGVzdF9zaW1wbGVfY29uZmlndXJhdGlvbi50ZXN0X3NpbXBsZV9jb25maWd1cmF0aW9uX2NoZWNrbGlzdC5zbG9nYW4=]"]'
    )->click();
    $session->elementExists(
      'css',
      '[name="reasons[dGVzdF9zaW1wbGVfY29uZmlndXJhdGlvbi50ZXN0X3NpbXBsZV9jb25maWd1cmF0aW9uX2NoZWNrbGlzdC5zbG9nYW4=]"]',
    )->setValue('Ignored!!!');
    $session
      ->elementExists('css', '[name="ignore"]')
      ->click();

    $session->waitForText('Reviewer Results');
    $session->checkboxChecked('ignored[dGVzdF9zaW1wbGVfY29uZmlndXJhdGlvbi50ZXN0X3NpbXBsZV9jb25maWd1cmF0aW9uX2NoZWNrbGlzdC5zbG9nYW4=]');
    $session->fieldValueEquals(
      'reasons[dGVzdF9zaW1wbGVfY29uZmlndXJhdGlvbi50ZXN0X3NpbXBsZV9jb25maWd1cmF0aW9uX2NoZWNrbGlzdC5zbG9nYW4=]',
      'Ignored!!!',
    );

    $session
      ->elementExists('css', '[name="unignore_all"]')
      ->click();
    $session->waitForText('Reviewer Results');
    $session->checkboxNotChecked('ignored[dGVzdF9zaW1wbGVfY29uZmlndXJhdGlvbi50ZXN0X3NpbXBsZV9jb25maWd1cmF0aW9uX2NoZWNrbGlzdC5zbG9nYW4=]');
    $session->fieldValueEquals(
      'reasons[dGVzdF9zaW1wbGVfY29uZmlndXJhdGlvbi50ZXN0X3NpbXBsZV9jb25maWd1cmF0aW9uX2NoZWNrbGlzdC5zbG9nYW4=]',
      '',
    );

    $session->checkboxChecked('ignored[dGVzdF9yZXR1cm5fdmFsdWVzLnRlc3RfcmV0dXJuX3ZhbHVlc19jaGVja2xpc3QuYm9vbF9mYWls]');
    $session->fieldDisabled('ignored[dGVzdF9yZXR1cm5fdmFsdWVzLnRlc3RfcmV0dXJuX3ZhbHVlc19jaGVja2xpc3QuYm9vbF9mYWls]');
    $session->fieldValueEquals(
      'reasons[dGVzdF9yZXR1cm5fdmFsdWVzLnRlc3RfcmV0dXJuX3ZhbHVlc19jaGVja2xpc3QuYm9vbF9mYWls]',
      'Ignored in code.',
    );
    $session->fieldDisabled('reasons[dGVzdF9yZXR1cm5fdmFsdWVzLnRlc3RfcmV0dXJuX3ZhbHVlc19jaGVja2xpc3QuYm9vbF9mYWls]');
  }

  /**
   * Load the results page.
   */
  private function getPage(): string {
    return $this->drupalGet('/admin/reports/reviewer');
  }

  /**
   * Get the details element containing another element.
   */
  private function getDetails(NodeElement $element): NodeElement|null {
    try {
      while ($element->getTagName() !== 'details') {
        $element = $element->getParent();
      }
    }
    catch (DriverException $e) {
      return NULL;
    }
    return $element;
  }

}
