<?php

declare(strict_types=1);

namespace Drupal\reviewer_test\Plugin\reviewer\Checklist;

use Drupal\reviewer\Attribute\Checklist;
use Drupal\reviewer\Plugin\reviewer\Checklist\ChecklistBase;

/**
 * Test task return values.
 */
#[Checklist(id: 'test_return_values_checklist', label: 'Test Return Values')]
final class TestReturnValues extends ChecklistBase {

  /**
   * {@inheritdoc}
   */
  public function tasks(): array {
    return [
      $this->taskFactory->create('array_valid', $this->arrayValid(...), 'Failures: @failures'),
      $this->taskFactory->create('array_error', $this->arrayError(...), 'Array error'),
      $this->taskFactory->create('bool_pass', $this->booleanPass(...), 'Pass'),
      $this->taskFactory->create('bool_fail', $this->booleanFail(...), 'Fail'),
      $this->taskFactory->create('error', $this->error(...), 'Return error'),
      $this->taskFactory->create('null', $this->null(...), 'Return null'),
    ];
  }

  /**
   * This check returns a valid array.
   *
   * @return array<string, bool>
   */
  private function arrayValid(): array {
    return [
      'one' => FALSE,
      'two' => TRUE,
      'three' => FALSE,
    ];
  }

  /**
   * This check returns an invalid array.
   *
   * @return array<string, int|bool>
   */
  private function arrayError(): array {
    return [
      'one' => 1,
      'two' => TRUE,
      'three' => FALSE,
    ];
  }

  /**
   * This check returns a pass.
   */
  private function booleanPass(): bool {
    return TRUE;
  }

  /**
   * This check returns a failure.
   */
  private function booleanFail(): bool {
    return FALSE;
  }

  /**
   * This check returns an invalid value.
   */
  private function error(): string {
    return 'hello';
  }

  /**
   * This check returns null.
   */
  private function null(): null {
    return NULL;
  }

}
