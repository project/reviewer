<?php

declare(strict_types=1);

namespace Drupal\reviewer_test\Plugin\reviewer\Checklist;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\reviewer\Attribute\Checklist;
use Drupal\reviewer\Plugin\reviewer\Checklist\ChecklistBase;
use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;
use Drupal\reviewer\Reviewer\Task\TaskFactoryInterface;

/**
 * Test entity view displays values.
 */
#[Checklist(
  id: 'test_entity_view_display_checklist',
  label: 'Test Entity View Display',
)]
final class TestEntityDisplay extends ChecklistBase {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TaskFactoryInterface $taskFactory,
    private readonly EntityDisplayRepositoryInterface $entityDisplayRepository,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $taskFactory);
  }

  /**
   * {@inheritdoc}
   */
  public function tasks(): array {
    return [
      $this->taskFactory->create('label_visible', $this->labelHidden(...), 'Visible field labels for fields: @failures.'),
      $this->taskFactory->create('link_trimmed', $this->linkNotTrimmed(...), 'Link text output is trimmed on the following fields: @failures'),
    ];
  }

  /**
   * Test that field labels are hidden.
   *
   * @return array<string, bool>|bool
   *
   * @throws \ErrorException
   */
  private function labelHidden(TaskConfigInterface $config): array|bool {
    $results = [];
    foreach ($this->viewDisplays($config->getEntity()) as $display) {
      foreach ($display->getComponents() as $field => $settings) {
        if (isset($settings['label'])) {
          $results["{$display->getMode()}.$field"] = $settings['label'] === 'hidden';
        }
      }
    }
    return $results ?: TRUE;
  }

  /**
   * Test that link text is not trimmed.
   *
   * @return array<string, bool>|bool
   *
   * @throws \ErrorException
   */
  private function linkNotTrimmed(TaskConfigInterface $config): array|bool {
    $results = [];
    foreach ($this->viewDisplays($config->getEntity()) as $display) {
      foreach ($display->getComponents() as $field => $settings) {
        if (isset($settings['type']) && $settings['type'] === 'link') {
          $results[$field] = $settings['settings']['trim_length'] === NULL;
        }
      }
    }
    return $results ?: TRUE;
  }

  /**
   * Get view displays for an entity.
   *
   * @return array<string, \Drupal\Core\Entity\Display\EntityViewDisplayInterface>
   */
  private function viewDisplays(ConfigEntityInterface $entity): array {
    if (
      !$entity->getEntityType()->getBundleOf()
      || !$entity->getOriginalId()
    ) {
      return [];
    }

    $enabled_displays = array_keys($this->entityDisplayRepository->getViewModeOptionsByBundle(
      $entity->getEntityType()->getBundleOf(),
      (string) $entity->getOriginalId(),
    ));

    $displays = [];
    foreach ($enabled_displays as $mode) {
      $displays[$mode] = $this->entityDisplayRepository->getViewDisplay(
        $entity->getEntityType()->getBundleOf(),
        (string) $entity->getOriginalId(),
        $mode,
      );
    }
    return $displays;
  }

}
