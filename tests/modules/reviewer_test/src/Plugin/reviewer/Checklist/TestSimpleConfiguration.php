<?php

declare(strict_types=1);

namespace Drupal\reviewer_test\Plugin\reviewer\Checklist;

use Drupal\reviewer\Attribute\Checklist;
use Drupal\reviewer\Plugin\reviewer\Checklist\ChecklistBase;
use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;

/**
 * Test simple configuration values.
 */
#[Checklist(
  id: 'test_simple_configuration_checklist',
  label: 'Test Simple Configuration',
)]
final class TestSimpleConfiguration extends ChecklistBase {

  /**
   * {@inheritdoc}
   */
  public function tasks(): array {
    return [
      $this->taskFactory->create('slogan', $this->slogan(...), 'Site slogan not set.'),
      $this->taskFactory->create('cache_max_age', $this->cacheMaxAge(...), 'Cache max age set to 0.'),
    ];
  }

  /**
   * Test that the site slogan is set.
   *
   * @throws \ErrorException
   */
  private function slogan(TaskConfigInterface $config): bool {
    $config = $config->getConfigOfTypeImmutableById('system.site');
    return $config->get('slogan') !== '';
  }

  /**
   * Test that the site slogan is set.
   *
   * @throws \ErrorException
   */
  private function cacheMaxAge(TaskConfigInterface $config): bool {
    $config = $config->getConfigOfTypeImmutableById('system.performance');
    return $config->get('cache.page.max_age') !== 0;
  }

}
