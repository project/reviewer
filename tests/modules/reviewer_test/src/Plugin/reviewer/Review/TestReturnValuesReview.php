<?php

declare(strict_types=1);

namespace Drupal\reviewer_test\Plugin\reviewer\Review;

use Drupal\reviewer\Attribute\Review;
use Drupal\reviewer\Plugin\reviewer\Review\ReviewBase;

/**
 * Test return values review.
 */
#[Review(
  id: 'test_return_values',
  label: 'Test Return Values',
  checklists: [
    'test_return_values_checklist',
  ],
  ignored: [
    ['id' => 'test_return_values.test_return_values_checklist.bool_fail', 'reason' => 'Ignored in code.'],
    ['id' => 'test_return_values.test_return_values_checklist.error', 'reason' => 'Ignored in code, again.'],
  ],
)]
final class TestReturnValuesReview extends ReviewBase {

}
