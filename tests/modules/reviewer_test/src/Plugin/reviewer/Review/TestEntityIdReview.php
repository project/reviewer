<?php

declare(strict_types=1);

namespace Drupal\reviewer_test\Plugin\reviewer\Review;

use Drupal\reviewer\Attribute\Review;
use Drupal\reviewer\Plugin\reviewer\Review\ReviewBase;

/**
 * Test entity ID review.
 */
#[Review(
  id: 'test_entity_id',
  label: 'Test Entity ID',
  checklists: [
    'test_entity_view_display_checklist',
  ],
  entity_id: 'node.type.node_fail',
)]
class TestEntityIdReview extends ReviewBase {

}
