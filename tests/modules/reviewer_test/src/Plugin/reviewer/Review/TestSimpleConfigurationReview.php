<?php

declare(strict_types=1);

namespace Drupal\reviewer_test\Plugin\reviewer\Review;

use Drupal\reviewer\Attribute\Review;
use Drupal\reviewer\Plugin\reviewer\Review\ReviewBase;

/**
 * Test simple configuration review.
 */
#[Review(
  id: 'test_simple_configuration',
  label: 'Test Simple Configuration',
  checklists: [
    'test_simple_configuration_checklist',
  ],
  configuration: [
    'system.performance',
    'system.site',
  ],
)]
final class TestSimpleConfigurationReview extends ReviewBase {

}
