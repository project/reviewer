<?php

declare(strict_types=1);

namespace Drupal\reviewer_test\Plugin\reviewer\Review;

use Drupal\reviewer\Attribute\Review;
use Drupal\reviewer\Plugin\reviewer\Review\ReviewBase;

/**
 * Test taxonomy review.
 */
#[Review(
  id: 'test_taxonomy',
  label: 'Test Taxonomy',
  checklists: [
    'test_entity_view_display_checklist',
  ],
  entity_type: 'taxonomy_vocabulary',
)]
final class TestTaxonomyReview extends ReviewBase {

}
