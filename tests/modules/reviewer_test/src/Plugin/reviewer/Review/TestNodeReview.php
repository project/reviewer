<?php

declare(strict_types=1);

namespace Drupal\reviewer_test\Plugin\reviewer\Review;

use Drupal\reviewer\Attribute\Review;
use Drupal\reviewer\Plugin\reviewer\Review\ReviewBase;

/**
 * Test node review.
 */
#[Review(
  id: 'test_node',
  label: 'Test Node',
  checklists: [
    'test_entity_view_display_checklist',
  ],
  entity_type: 'node_type',
  bundles: [
    'node_pass',
    'node_fail',
  ],
)]
final class TestNodeReview extends ReviewBase {

}
