<?php

declare(strict_types=1);

namespace Drupal\reviewer\Plugin\reviewer\Checklist;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\reviewer\Plugin\ContainerFactoryPluginAutowireTrait;
use Drupal\reviewer\Reviewer\Task\TaskFactoryInterface;

/**
 * Base checklist plugin.
 */
abstract class ChecklistBase extends PluginBase implements ChecklistPluginInterface, ContainerFactoryPluginInterface {

  use ContainerFactoryPluginAutowireTrait;

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly TaskFactoryInterface $taskFactory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->configuration['label'];
  }

}
