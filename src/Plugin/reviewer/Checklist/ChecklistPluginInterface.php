<?php

namespace Drupal\reviewer\Plugin\reviewer\Checklist;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\reviewer\Plugin\reviewer\BuildableInterface;
use Drupal\reviewer\Reviewer\Task\TaskProviderInterface;

/**
 * Defines a checklist plugin.
 */
interface ChecklistPluginInterface extends TaskProviderInterface, BuildableInterface, PluginInspectionInterface {

  /**
   * Get the human-readable label.
   */
  public function getLabel(): string;

}
