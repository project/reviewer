<?php

namespace Drupal\reviewer\Plugin\reviewer\Review;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\reviewer\Plugin\reviewer\BuildableInterface;

/**
 * Defines a review plugin which reviews configuration.
 */
interface ReviewPluginInterface extends BuildableInterface, PluginInspectionInterface {

  /**
   * Get the human-readable label.
   */
  public function getLabel(): string;

  /**
   * Get the checklist plugin IDs this review should run.
   *
   * @return string[]
   *
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   *   Thrown when no checklists are defined on the review.
   */
  public function getChecklists(): array;

  /**
   * Get the specific entity ID to review.
   */
  public function getEntityType(): string|null;

  /**
   * Get the type of entity to review.
   */
  public function getEntityId(): string|null;

  /**
   * Get the bundles of the entity type to review.
   *
   * @return string[]
   */
  public function getBundles(): array;

  /**
   * Get configuration IDs to load when running the review.
   *
   * @return string[]
   */
  public function getConfiguration(): array;

  /**
   * Get failed task IDs to ignore.
   *
   * @return array{id: string, reason: string}[]
   */
  public function getIgnored(): array;

}
