<?php

declare(strict_types=1);

namespace Drupal\reviewer\Plugin\reviewer\Review;

use Drupal\Core\Plugin\PluginBase;
use Drupal\reviewer\Exception\NoChecklistsException;

/**
 * Base review plugin.
 */
abstract class ReviewBase extends PluginBase implements ReviewPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->configuration['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getChecklists(): array {
    return $this->configuration['checklists'] ?? throw new NoChecklistsException($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityId(): string|null {
    return $this->configuration['entity_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityType(): string|null {
    return $this->configuration['entity_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function getBundles(): array {
    return $this->configuration['bundles'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration['configuration'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getIgnored(): array {
    return $this->configuration['ignored'] ?? [];
  }

}
