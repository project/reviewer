<?php

declare(strict_types=1);

namespace Drupal\reviewer\Plugin;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\AutowiringFailedException;

/**
 * Trait to autowire plugins.
 *
 * @todo Remove when implemented by Drupal.
 * @see https://www.drupal.org/project/drupal/issues/3294266
 */
trait ContainerFactoryPluginAutowireTrait {

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): self {
    $args = [];

    $constructor = new \ReflectionMethod(static::class, '__construct');
    $parameters = \array_slice($constructor->getParameters(), 3, NULL, TRUE);

    foreach ($parameters as $parameter) {
      $service = (string) $parameter->getType();

      if (!$container->has($service)) {
        throw new AutowiringFailedException($service, sprintf(
          'Cannot autowire service "%s": argument "$%s" of method "%s::_construct()", you should configure its value explicitly.',
          $service,
          $parameter->getName(),
          static::class,
        ));
      }

      $args[] = $container->get($service);
    }

    return new static($configuration, $plugin_id, $plugin_definition, ...$args);
  }

}
