<?php

declare(strict_types=1);

namespace Drupal\reviewer\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\reviewer\Attribute\Checklist;
use Drupal\reviewer\Plugin\reviewer\Checklist\ChecklistPluginInterface;

/**
 * Create checklist plugins.
 */
class ChecklistPluginManager extends DefaultPluginManager implements ChecklistManagerInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/reviewer/Checklist',
      $namespaces,
      $module_handler,
      ChecklistPluginInterface::class,
      Checklist::class,
    );
    $this->setCacheBackend($cache_backend, 'reviewer_checklist_plugins');
    $this->alterInfo('reviewer_checklist');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function createAllInstances(): array {
    return array_map(
      fn(array $definition) => $this->createInstance($definition['id'], $definition),
      $this->getDefinitions(),
    );
  }

}
