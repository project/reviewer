<?php

namespace Drupal\reviewer\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Definition for a checklist plugin manager.
 */
interface ChecklistManagerInterface extends PluginManagerInterface {

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\reviewer\Plugin\reviewer\Checklist\ChecklistPluginInterface
   */
  public function createInstance($plugin_id, array $configuration = []);

  /**
   * Create instances for all checklist plugins.
   *
   * @return array<string, \Drupal\reviewer\Plugin\reviewer\Checklist\ChecklistPluginInterface>
   */
  public function createAllInstances(): array;

}
