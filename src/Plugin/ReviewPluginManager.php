<?php

declare(strict_types=1);

namespace Drupal\reviewer\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\reviewer\Attribute\Review;
use Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface;

/**
 * Creates review plugins.
 */
class ReviewPluginManager extends DefaultPluginManager implements ReviewManagerInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/reviewer/Review',
      $namespaces,
      $module_handler,
      ReviewPluginInterface::class,
      Review::class,
    );
    $this->setCacheBackend($cache_backend, 'reviewer_review_plugins');
    $this->alterInfo('reviewer_review');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function createAllInstances(): array {
    return array_map(
      fn(array $definition) => $this->createInstance($definition['id'], $definition),
      $this->getDefinitions(),
    );
  }

}
