<?php

namespace Drupal\reviewer\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Definition for a review plugin manager.
 */
interface ReviewManagerInterface extends PluginManagerInterface {

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface
   */
  public function createInstance($plugin_id, array $configuration = []);

  /**
   * Create instances for all checklist plugins.
   *
   * @return array<string, \Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface>
   */
  public function createAllInstances(): array;

}
