<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface;
use Drupal\reviewer\Reviewer\Review\ReviewInterface;

/**
 * Evaluates bundles.
 */
final readonly class BundleEvaluator implements BundleEvaluatorInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    private EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function allBundles(ReviewPluginInterface $review_plugin): array {
    if ($bundles = $review_plugin->getBundles()) {
      return $bundles;
    }

    $entity_type_id = $review_plugin->getEntityType();
    if (
      !$entity_type_id
      && $review_plugin->getEntityId()
    ) {
      return [$review_plugin->getEntityId()];
    }

    if (!$entity_type_id) {
      return [];
    }

    $entity_type = $this
      ->entityTypeManager
      ->getStorage($entity_type_id)
      ->getEntityType();

    $bundle = $entity_type->getBundleOf();
    if (!$bundle) {
      return [];
    }

    $bundles = $this
      ->entityTypeBundleInfo
      ->getBundleInfo($bundle);

    return \count($bundles) === 1 && array_key_first($bundles) === $review_plugin->getEntityType()
      ? []
      : array_keys($bundles);
  }

  /**
   * {@inheritdoc}
   */
  public function reviewHasBundle(
    ReviewInterface $review,
    array $bundles,
  ): bool {
    return str_contains($review->getId(), '.')
      && \in_array(explode('.', $review->getId())[1], $bundles, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function reviewsOfBundles(
    array $reviews,
    array $bundles,
  ): array {
    return $bundles
      ? array_filter(
        $reviews,
        fn(ReviewInterface $review) => $this->reviewHasBundle($review, $bundles),
      )
      : $reviews;
  }

}
