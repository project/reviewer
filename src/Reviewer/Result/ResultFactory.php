<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Result;

use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\IgnorerInterface;
use Drupal\reviewer\Reviewer\Status\Status;
use Drupal\reviewer\Reviewer\Status\StatusFactoryInterface;

/**
 * Creates results.
 */
final readonly class ResultFactory implements ResultFactoryInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private IgnorerInterface $ignorer,
    private StatusFactoryInterface $statusFactory,
  ) {}

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\reviewer\Exception\NotIgnoredException
   */
  public function create(
    string $id,
    Type $type,
    Status $status,
    IgnoredTasksInterface $ignored,
    string $message = '',
  ): ResultInterface {
    $is_ignored = $this->ignorer->isIgnored($id);
    return new Result(
      $id,
      $type,
      $is_ignored ? $this->statusFactory->createIgnored($status) : $status,
      $message,
      $is_ignored ? $this->ignorer->ignoredReason($id) : '',
    );
  }

}
