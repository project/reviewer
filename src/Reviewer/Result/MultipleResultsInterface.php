<?php

namespace Drupal\reviewer\Reviewer\Result;

/**
 * Definition for a runnable object which can return multiple results.
 */
interface MultipleResultsInterface {

  /**
   * Get a collection of all results for this item.
   *
   * @throws \Drupal\reviewer\Exception\NotRunException
   *   Thrown when results are accessed before the item is run.
   */
  public function results(): ResultCollectionInterface;

}
