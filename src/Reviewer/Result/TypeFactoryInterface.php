<?php

namespace Drupal\reviewer\Reviewer\Result;

/**
 * Defines a factory which creates types.
 */
interface TypeFactoryInterface {

  /**
   * Create an individual type.
   */
  public function createIndividual(): Type;

  /**
   * Create a summary type.
   */
  public function createSummary(): Type;

}
