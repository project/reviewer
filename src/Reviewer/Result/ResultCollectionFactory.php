<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Result;

use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\RunnableInterface;

/**
 * Creates result collections.
 */
class ResultCollectionFactory implements ResultCollectionFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function create(
    MultipleResultsInterface & RunnableInterface & IgnoredTasksInterface $ran,
    ResultInterface $result,
    array $results,
  ): ResultCollectionInterface {
    return new ResultCollection($ran, $result, $results);
  }

}
