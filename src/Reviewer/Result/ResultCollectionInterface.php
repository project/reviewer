<?php

namespace Drupal\reviewer\Reviewer\Result;

use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\RunnableInterface;

/**
 * Defines a collection of results.
 */
interface ResultCollectionInterface {

  /**
   * The item which was run to generate the collection.
   */
  public function ran(): RunnableInterface&MultipleResultsInterface&IgnoredTasksInterface;

  /**
   * Get tho single result that describes all results in the collection.
   */
  public function result(): ResultInterface;

  /**
   * Get all results from the collection.
   *
   * This can be a set of more collections or raw results.
   *
   * @return array<string, \Drupal\reviewer\Reviewer\Result\ResultCollectionInterface|\Drupal\reviewer\Reviewer\Result\ResultInterface>
   */
  public function results(): array;

  /**
   * Return all results contained in this collection or contained collections.
   *
   * @return array<string, \Drupal\reviewer\Reviewer\Result\ResultInterface>
   */
  public function individualResults(): array;

}
