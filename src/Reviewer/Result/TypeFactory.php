<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Result;

/**
 * Creates types.
 */
class TypeFactory implements TypeFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createIndividual(): Type {
    return Type::Individual;
  }

  /**
   * {@inheritdoc}
   */
  public function createSummary(): Type {
    return Type::Summary;
  }

}
