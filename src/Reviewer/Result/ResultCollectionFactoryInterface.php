<?php

namespace Drupal\reviewer\Reviewer\Result;

use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\RunnableInterface;

/**
 * Defines a factory which creates result collections.
 */
interface ResultCollectionFactoryInterface {

  /**
   * Create a result collection.
   *
   * @param array<string, \Drupal\reviewer\Reviewer\Result\ResultCollectionInterface|\Drupal\reviewer\Reviewer\Result\ResultInterface> $results
   */
  public function create(
    MultipleResultsInterface & RunnableInterface & IgnoredTasksInterface $ran,
    ResultInterface $result,
    array $results,
  ): ResultCollectionInterface;

}
