<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Result;

/**
 * Represents the type of result.
 */
enum Type {

  case Individual;
  case Summary;

}
