<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Result;

use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\RunnableInterface;

/**
 * A collection of results.
 */
final readonly class ResultCollection implements ResultCollectionInterface {

  /**
   * Constructor.
   *
   * @param array<string, \Drupal\reviewer\Reviewer\Result\ResultCollectionInterface|\Drupal\reviewer\Reviewer\Result\ResultInterface> $results
   */
  public function __construct(
    private RunnableInterface & MultipleResultsInterface & IgnoredTasksInterface $ran,
    private ResultInterface $result,
    private array $results,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function ran(): RunnableInterface & MultipleResultsInterface & IgnoredTasksInterface {
    return $this->ran;
  }

  /**
   * {@inheritdoc}
   */
  public function result(): ResultInterface {
    return $this->result;
  }

  /**
   * {@inheritdoc}
   */
  public function results(): array {
    return $this->results;
  }

  /**
   * {@inheritdoc}
   */
  public function individualResults(): array {
    $results = $this->results;
    if (reset($results) instanceof ResultInterface) {
      /** @var \Drupal\reviewer\Reviewer\Result\ResultInterface[] $results */
      return $results;
    }

    $results = [];
    /** @var \Drupal\reviewer\Reviewer\Result\ResultCollectionInterface $result */
    foreach ($this->results as $result) {
      $results = array_merge($results, $result->individualResults());
    }
    return $results;
  }

}
