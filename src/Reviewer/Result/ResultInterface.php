<?php

namespace Drupal\reviewer\Reviewer\Result;

use Drupal\reviewer\Reviewer\Status\Status;

/**
 * Defines a result.
 */
interface ResultInterface {

  /**
   * Get the result ID.
   */
  public function getId(): string;

  /**
   * Get the type of result.
   */
  public function getType(): Type;

  /**
   * Get the status of the result.
   */
  public function getStatus(): Status;

  /**
   * Set a status override.
   */
  public function overrideStatus(Status $status): void;

  /**
   * Get the result message.
   *
   * This method will return the message override if available, then the
   * original message, and then the status label if neither exist.
   */
  public function message(): string;

  /**
   * Get the original result message.
   */
  public function getOriginalMessage(): string;

  /**
   * Get the original result message.
   */
  public function getOverrideMessage(): string;

  /**
   * Set a message override.
   */
  public function setOverrideMessage(string $message): void;

}
