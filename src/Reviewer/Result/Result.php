<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Result;

use Drupal\reviewer\Reviewer\Status\Status;

/**
 * A result.
 */
final class Result implements ResultInterface {

  private Status|null $overrideStatus = NULL;

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private readonly string $id,
    private readonly Type $type,
    private readonly Status $status,
    private readonly string $originalMessage = '',
    private string $overrideMessage = '',
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): Type {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): Status {
    return $this->overrideStatus ?? $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function overrideStatus(Status $status): void {
    $this->overrideStatus = $status;
  }

  /**
   * {@inheritdoc}
   */
  public function message(): string {
    return $this->overrideMessage ?: ($this->originalMessage ?: $this->status->label());
  }

  /**
   * {@inheritdoc}
   */
  public function getOriginalMessage(): string {
    return $this->originalMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function getOverrideMessage(): string {
    return $this->overrideMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function setOverrideMessage(string $message): void {
    $this->overrideMessage = $message;
  }

}
