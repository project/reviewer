<?php

namespace Drupal\reviewer\Reviewer\Result;

use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\Status\Status;

/**
 * Defines a factory which creates results.
 */
interface ResultFactoryInterface {

  /**
   * Create a result.
   */
  public function create(
    string $id,
    Type $type,
    Status $status,
    IgnoredTasksInterface $ignored,
    string $message = '',
  ): ResultInterface;

}
