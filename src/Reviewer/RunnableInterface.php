<?php

namespace Drupal\reviewer\Reviewer;

use Drupal\reviewer\Reviewer\Result\ResultInterface;

/**
 * Definition for an item with runnable checks.
 */
interface RunnableInterface {

  /**
   * Run the checks.
   */
  public function run(): self;

  /**
   * Generate a unique ID for the check results.
   */
  public function resultId(string $variant = ''): string;

  /**
   * Get the result of the checks.
   *
   * @throws \Drupal\reviewer\Exception\NotRunException
   *   Thrown when the result is accessed before the item is run.
   */
  public function result(): ResultInterface;

}
