<?php

namespace Drupal\reviewer\Reviewer\Checklist;

use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\Result\MultipleResultsInterface;
use Drupal\reviewer\Reviewer\Review\ReviewInterface;
use Drupal\reviewer\Reviewer\RunnableInterface;
use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;
use Drupal\reviewer\Reviewer\Task\TaskProviderInterface;

/**
 * Defines a checklist.
 */
interface ChecklistInterface extends RunnableInterface, MultipleResultsInterface, TaskProviderInterface, IgnoredTasksInterface {

  /**
   * Get the checklist ID.
   */
  public function getId(): string;

  /**
   * Get the checklist label.
   */
  public function getLabel(): string;

  /**
   * Get configuration entities loaded for this checklist.
   */
  public function getConfiguration(): TaskConfigInterface|null;

  /**
   * Set the review that contains this checklist.
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   *   Thrown attempting to set a review when a review is already set.
   */
  public function setReview(ReviewInterface $review): ChecklistInterface;

}
