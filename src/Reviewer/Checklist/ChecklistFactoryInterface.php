<?php

namespace Drupal\reviewer\Reviewer\Checklist;

use Drupal\reviewer\Reviewer\Review\ReviewInterface;

/**
 * Defines a factory which creates checklists.
 */
interface ChecklistFactoryInterface {

  /**
   * Create a checklist.
   *
   * @param \Drupal\reviewer\Reviewer\Task\TaskInterface[] $tasks
   */
  public function create(
    string $id,
    string $label,
    array $tasks,
    ReviewInterface|null $review = NULL,
  ): ChecklistInterface;

}
