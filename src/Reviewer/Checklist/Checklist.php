<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Checklist;

use Drupal\reviewer\Exception\CannotOverridePropertyException;
use Drupal\reviewer\Exception\NotRunException;
use Drupal\reviewer\Reviewer\Result\ResultCollectionFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultCollectionInterface;
use Drupal\reviewer\Reviewer\Result\ResultFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\Result\TypeFactoryInterface;
use Drupal\reviewer\Reviewer\Review\ReviewInterface;
use Drupal\reviewer\Reviewer\Status\StatusFactoryInterface;
use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;

/**
 * A checklist.
 */
final class Checklist implements ChecklistInterface {

  private ResultCollectionInterface|null $result = NULL;

  /**
   * Constructor.
   *
   * @param \Drupal\reviewer\Reviewer\Task\TaskInterface[] $tasks
   */
  public function __construct(
    private readonly string $id,
    private readonly string $label,
    private readonly array $tasks,
    private readonly ResultCollectionFactoryInterface $resultCollectionFactory,
    private readonly ResultFactoryInterface $resultFactory,
    private readonly StatusFactoryInterface $statusFactory,
    private readonly TypeFactoryInterface $typeFactory,
    private ReviewInterface|null $review = NULL,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function run(): ChecklistInterface {
    $results = [];

    foreach ($this->tasks() as $task) {
      $task->run()->result();
      foreach ($task->results()->individualResults() as $task_result) {
        $results[$task_result->getId()] = $task_result;
      }
    }

    $status = $this->statusFactory->createMostSevere($results);
    $message = $status->label();

    $result = $this->resultFactory->create(
      $this->resultId(),
      $this->typeFactory->createSummary(),
      $status,
      $this,
      $message,
    );

    $this->result = $this->resultCollectionFactory->create($this, $result, $results);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function resultId(string $variant = ''): string {
    return $this->review ? "{$this->review->resultId()}.$this->id" : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): TaskConfigInterface|null {
    return $this->review?->getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function setReview(ReviewInterface $review): ChecklistInterface {
    if ($this->review) {
      throw new CannotOverridePropertyException('checklist', $this::class);
    }
    $this->review = $review;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function tasks(): array {
    return $this->tasks;
  }

  /**
   * {@inheritdoc}
   */
  public function result(): ResultInterface {
    return $this->results()->result();
  }

  /**
   * {@inheritdoc}
   */
  public function results(): ResultCollectionInterface {
    return $this->result ?? throw new NotRunException($this->getId());
  }

  /**
   * {@inheritdoc}
   */
  public function getIgnored(): array {
    return $this->review?->getIgnored() ?? throw new \LogicException(sprintf(
      'Cannot get ignored items because the review is not set in %s.',
      $this->getId(),
    ));
  }

}
