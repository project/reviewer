<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Checklist;

use Drupal\reviewer\Reviewer\Result\ResultCollectionFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultFactoryInterface;
use Drupal\reviewer\Reviewer\Result\TypeFactoryInterface;
use Drupal\reviewer\Reviewer\Review\ReviewInterface;
use Drupal\reviewer\Reviewer\Status\StatusFactoryInterface;

/**
 * Creates checklists.
 */
final readonly class ChecklistFactory implements ChecklistFactoryInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private ResultCollectionFactoryInterface $resultCollectionFactory,
    private ResultFactoryInterface $resultFactory,
    private StatusFactoryInterface $statusFactory,
    private TypeFactoryInterface $typeFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function create(
    string $id,
    string $label,
    array $tasks,
    ReviewInterface|null $review = NULL,
  ): ChecklistInterface {
    return new Checklist(
      $id,
      $label,
      $tasks,
      $this->resultCollectionFactory,
      $this->resultFactory,
      $this->statusFactory,
      $this->typeFactory,
      $review,
    );
  }

}
