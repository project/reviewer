<?php

namespace Drupal\reviewer\Reviewer;

/**
 * Definition for as item that supports ignored tasks.
 */
interface IgnoredTasksInterface {

  /**
   * Get task IDs to ignore.
   *
   * @return array{id: string, reason: string}[]
   *
   * @throws \LogicException
   *   Thrown when the review containing the ignored items is not set.
   */
  public function getIgnored(): array;

}
