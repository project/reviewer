<?php

namespace Drupal\reviewer\Reviewer;

use Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface;
use Drupal\reviewer\Reviewer\Review\ReviewInterface;

/**
 * Defines a class which evaluates review bundles.
 */
interface BundleEvaluatorInterface {

  /**
   * Get all bundles of an entity review plugin.
   *
   * @return string[]
   */
  public function allBundles(ReviewPluginInterface $review_plugin): array;

  /**
   * Check if a review is in a list of bundles.
   *
   * @param string[] $bundles
   */
  public function reviewHasBundle(
    ReviewInterface $review,
    array $bundles,
  ): bool;

  /**
   * Get reviews matching a set of bundles.
   *
   * @param array<string, \Drupal\reviewer\Reviewer\Review\ReviewInterface> $reviews
   * @param string[] $bundles
   *
   * @return array<string, \Drupal\reviewer\Reviewer\Review\ReviewInterface>
   */
  public function reviewsOfBundles(array $reviews, array $bundles): array;

}
