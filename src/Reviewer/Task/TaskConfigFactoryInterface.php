<?php

namespace Drupal\reviewer\Reviewer\Task;

use Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface;

/**
 * Defines a factory which creates task configuration value objects.
 */
interface TaskConfigFactoryInterface {

  /**
   * Load configuration defined on the review plugin's attributes.
   */
  public function create(
    ReviewPluginInterface $review,
    string $bundle = '',
  ): TaskConfigInterface;

}
