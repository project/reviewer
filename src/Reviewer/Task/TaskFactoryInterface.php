<?php

namespace Drupal\reviewer\Reviewer\Task;

use Drupal\reviewer\Reviewer\Checklist\ChecklistInterface;

/**
 * Defines a factory which creates tasks.
 *
 * Example of a basic task which returns one result:
 * @code
 * TaskFactoryInterface::create(
 *   'slogan',
 *   reviewer_check_slogan(...),
 *   'Site slogan not set.',
 * );
 *
 * function reviewer_check_slogan(array $configuration): bool {
 *   $config->getConfigOfTypeImmutableById('system.site');
 *   return $config->get('slogan') !== '';
 * }
 * @endcode
 *
 * Example of a task which returns multiple results:
 * @code
 * TaskFactoryInterface::create(
 *   'field_label_visible',
 *   reviewer_check_visible_field_label(...),
 *   'Field label visible for fields: @failures.',
 * );
 *
 * function reviewer_check_visible_field_label(array $configuration): array|bool {
 *   // Logic to load entity view displays.
 *   $results = [];
 *   foreach ($displays as $display) {
 *     foreach ($display->getComponents() as $field => $settings) {
 *       if (isset($settings['label'])) {
 *         $results["{$display->getMode()}.$field"] = $settings['label'] === 'hidden';
 *       }
 *     }
 *   }
 *   return $results ?: TRUE;
 * }
 * @endcode
 *
 * When creating tasks from a checklist $check closures can be methods on the
 * checklist or organized into classes:
 * @code
 * $this->taskFactory->create(
 *   'method_on_checklist',
 *   $this->checklistMethod(...),
 *   'Check failed.',
 * );
 *
 * private function checklistMethod(array $configuration): array|bool {
 *   // Checks
 * }
 *
 * $this->taskFactory->create(
 *    'static_method',
 *    StaticClass::checkMethod(...),
 *   'Check failed.',
 *  );
 *
 * $this->taskFactory->create(
 *   'static_method',
 *   $this->myService->checkMethod(...),
 *  'Check failed.',
 * );
 * @endcode
 */
interface TaskFactoryInterface {

  /**
   * Create a task.
   *
   * @param string $id
   *   The task unique ID.
   * @param \Closure $check
   *   The check to run. Requirements:
   *   - The check's return signature must be array<string, bool>|bool|null.
   *   - Tasks which only check one item it may return a boolean.
   *   - Tasks which check multiple items (all fields on an entity type, for
   *     instance) may return an array for each item, keyed by the item's ID and
   *     with boolean values for the check result.
   *   - The check closure is passed one argument of type
   *     \Drupal\reviewer\Reviewer\Task\TaskConfigInterface and which contains
   *     all configuration defined and loaded by the review.
   *   - Tasks which not run may return null.
   * @param string $messageFailure
   *   The message to display if the check fails. If the task checks multiple
   *   items, the failure string may contain the @failures placeholder which
   *   will be replaced by a comma separated list of each item ID. Item IDs are
   *   the keys of the array returned by the $check method.
   * @param \Drupal\reviewer\Reviewer\Checklist\ChecklistInterface|null $checklist
   *   The checklist this task is run from.
   *
   * @see \Drupal\reviewer\Reviewer\Task\TaskConfigInterface
   * @see \Drupal\reviewer\Reviewer\Review\ReviewInterface::getConfiguration
   * @see \Drupal\reviewer\Reviewer\Task\TaskProviderInterface::tasks
   */
  public function create(
    string $id,
    \Closure $check,
    string $messageFailure,
    ChecklistInterface|null $checklist = NULL,
  ): TaskInterface;

}
