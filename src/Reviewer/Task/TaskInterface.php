<?php

namespace Drupal\reviewer\Reviewer\Task;

use Drupal\reviewer\Reviewer\Checklist\ChecklistInterface;
use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\Result\MultipleResultsInterface;
use Drupal\reviewer\Reviewer\RunnableInterface;

/**
 * Defines a task.
 */
interface TaskInterface extends RunnableInterface, MultipleResultsInterface, IgnoredTasksInterface {

  /**
   * Get the task ID.
   */
  public function getId(): string;

  /**
   * Get the task check closure.
   */
  public function getCheck(): \Closure;

  /**
   * Get the failure message.
   */
  public function getFailureMessage(): string;

  /**
   * Set the checklist that contains this task.
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   *   Thrown attempting to set a checklist when a checklist is already set.
   */
  public function setChecklist(ChecklistInterface $checklist): self;

}
