<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Task;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Configuration value object passed to task check closures.
 */
final readonly class TaskConfig implements TaskConfigInterface {

  /**
   * Constructor.
   *
   * @param array<string, \Drupal\Core\Config\ImmutableConfig|\Drupal\Core\Config\Entity\ConfigEntityInterface> $configuration
   */
  public function __construct(
    private array $configuration,
    private ConfigEntityInterface|null $entity = NULL,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getConfig(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigOfTypeImmutable(): array {
    return array_filter(
      $this->configuration,
      fn($config) => $config instanceof ImmutableConfig,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigOfTypeEntity(): array {
    return array_filter(
      $this->configuration,
      fn($config) => $config instanceof ConfigEntityInterface,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigById(string $id): ConfigEntityInterface|ImmutableConfig {
    return $this->configuration[$id] ?? throw new \ErrorException("Undefined array key $id");
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigOfTypeImmutableById(string $id): ImmutableConfig {
    return $this->getConfigOfTypeImmutable()[$id] ?? throw new \ErrorException("Undefined array key $id");
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigOfTypeEntityById(string $id): ConfigEntityInterface {
    return $this->getConfigOfTypeEntity()[$id] ?? throw new \ErrorException("Undefined array key $id");
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): ConfigEntityInterface {
    return $this->entity ?? throw new \ErrorException('No config entity set');
  }

}
