<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Task;

use Drupal\reviewer\Exception\CannotOverridePropertyException;
use Drupal\reviewer\Exception\NotRunException;
use Drupal\reviewer\Exception\TaskReturnException;
use Drupal\reviewer\Reviewer\Checklist\ChecklistInterface;
use Drupal\reviewer\Reviewer\Result\ResultCollectionFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultCollectionInterface;
use Drupal\reviewer\Reviewer\Result\ResultFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\Result\TypeFactoryInterface;
use Drupal\reviewer\Reviewer\Status\StatusEvaluatorInterface;
use Drupal\reviewer\Reviewer\Status\StatusFactoryInterface;

/**
 * A task.
 */
final class Task implements TaskInterface {

  private ResultCollectionInterface|null $result = NULL;

  /**
   * Constructor.
   *
   * @throws \InvalidArgumentException
   */
  public function __construct(
    private readonly string $id,
    private readonly \Closure $check,
    private readonly string $failureMessage,
    private ChecklistInterface|null $checklist,
    private readonly ResultCollectionFactoryInterface $resultCollectionFactory,
    private readonly ResultFactoryInterface $resultFactory,
    private readonly StatusEvaluatorInterface $statusEvaluator,
    private readonly StatusFactoryInterface $statusFactory,
    private readonly TypeFactoryInterface $typeFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function run(): TaskInterface {
    $results = [];
    $failures = [];

    try {
      $check_return = $this->getCheck()($this->checklist?->getConfiguration());

      foreach ($this->normalizeResult($check_return) as $test => $result) {
        $status = \is_null($result)
          ? $this->statusFactory->createNotRun()
          : $this->statusFactory->createFromBool($result);

        $message = '';
        if ($this->statusEvaluator->isFailure($status)) {
          $failure_id = str_contains($test, '.')
            ? explode('.', $test)[1]
            : $test;
          $message = $this->failureMessageWithIds($failure_id);
          $failures[] = $failure_id;
        }

        $results[$this->resultId($test)] = $this->resultFactory->create(
          $this->resultId($test),
          $this->typeFactory->createIndividual(),
          $status,
          $this,
          $message,
        );
      }
    }
    catch (\Exception $e) {
      $status = $this->statusFactory->createError();
      $message = $e->getMessage();

      $results[$this->resultId()] = $this->resultFactory->create(
        $this->resultId(),
        $this->typeFactory->createIndividual(),
        $status,
        $this,
        $message
      );
    }

    $status = $this->statusFactory->createMostSevere($results);
    $message = $this->statusEvaluator->isFailure($status)
      ? $this->failureMessageWithIds($failures)
      : $message ?? '';

    $result = $this->resultFactory->create(
      $this->resultId(),
      $this->typeFactory->createSummary(),
      $status,
      $this,
      $message,
    );

    $this->result = $this->resultCollectionFactory->create($this, $result, $results);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function resultId(string $variant = ''): string {
    $id = $this->checklist
      ? trim("{$this->checklist->resultId()}.{$this->getId()}.$variant", '.')
      : '';
    return implode('.', array_unique(explode('.', $id)));
  }

  /**
   * Ensure that all check returns are arrays of the correct type and structure.
   *
   * @return array<string, bool|null>
   *
   * @throws \ReflectionException
   * @throws \Drupal\reviewer\Exception\TaskReturnException
   *   Thrown when the checklist task return an invalid value.
   *
   * @see \Drupal\reviewer\Reviewer\Task\TaskProviderInterface::tasks
   */
  private function normalizeResult(mixed $result): array {
    if (\is_array($result)) {
      foreach ($result as $id => $pass) {
        if (!\is_bool($pass)) {
          throw new TaskReturnException(sprintf(
            'Tasks must returning an array must have with boolean values, %s returned type %s for key "%s" instead.',
            $this->checkFullQualifiedName(),
            \gettype($pass),
            $id,
          ));
        }
      }
      /** @var array<string, bool> $result */
      return $result;
    }

    if (\is_bool($result) || \is_null($result)) {
      return [$this->getId() => $result];
    }

    throw new TaskReturnException(sprintf(
      'Tasks must return an array, boolean value, or null. %s returned %s instead.',
       $this->checkFullQualifiedName(),
      \gettype($result),
    ));
  }

  /**
   * Get the full qualified name of the check.
   *
   * @throws \ReflectionException
   */
  private function checkFullQualifiedName(): string {
    $reflection = new \ReflectionFunction($this->getCheck()(...));
    $name = $reflection->getClosureScopeClass()?->getName() ?? '';
    return "$name::{$reflection->getName()}()";
  }

  /**
   * Generate a failure message with IDs added.
   *
   * @param string|string[] $failure_ids
   */
  private function failureMessageWithIds(string|array $failure_ids): string {
    $message = $this->getFailureMessage();
    if (str_contains($message, '@failures')) {
      $failure_ids = \is_string($failure_ids) ? [$failure_ids] : $failure_ids;
      $message = str_replace('@failures', implode(', ', $failure_ids), $message);
    }
    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->checklist ? "{$this->checklist->getId()}.$this->id" : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCheck(): \Closure {
    return $this->check;
  }

  /**
   * {@inheritdoc}
   */
  public function getFailureMessage(): string {
    return $this->failureMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function setChecklist(ChecklistInterface $checklist): TaskInterface {
    if ($this->checklist) {
      throw new CannotOverridePropertyException('checklist', $this::class);
    }
    $this->checklist = $checklist;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function result(): ResultInterface {
    return $this->results()->result();
  }

  /**
   * {@inheritdoc}
   */
  public function results(): ResultCollectionInterface {
    return $this->result ?? throw new NotRunException($this->getId());
  }

  /**
   * {@inheritdoc}
   */
  public function getIgnored(): array {
    return $this->checklist?->getIgnored() ?? [];
  }

}
