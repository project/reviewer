<?php

namespace Drupal\reviewer\Reviewer\Task;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Defines a configuration value object passed to task check closures.
 */
interface TaskConfigInterface {

  /**
   * Get all configuration in the review plugin's "configuration" property.
   *
   * @return array<string, \Drupal\Core\Config\Entity\ConfigEntityInterface|\Drupal\Core\Config\ImmutableConfig>
   */
  public function getConfig(): array;

  /**
   * Get all immutable config in the review plugin's "configuration" property.
   *
   * @return array<string, \Drupal\Core\Config\ImmutableConfig>
   */
  public function getConfigOfTypeImmutable(): array;

  /**
   * Get all config entities in the review plugin's "configuration" property.
   *
   * @return array<string, \Drupal\Core\Config\Entity\ConfigEntityInterface>
   */
  public function getConfigOfTypeEntity(): array;

  /**
   * Get a single item in the review plugin's "configuration" property by ID.
   *
   * @throws \ErrorException
   *   Thrown when configuration of the ID does not exist.
   */
  public function getConfigById(string $id): ConfigEntityInterface|ImmutableConfig;

  /**
   * Get a config item in the review plugin's "configuration" property by ID.
   *
   * @throws \ErrorException
   *   Thrown when configuration of the ID does not exist.
   */
  public function getConfigOfTypeImmutableById(string $id): ImmutableConfig;

  /**
   * Get a config entity in the review plugin's "configuration" property by ID.
   *
   * @throws \ErrorException
   *   Thrown when configuration of the ID does not exist.
   */
  public function getConfigOfTypeEntityById(string $id): ConfigEntityInterface;

  /**
   * Get the review plugin's config entity, either defined by type or ID.
   *
   * @throws \ErrorException
   *   Thrown when the configuration entity does not exist.
   */
  public function getEntity(): ConfigEntityInterface;

}
