<?php

namespace Drupal\reviewer\Reviewer\Task;

/**
 * Definition for an object which provides tasks.
 */
interface TaskProviderInterface {

  /**
   * Return the tasks to execute when the item is run.
   *
   * Use \Drupal\reviewer\Reviewer\Task\TaskFactoryInterface::create to ensure
   * that tasks have the required dependencies.
   *
   * @return \Drupal\reviewer\Reviewer\Task\TaskInterface[]
   *
   * @see \Drupal\reviewer\Reviewer\Task\TaskFactoryInterface::create
   */
  public function tasks(): array;

}
