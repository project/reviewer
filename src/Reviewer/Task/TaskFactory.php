<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Task;

use Drupal\reviewer\Reviewer\Checklist\ChecklistInterface;
use Drupal\reviewer\Reviewer\Result\ResultCollectionFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultFactoryInterface;
use Drupal\reviewer\Reviewer\Result\TypeFactoryInterface;
use Drupal\reviewer\Reviewer\Status\StatusEvaluatorInterface;
use Drupal\reviewer\Reviewer\Status\StatusFactoryInterface;

/**
 * Creates tasks.
 */
final readonly class TaskFactory implements TaskFactoryInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private ResultCollectionFactoryInterface $resultCollectionFactory,
    private ResultFactoryInterface $resultFactory,
    private StatusEvaluatorInterface $statusEvaluator,
    private StatusFactoryInterface $statusFactory,
    private TypeFactoryInterface $typeFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function create(
    string $id,
    \Closure $check,
    string $messageFailure,
    ChecklistInterface|null $checklist = NULL,
  ): TaskInterface {
    return new Task(
      $id,
      $check,
      $messageFailure,
      $checklist,
      $this->resultCollectionFactory,
      $this->resultFactory,
      $this->statusEvaluator,
      $this->statusFactory,
      $this->typeFactory,
    );
  }

}
