<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Task;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface;

/**
 * Creates task configuration value objects.
 */
final readonly class TaskConfigFactory implements TaskConfigFactoryInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private ConfigManagerInterface $configManager,
    private EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function create(
    ReviewPluginInterface $review,
    string $bundle = '',
  ): TaskConfigInterface {
    $configuration = [];
    foreach ($review->getConfiguration() as $id) {
      $config = $this->configManager->getConfigFactory()->get($id);
      if ($config->getRawData()) {
        $configuration[$id] = $config;
      }
      elseif ($config_entity = $this->configManager->loadConfigEntityByName($id)) {
        /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $config_entity */
        $configuration[$id] = $config_entity;
      }
    }

    $entity = NULL;
    if ($review->getEntityId()) {
      $entity = $this->configManager->loadConfigEntityByName($review->getEntityId());
    }
    elseif ($review->getEntityType()) {
      $entity = $bundle
        ? $this
          ->entityTypeManager
          ->getStorage($review->getEntityType())
          ->load($bundle)
        : throw new \InvalidArgumentException('Parameter $bundle must be a non-empty string when the review has an entity type defined.');
    }
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface|null $entity */

    return new TaskConfig($configuration, $entity);
  }

}
