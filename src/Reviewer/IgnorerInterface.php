<?php

namespace Drupal\reviewer\Reviewer;

use Drupal\reviewer\Reviewer\Result\ResultInterface;

/**
 * Defines a class which ignores review results.
 */
interface IgnorerInterface {

  /**
   * Get IDs and reasons for all ignored tasks.
   *
   * @return array{id: string, reason: string}[]
   */
  public function getIgnored(): array;

  /**
   * Get IDs and reasons for all tasks ignored in plugin attributes.
   *
   * @return array{id: string, reason: string}[]
   */
  public function getPluginIgnored(): array;

  /**
   * Get IDs and reasons for all tasks ignored in configuration.
   *
   * @return array{id: string, reason: string}[]
   */
  public function getConfigIgnored(): array;

  /**
   * Check if a result is in the list of ignored results.
   */
  public function isIgnored(ResultInterface|string $result): bool;

  /**
   * Check if a result is in the list of results ignored in plugin attributes.
   */
  public function isPluginIgnored(ResultInterface|string $result): bool;

  /**
   * Check if a result is in the list of results ignored in configuration.
   */
  public function isConfigIgnored(ResultInterface|string $result): bool;

  /**
   * Get the reason an item was ignored.
   *
   * @throws \Drupal\reviewer\Exception\NotIgnoredException
   *   Thrown when attempting to get a reason from a result that is not ignored.
   */
  public function ignoredReason(ResultInterface|string $result): string;

  /**
   * Return results in the list of ignored results from a set of results.
   *
   * @param \Drupal\reviewer\Reviewer\Result\ResultInterface[] $results
   *
   * @return \Drupal\reviewer\Reviewer\Result\ResultInterface[]
   */
  public function ignoredResults(array $results): array;

  /**
   * Return results not in the list of ignored results from a set of results.
   *
   * @param \Drupal\reviewer\Reviewer\Result\ResultInterface[] $results
   *
   * @return \Drupal\reviewer\Reviewer\Result\ResultInterface[]
   */
  public function unignoredResults(array $results): array;

  /**
   * Add a result to the list of ignored results.
   */
  public function ignore(
    ResultInterface|string $result,
    string $reason = '',
  ): void;

  /**
   * Remove a result from the list of ignored results.
   *
   * @param array<\Drupal\reviewer\Reviewer\Result\ResultInterface|string>|\Drupal\reviewer\Reviewer\Result\ResultInterface|string $results
   */
  public function unignore(ResultInterface|array|string $results): void;

  /**
   * Remove all results from the list of ignored results.
   */
  public function unignoreAll(): void;

}
