<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer;

use Drupal\reviewer\Plugin\ReviewManagerInterface;
use Drupal\reviewer\Reviewer\Builder\Review\ReviewBuilderInterface;

/**
 * Runs reviews.
 */
final readonly class Runner implements RunnerInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private ReviewBuilderInterface $reviewBuilder,
    private BundleEvaluatorInterface $bundleEvaluator,
    private ReviewManagerInterface $reviewManager,
  ) {}

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function runIds(array $review_ids = [], array $bundles = []): array {
    $definitions = array_keys($this->reviewManager->getDefinitions());
    if ($review_ids) {
      $definitions = array_intersect($definitions, $review_ids);
    }

    /** @var array<string, \Drupal\reviewer\Reviewer\Review\ReviewInterface> $reviews */
    $reviews = $this->reviewBuilder->fromIds($definitions);
    return $this->run($this->bundleEvaluator->reviewsOfBundles($reviews, $bundles));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function runPlugins(
    array $review_plugins = [],
    array $bundles = [],
  ): array {
    if (!$review_plugins) {
      return $this->runIds([], $bundles);
    }

    /** @var array<string, \Drupal\reviewer\Reviewer\Review\ReviewInterface> $reviews */
    $reviews = $this->reviewBuilder->fromPlugins($review_plugins);
    return $this->run($this->bundleEvaluator->reviewsOfBundles($reviews, $bundles));
  }

  /**
   * Run reviews.
   *
   * @param array<string, \Drupal\reviewer\Reviewer\Review\ReviewInterface> $reviews
   *
   * @return array<string, \Drupal\reviewer\Reviewer\Review\ReviewInterface>
   */
  private function run(array $reviews): array {
    $results = [];
    foreach ($reviews as $review) {
      $review->run();
      $results[$review->getId()] = $review;
    }
    return $results;
  }

}
