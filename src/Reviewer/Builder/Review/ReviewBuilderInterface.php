<?php

namespace Drupal\reviewer\Reviewer\Builder\Review;

use Drupal\reviewer\Reviewer\Builder\RunnableBuilderInterface;

/**
 * Defines a class which builds runnable reviews from review plugins.
 */
interface ReviewBuilderInterface extends RunnableBuilderInterface {

}
