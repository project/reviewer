<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Builder\Review;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\reviewer\Plugin\reviewer\BuildableInterface;
use Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface;
use Drupal\reviewer\Plugin\ReviewManagerInterface;
use Drupal\reviewer\Reviewer\Builder\Checklist\ChecklistBuilderInterface;
use Drupal\reviewer\Reviewer\Builder\RunnableBuilderBase;
use Drupal\reviewer\Reviewer\BundleEvaluatorInterface;
use Drupal\reviewer\Reviewer\Review\ReviewFactoryInterface;
use Drupal\reviewer\Reviewer\Task\TaskConfigFactoryInterface;

/**
 * Builds runnable reviews from review plugins.
 */
final class ReviewBuilder extends RunnableBuilderBase implements ReviewBuilderInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private readonly BundleEvaluatorInterface $bundleEvaluator,
    protected readonly ChecklistBuilderInterface $checklistBuilder,
    protected readonly TaskConfigFactoryInterface $taskConfigFactory,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly ReviewFactoryInterface $reviewFactory,
    ReviewManagerInterface $pluginManager,
  ) {
    parent::__construct($pluginManager);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   *   Thrown when no checklists are defined on the review.
   */
  public function fromPlugin(
    BuildableInterface $plugin,
    string $id = '',
  ): array {
    assert($plugin instanceof ReviewPluginInterface);

    $reviews = [];
    if ($plugin->getEntityId() || $plugin->getEntityType()) {
      foreach ($this->bundleEvaluator->allBundles($plugin) as $bundle) {
        if ($plugin->getEntityId() === $bundle) {
          $bundle_id = $bundle;
        }
        else {
          $bundle_id = $id ?: "{$plugin->getPluginId()}.$bundle";
        }

        /** @var array<string, \Drupal\reviewer\Reviewer\Checklist\ChecklistInterface> $checklists */
        $checklists = $this->checklistBuilder->fromIds($plugin->getChecklists());
        $reviews[$bundle_id] = $this->reviewFactory->create(
          $bundle_id,
          $this->labelWithBundle($plugin, $bundle),
          $checklists,
          $this->taskConfigFactory->create($plugin, $bundle),
          $plugin->getIgnored(),
        );
      }
    }
    else {
      /** @var array<string, \Drupal\reviewer\Reviewer\Checklist\ChecklistInterface> $checklists */
      $checklists = $this->checklistBuilder->fromIds($plugin->getChecklists());
      $reviews[$id ?: $plugin->getPluginId()] = $this->reviewFactory->create(
        $id ?: $plugin->getPluginId(),
        $plugin->getLabel(),
        $checklists,
        $this->taskConfigFactory->create($plugin),
        $plugin->getIgnored(),
      );
    }

    foreach ($reviews as $review) {
      foreach ($review->getChecklists() as $checklist) {
        $checklist->setReview($review);
      }
    }
    return $reviews;
  }

  /**
   * Add the bundle label to a review plugin label.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function labelWithBundle(
    ReviewPluginInterface $review,
    string $bundle,
  ): string {
    if (
      ($entity_type = $review->getEntityType())
      && ($entity = $this->entityTypeManager->getStorage($entity_type)->load($bundle))
    ) {
      return sprintf(
        '%s: %s',
        $review->getLabel(),
        $entity->label(),
      );
    }
    return $review->getLabel();
  }

}
