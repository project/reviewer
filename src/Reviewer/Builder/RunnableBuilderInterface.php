<?php

namespace Drupal\reviewer\Reviewer\Builder;

use Drupal\reviewer\Plugin\reviewer\BuildableInterface;

/**
 * Defines a class which builds runnable items from review plugins.
 */
interface RunnableBuilderInterface {

  /**
   * Create a runnable item from a buildable plugin ID.
   *
   * @return array<string, \Drupal\reviewer\Reviewer\RunnableInterface>
   *   An array of runnable items, keyed by the plugin ID. If the runnable item
   *   is an instance of EntityReviewInterface and supports bundles then a
   *   runnable item will be returned for each bundle, keyed by the plugin ID
   *   appended with the bundle machine name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   *   Thrown when a setter is called on a property that already has a value.
   */
  public function fromId(string $runnable_plugin_id, string $id = ''): array;

  /**
   * Create a runnable item from an array of buildable item plugin IDs.
   *
   * @param string[] $runnable_plugin_ids
   *
   * @return array<string, \Drupal\reviewer\Reviewer\RunnableInterface>
   *   An array of runnable items, keyed by the plugin ID. If the runnable item
   *   is an instance of EntityReviewInterface and supports bundles then a
   *   runnable item will be returned for each bundle, keyed by the plugin ID
   *   appended with the bundle machine name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   *    Thrown when a setter is called on a property that already has a value.
   */
  public function fromIds(array $runnable_plugin_ids, string $id = ''): array;

  /**
   * Create a runnable item from an instance of a buildable plugin.
   *
   * @return array<string, \Drupal\reviewer\Reviewer\RunnableInterface>
   *   An array of runnable items, keyed by the plugin ID. If the runnable item
   *   is an instance of EntityReviewInterface and supports bundles then a
   *   runnable item will be returned for each bundle, keyed by the plugin ID
   *   appended with the bundle machine name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   *    Thrown when a setter is called on a property that already has a value.
   */
  public function fromPlugin(
    BuildableInterface $plugin,
    string $id = '',
  ): array;

  /**
   * Create a runnable item from an array of a buildable plugin instances.
   *
   * @param \Drupal\reviewer\Plugin\reviewer\BuildableInterface[] $plugins
   *
   * @return array<string, \Drupal\reviewer\Reviewer\RunnableInterface>
   *   An array of runnable items, keyed by the plugin ID. If the runnable item
   *   is an instance of EntityReviewInterface and supports bundles then a
   *   runnable item will be returned for each bundle, keyed by the plugin ID
   *   appended with the bundle machine name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   *    Thrown when a setter is called on a property that already has a value.
   */
  public function fromPlugins(array $plugins, string $id = ''): array;

}
