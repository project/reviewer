<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Builder;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\reviewer\Plugin\reviewer\BuildableInterface;

/**
 * Base class for builders that create runnable items.
 *
 * @property \Drupal\Component\Plugin\PluginManagerInterface $pluginManager
 */
abstract class RunnableBuilderBase implements RunnableBuilderInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private readonly PluginManagerInterface $pluginManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function fromId(string $runnable_plugin_id, string $id = ''): array {
    $buildable_plugin = $this->pluginManager->createInstance(
      $runnable_plugin_id,
      $this->pluginManager->getDefinition($runnable_plugin_id),
    );
    assert($buildable_plugin instanceof BuildableInterface);
    return $this->fromPlugin($buildable_plugin, $id);
  }

  /**
   * {@inheritdoc}
   */
  public function fromIds(array $runnable_plugin_ids, string $id = ''): array {
    $built = [];
    foreach ($runnable_plugin_ids as $runnable_plugin_id) {
      $built += $this->fromId($runnable_plugin_id, $id);
    }
    return $built;
  }

  /**
   * {@inheritdoc}
   */
  public function fromPlugins(array $plugins, string $id = ''): array {
    $built = [];
    /** @var \Drupal\reviewer\Plugin\reviewer\BuildableInterface&\Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface $plugin */
    foreach ($plugins as $plugin) {
      $built += $this->fromPlugin($plugin, $id);
    }
    return $built;
  }

}
