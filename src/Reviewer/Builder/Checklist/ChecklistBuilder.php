<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Builder\Checklist;

use Drupal\reviewer\Plugin\ChecklistManagerInterface;
use Drupal\reviewer\Plugin\reviewer\BuildableInterface;
use Drupal\reviewer\Plugin\reviewer\Checklist\ChecklistPluginInterface;
use Drupal\reviewer\Reviewer\Builder\RunnableBuilderBase;
use Drupal\reviewer\Reviewer\Checklist\ChecklistFactoryInterface;

/**
 * Builds runnable checklists from checklist plugins.
 */
final class ChecklistBuilder extends RunnableBuilderBase implements ChecklistBuilderInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    protected readonly ChecklistFactoryInterface $checklistFactory,
    ChecklistManagerInterface $pluginManager,
  ) {
    parent::__construct($pluginManager);
  }

  /**
   * {@inheritdoc}
   */
  public function fromPlugin(
    BuildableInterface $plugin,
    string $id = '',
  ): array {
    assert($plugin instanceof ChecklistPluginInterface);
    $checklist = $this->checklistFactory->create(
      $plugin->getPluginId(),
      $plugin->getLabel(),
      $plugin->tasks(),
    );
    foreach ($checklist->tasks() as $task) {
      $task->setChecklist($checklist);
    }
    return [$id ?: $checklist->getId() => $checklist];
  }

}
