<?php

namespace Drupal\reviewer\Reviewer\Builder\Checklist;

use Drupal\reviewer\Reviewer\Builder\RunnableBuilderInterface;

/**
 * Defines a class which builds runnable checklists from checklist plugins.
 */
interface ChecklistBuilderInterface extends RunnableBuilderInterface {

}
