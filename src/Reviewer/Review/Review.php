<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Review;

use Drupal\reviewer\Exception\NotRunException;
use Drupal\reviewer\Reviewer\Result\ResultCollectionFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultCollectionInterface;
use Drupal\reviewer\Reviewer\Result\ResultFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\Result\TypeFactoryInterface;
use Drupal\reviewer\Reviewer\Status\StatusFactoryInterface;
use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;

/**
 * A review.
 */
final class Review implements ReviewInterface {

  private ResultCollectionInterface|null $result = NULL;

  /**
   * Constructor.
   *
   * @param array<string, \Drupal\reviewer\Reviewer\Checklist\ChecklistInterface> $checklists
   * @param array{id: string, reason: string}[] $ignored
   */
  public function __construct(
    private readonly string $id,
    private readonly string $label,
    private readonly array $checklists,
    private readonly TaskConfigInterface $configuration,
    private readonly array $ignored,
    private readonly ResultCollectionFactoryInterface $resultCollectionFactory,
    private readonly ResultFactoryInterface $resultFactory,
    private readonly StatusFactoryInterface $statusFactory,
    private readonly TypeFactoryInterface $typeFactory,
  ) {}

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   */
  public function run(): ReviewInterface {
    $results = [];

    foreach ($this->getChecklists() as $checklist) {
      $checklist->run();
      $results[$checklist->getId()] = $checklist->results();
    }

    $status = $this->statusFactory->createMostSevere(array_map(
      fn(ResultCollectionInterface $result_collection) => $result_collection->result(),
      $results,
    ));
    $message = $status->label();

    $result = $this->resultFactory->create(
      $this->resultId(),
      $this->typeFactory->createSummary(),
      $status,
      $this,
      $message,
    );

    $this->result = $this->resultCollectionFactory->create($this, $result, $results);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function resultId(string $variant = ''): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getChecklists(): array {
    return $this->checklists;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): TaskConfigInterface {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getIgnored(): array {
    return $this->ignored;
  }

  /**
   * {@inheritdoc}
   */
  public function result(): ResultInterface {
    return $this->results()->result();
  }

  /**
   * {@inheritdoc}
   */
  public function results(): ResultCollectionInterface {
    return $this->result ?? throw new NotRunException($this->getId());
  }

}
