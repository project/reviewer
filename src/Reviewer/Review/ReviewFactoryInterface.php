<?php

namespace Drupal\reviewer\Reviewer\Review;

use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;

/**
 * Defines a factory which creates reviews.
 */
interface ReviewFactoryInterface {

  /**
   * Create a review.
   *
   * @param array<string, \Drupal\reviewer\Reviewer\Checklist\ChecklistInterface> $checklists
   * @param array{id: string, reason: string}[] $ignored
   */
  public function create(
    string $id,
    string $label,
    array $checklists,
    TaskConfigInterface $configuration,
    array $ignored,
  ): ReviewInterface;

}
