<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Review;

use Drupal\reviewer\Reviewer\Result\ResultCollectionFactoryInterface;
use Drupal\reviewer\Reviewer\Result\ResultFactoryInterface;
use Drupal\reviewer\Reviewer\Result\TypeFactoryInterface;
use Drupal\reviewer\Reviewer\Status\StatusFactoryInterface;
use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;

/**
 * Creates reviews.
 */
final readonly class ReviewFactory implements ReviewFactoryInterface {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private ResultCollectionFactoryInterface $resultCollectionFactory,
    private ResultFactoryInterface $resultFactory,
    private StatusFactoryInterface $statusFactory,
    private TypeFactoryInterface $typeFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function create(
    string $id,
    string $label,
    array $checklists,
    TaskConfigInterface $configuration,
    array $ignored,
  ): Review {
    return new Review(
      $id,
      $label,
      $checklists,
      $configuration,
      $ignored,
      $this->resultCollectionFactory,
      $this->resultFactory,
      $this->statusFactory,
      $this->typeFactory,
    );
  }

}
