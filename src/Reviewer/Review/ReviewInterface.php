<?php

namespace Drupal\reviewer\Reviewer\Review;

use Drupal\reviewer\Reviewer\IgnoredTasksInterface;
use Drupal\reviewer\Reviewer\Result\MultipleResultsInterface;
use Drupal\reviewer\Reviewer\RunnableInterface;
use Drupal\reviewer\Reviewer\Task\TaskConfigInterface;

/**
 * Defines a review.
 */
interface ReviewInterface extends RunnableInterface, MultipleResultsInterface, IgnoredTasksInterface {

  /**
   * Get the review ID.
   */
  public function getId(): string;

  /**
   * Get the review label.
   */
  public function getLabel(): string;

  /**
   * Get all checklists contained in the review.
   *
   * @return array<string, \Drupal\reviewer\Reviewer\Checklist\ChecklistInterface>
   *
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   *   Thrown when no checklists are defined on the review.
   */
  public function getChecklists(): array;

  /**
   * Get configuration entities loaded for this review.
   */
  public function getConfiguration(): TaskConfigInterface;

}
