<?php

namespace Drupal\reviewer\Reviewer;

/**
 * Defines a class which runs reviews.
 */
interface RunnerInterface {

  /**
   * Run reviews from review plugin IDs.
   *
   * @param string[] $review_ids
   *   Review plugin IDs to run. Omit for all reviews.
   * @param string[] $bundles
   *   The bundles of the entity type to review. Omit to test all bundles of the
   *   entity type, or if the entity type does not support bundles.
   *
   * @return array<string, \Drupal\reviewer\Reviewer\Review\ReviewInterface>
   *   Results keyed by result ID.
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   *   Thrown when a setter is called on a property that already has a value.
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   *   Thrown when no checklists are defined on the review.
   */
  public function runIds(array $review_ids = [], array $bundles = []): array;

  /**
   * Run reviews from review plugins.
   *
   * @param \Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface[] $review_plugins
   *   Review plugins to run. Omit for all reviews.
   * @param string[] $bundles
   *   The bundles of the entity type to review. Omit to test all bundles of the
   *   entity type, or if the entity type does not support bundles.
   *
   * @return array<string, \Drupal\reviewer\Reviewer\Review\ReviewInterface>
   *   Results keyed by result ID.
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   *   Thrown when a setter is called on a property that already has a value.
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   *   Thrown when no checklists are defined on the review.
   */
  public function runPlugins(
    array $review_plugins = [],
    array $bundles = [],
  ): array;

}
