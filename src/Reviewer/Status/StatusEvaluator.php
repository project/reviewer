<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Status;

/**
 * Evaluates statuses.
 */
final readonly class StatusEvaluator implements StatusEvaluatorInterface {

  /**
   * {@inheritdoc}
   */
  public function isFailure(Status $status): bool {
    return match ($status) {
      Status::Fail, Status::IgnoredFailure => TRUE,
      default => FALSE,
    };
  }

  /**
   * {@inheritdoc}
   */
  public function isError(Status $status): bool {
    return match ($status) {
      Status::Error, Status::IgnoredError => TRUE,
      default => FALSE,
    };
  }

  /**
   * {@inheritdoc}
   */
  public function isIgnored(Status $status): bool {
    return match ($status) {
      Status::IgnoredFailure, Status::IgnoredError => TRUE,
      default => FALSE,
    };
  }

  /**
   * {@inheritdoc}
   */
  public function isNotRun(Status $status): bool {
    return $status === Status::NotRun;
  }

  /**
   * {@inheritdoc}
   */
  public function isPass(Status $status): bool {
    return $status === Status::Pass;
  }

}
