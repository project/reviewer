<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Status;

/**
 * Represents the status of a result.
 *
 * Cases backed with larger integers are considered more severe than cases
 * backed with smaller integers.
 *
 * @see \Drupal\reviewer\Reviewer\Status\StatusFactoryInterface::createMostSevere
 */
enum Status: int {

  case NotRun = 0;
  case Pass = 1;
  case IgnoredFailure = 2;
  case IgnoredError = 3;
  case Fail = 4;
  case Error = 5;

  /**
   * A human-readable label for the status.
   */
  public function label(): string {
    return match($this) {
      Status::NotRun => 'Not Run',
      Status::Pass => 'Passed',
      Status::IgnoredFailure => 'Ignored Failure',
      Status::IgnoredError => 'Ignored Error',
      Status::Fail => 'Failed',
      Status::Error => 'Errored',
    };
  }

  /**
   * A colorized human-readable label for the status displayed in the console.
   */
  public function labelConsole(): string {
    return match ($this) {
      Status::NotRun => "<fg=black;bg=white>{$this->label()}</>",
      Status::IgnoredFailure, Status::IgnoredError => "<fg=black;bg=yellow>{$this->label()}</>",
      Status::Pass => "<info>{$this->label()}</info>",
      Status::Fail, Status::Error => "<error>{$this->label()}</error>",
    };
  }

}
