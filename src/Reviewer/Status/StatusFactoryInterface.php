<?php

namespace Drupal\reviewer\Reviewer\Status;

/**
 * Defines a factory which creates statuses.
 */
interface StatusFactoryInterface {

  /**
   * Create a no results status.
   */
  public function createNotRun(): Status;

  /**
   * Create an ignored status from a status.
   */
  public function createIgnored(Status $status): Status;

  /**
   * Create an unignored status from a status.
   */
  public function createUnignored(Status $status): Status;

  /**
   * Create an error status.
   */
  public function createError(): Status;

  /**
   * Create a status from a boolean.
   */
  public function createFromBool(bool $bool): Status;

  /**
   * Return the most severe status from a set of results.
   *
   * The most severe status is the case backed by the highest integer.
   *
   * @param \Drupal\reviewer\Reviewer\Result\ResultInterface[] $results
   */
  public function createMostSevere(array $results): Status;

}
