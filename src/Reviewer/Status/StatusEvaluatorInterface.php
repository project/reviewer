<?php

namespace Drupal\reviewer\Reviewer\Status;

/**
 * Defines a class which evaluates statuses.
 */
interface StatusEvaluatorInterface {

  /**
   * Check if a status is a failure.
   */
  public function isFailure(Status $status): bool;

  /**
   * Check if a status is an error.
   */
  public function isError(Status $status): bool;

  /**
   * Check if a status is ignored.
   */
  public function isIgnored(Status $status): bool;

  /**
   * Check if a status is no results.
   */
  public function isNotRun(Status $status): bool;

  /**
   * Check if a status a pass.
   */
  public function isPass(Status $status): bool;

}
