<?php

declare(strict_types=1);

namespace Drupal\reviewer\Reviewer\Status;

use Drupal\reviewer\Reviewer\Result\ResultInterface;

/**
 * Creates statuses.
 */
final readonly class StatusFactory implements StatusFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createNotRun(): Status {
    return Status::NotRun;
  }

  /**
   * {@inheritdoc}
   */
  public function createIgnored(Status $status): Status {
    return match ($status) {
      Status::Fail => Status::IgnoredFailure,
      Status::Error => Status::IgnoredError,
      default => $status,
    };
  }

  /**
   * {@inheritdoc}
   */
  public function createUnignored(Status $status): Status {
    return match ($status) {
      Status::IgnoredFailure => Status::Fail,
      Status::IgnoredError => Status::Error,
      default => $status,
    };
  }

  /**
   * {@inheritdoc}
   */
  public function createError(): Status {
    return Status::Error;
  }

  /**
   * {@inheritdoc}
   */
  public function createFromBool(bool $bool): Status {
    return $bool ? Status::Pass : Status::Fail;
  }

  /**
   * {@inheritdoc}
   */
  public function createMostSevere(array $results): Status {
    return $results
      ? Status::from(max(array_map(
          fn(ResultInterface $result) => $result->getStatus()->value,
          $results,
        )))
      : $this->createNotRun();
  }

}
