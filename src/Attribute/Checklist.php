<?php

declare(strict_types=1);

namespace Drupal\reviewer\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;

/**
 * Attribute definition for checklist plugins.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class Checklist extends Plugin {

  /**
   * Constructor.
   *
   * @param string $id
   *   The checklist plugin unique ID.
   * @param string $label
   *   The checklist human-readable label.
   */
  public function __construct(string $id, public readonly string $label) {
    parent::__construct($id);
  }

}
