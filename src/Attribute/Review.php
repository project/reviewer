<?php

declare(strict_types=1);

namespace Drupal\reviewer\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;

/**
 * Attribute definition for review plugins.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class Review extends Plugin {

  /**
   * Constructor.
   *
   * @param string $id
   *   The review plugin unique ID.
   * @param string $label
   *   The review human-readable label.
   * @param string[] $checklists
   *   Checklist plugin IDs this review should run.
   * @param string|null $entity_id
   *   A specific entity ID to review.
   * @param string|null $entity_type
   *   An entity type ID to review.
   * @param string[] $bundles
   *   The bundles of the entity type to review. Omit to test all bundles of the
   *   entity type, or if the entity type does not support bundles.
   * @param string[] $configuration
   *   Configuration IDs, either simple or config entities, to load when running
   *   the review.
   * @param array{id: string, reason: string}[] $ignored
   *   Failed tasks that should be ignored. Each item is an array with two keys:
   *   id (the failed task ID) and reason (the user-displayed reason for
   *   ignoring the item).
   */
  public function __construct(
    string $id,
    public readonly string $label,
    public readonly array $checklists,
    public readonly string|null $entity_id = NULL,
    public readonly string|null $entity_type = NULL,
    public readonly array $bundles = [],
    public readonly array $configuration = [],
    public readonly array $ignored = [],
  ) {
    parent::__construct($id);
  }

}
