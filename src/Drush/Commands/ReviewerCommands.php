<?php

namespace Drupal\reviewer\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\reviewer\Plugin\reviewer\Review\ReviewPluginInterface;
use Drupal\reviewer\Plugin\ReviewManagerInterface;
use Drupal\reviewer\Reviewer\IgnorerInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\RunnerInterface;
use Drupal\reviewer\Reviewer\Status\StatusEvaluatorInterface;
use Drupal\reviewer\Reviewer\Status\StatusFactoryInterface;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleOutputInterface;

/**
 * Drush commands for reviewer.
 */
final class ReviewerCommands extends DrushCommands {

  use AutowireTrait;

  private int $width = 9999;

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private readonly IgnorerInterface $ignorer,
    private readonly ReviewManagerInterface $reviewManager,
    private readonly RunnerInterface $reviewRunner,
    private readonly StatusEvaluatorInterface $statusEvaluator,
    private readonly StatusFactoryInterface $statusFactory,
  ) {
    parent::__construct();
  }

  /**
   * Run reviews.
   *
   * @param string[] $ids
   * @param array<string, bool|string> $options
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   * @throws \Drupal\reviewer\Exception\NotRunException
   * @throws \Drupal\reviewer\Exception\NotIgnoredException
   */
  #[CLI\Command(name: 'reviewer:run', aliases: ['rvr'])]
  #[CLI\Argument(name: 'ids', description: 'Review IDs to run, separated by spaces.')]
  #[CLI\Option(name: 'review', description: 'Interactively review, prompting to ignore failures and errors.')]
  #[CLI\Option(name: 'review-new', description: 'Interactively review, prompting to ignore new failures and errors only.')]
  #[CLI\Option(name: 'review-reset', description: 'Interactively review, prompting to ignore all failures and errors. Any previously ignored items will no longer be ignored.')]
  #[ClI\Option(name: 'show-passed', description: 'Display passed tests in addition to failures and errors.')]
  #[ClI\Option(name: 'show-not-run', description: 'Display tests which were not run in addition to failures and errors.')]
  #[ClI\Option(name: 'show-ignored', description: 'Display ignored tests in addition to failures and errors.')]
  #[ClI\Option(name: 'show-all', description: 'Display ignored and passed tests in addition to failures and errors.')]
  #[CLI\Option(name: 'window-width', description: 'Set the window width to constrain the output table size.')]
  #[CLI\Help(description: 'Run reviews.')]
  #[CLI\Usage(name: 'reviewer:run', description: 'Run all reviews.')]
  #[CLI\Usage(name: 'reviewer:run node', description: 'Run reviews for all node types.')]
  #[CLI\Usage(name: 'reviewer:run node:article,page paragraph:gallery', description: 'Run reviews for the article and page node types and the gallery paragraph type.')]
  public function run(
    array $ids,
    array $options = [
      'review' => FALSE,
      'review-new' => FALSE,
      'review-reset' => FALSE,
      'show-ignored' => FALSE,
      'show-passed' => FALSE,
      'show-not-run' => FALSE,
      'show-all' => FALSE,
      'window-width' => '9999',
    ],
  ): void {
    $do_review = $options['review'] || $options['review-new'] || $options['review-reset'];
    $do_review_new = (bool) $options['review-new'];
    $do_review_reset = (bool) $options['review-reset'];
    $show_ignored = $options['show-ignored'] || $options['show-all'];
    $show_passed = $options['show-passed'] || $options['show-all'];
    $show_not_run = $options['show-not-run'] || $options['show-all'];
    $show_all = (bool) $options['show-all'];

    if (!\is_numeric($options['window-width'])) {
      $this->io()->warning(dt('Ignoring --window-width: Option value must be an integer.'));
      $this->width = 9999;
    }
    elseif ($options['window-width'] !== '9999') {
      $this->width = ((int) $options['window-width']) - 4;
    }
    else {
      $this->width = 9999;
    }

    $reviews = [];
    if ($review_ids = $this->processReviewIds($ids)) {
      foreach ($review_ids as $review_id => $bundles) {
        if (!$this->reviewManager->hasDefinition($review_id)) {
          $this->io()->warning(dt('Review @id does not exist.', [
            '@id' => $review_id,
          ]));
        }

        $reviews = array_merge($reviews, $this->reviewRunner->runIds(
          [$review_id],
          $bundles,
        ));
      }
    }
    else {
      $reviews = $this->reviewRunner->runIds();
    }

    foreach ($reviews as $review) {
      $rows = [];

      if ($do_review_reset) {
        $this->ignorer->unignore($review->results()->individualResults());
      }

      if ($do_review) {
        $failures_only = array_filter(
          $review->results()->individualResults(),
          fn(ResultInterface $result) => !$this->statusEvaluator->isPass($result->getStatus()),
        );
        $failures_only = array_filter(
          $failures_only,
          fn(ResultInterface $result) => !($this->statusEvaluator->isIgnored($result->getStatus()) && $this->ignorer->isPluginIgnored($result)),
        );
        if ($do_review_reset) {
          foreach ($failures_only as $result) {
            $result->overrideStatus($this->statusFactory->createUnignored($result->getStatus()));
          }
        }
        $this->tableFromResults($failures_only, $review->getLabel());

        foreach ($review->results()->individualResults() as $result) {
          if ($this->statusEvaluator->isPass($result->getStatus())) {
            continue;
          }

          $ignore = FALSE;
          $update = FALSE;
          $reason = '';

          if (
            !$do_review_new
            && $this->ignorer->isConfigIgnored($result)
          ) {
            $reason = $this->ignorer->ignoredReason($result);

            $ignore = $this->io()->confirm(dt('Keep ignoring result @id?' . PHP_EOL . ' Current ignored reason: @reason', [
              '@id' => $result->getId(),
              '@reason' => $reason,
            ]));

            $update = TRUE;
          }

          if (!$this->ignorer->isIgnored($result)) {
            $ignore = $this->io()->confirm(
              dt('Ignore result @id?', ['@id' => $result->getId()]),
              FALSE,
            );

            $update = TRUE;
          }

          if (!$update) {
            continue;
          }

          if ($ignore) {
            $reason = (string) $this->io()->ask(
              dt('Provide a reason for ignoring @id.', [
                '@id' => $result->getId(),
              ]),
              $reason,
            );

            $this->ignorer->ignore($result, $reason);
            $result->overrideStatus($this->statusFactory->createIgnored($result->getStatus()));
            $result->setOverrideMessage($reason);
          }
          else {
            $this->ignorer->unignore($result);
            $result->overrideStatus($this->statusFactory->createUnignored($result->getStatus()));
            $result->setOverrideMessage('');
          }
        }
      }

      $results = match ($show_all) {
        TRUE => $review->results()->individualResults(),
        FALSE => $this->statusEvaluator->isPass($review->result()->getStatus())
          ? [$review->result()]
          : $review->results()->individualResults(),
      };

      foreach ($results as $result) {
        if ($show_passed && $this->statusEvaluator->isPass($result->getStatus())) {
          $rows[] = $result;
          continue;
        }

        if ($show_not_run && $this->statusEvaluator->isNotRun($result->getStatus())) {
          $rows[] = $result;
          continue;
        }

        if ($show_ignored && $this->ignorer->isIgnored($result)) {
          $rows[] = $result;
          continue;
        }

        if (
          !$this->statusEvaluator->isPass($result->getStatus())
          && !$this->statusEvaluator->isNotRun($result->getStatus())
          && !$this->ignorer->isIgnored($result)
        ) {
          $rows[] = $result;
        }
      }

      $this->tableFromResults($rows, $review->getLabel());
    }
  }

  /**
   * List all available reviews.
   */
  #[CLI\Command(name: 'reviewer:list', aliases: ['rvl'])]
  #[CLI\Help(description: 'List available reviews.')]
  #[CLI\Usage(name: 'reviewer:list', description: 'List available reviews.')]
  #[CLI\DefaultTableFields(fields: ['id', 'label'])]
  #[CLI\FieldLabels(labels: ['id' => 'ID', 'label' => 'Label'])]
  public function list(): RowsOfFields {
    $this->io()->writeln('');
    $this->io()->writeln(dt('Available Reviews:'));
    return new RowsOfFields(array_map(
      fn(ReviewPluginInterface $review) => ['id' => $review->getPluginId(), 'label' => $review->getLabel()],
      $this->reviewManager->createAllInstances(),
    ));
  }

  /**
   * Process review IDs and return an array of review IDs and their bundles.
   *
   * @param string[] $review_ids
   *
   * @return array<string, string[]>
   *   Array keys are the review IDs with the values the bundles for each review
   *   ID, with an empty array for all bundles or a review which does not
   *   support bundles.
   */
  private function processReviewIds(array $review_ids): array {
    $processed_ids = [];
    foreach ($review_ids as $review_id) {
      $split_ids = explode(':', $review_id);
      $bundles = match (\count($split_ids)) {
        1 => [],
        default => explode(',', $split_ids[1]),
      };
      $processed_ids = array_merge(
        $processed_ids,
        array_fill_keys(explode(',', $split_ids[0]), $bundles),
      );
    }
    return $processed_ids;
  }

  /**
   * Print a results table from an array of results.
   *
   * @param \Drupal\reviewer\Reviewer\Result\ResultInterface[] $rows
   */
  private function tableFromResults(
    array $rows,
    string $label,
    ConsoleOutputInterface $output = new ConsoleOutput(),
  ): void {
    if ($rows) {
      $table = $this->io()
        ->createTable()
        ->setVertical()
        ->setColumnWidth(0, mb_strlen($label) + 4)
        ->setHeaderTitle($label)
        ->setHeaders([dt('Status'), dt('ID'), dt('Message')]);

      if ($this->width !== 9999) {
        $table
          ->setColumnWidth(0, max(0, $this->width))
          ->setColumnMaxWidth(0, max(0, $this->width));
      }

      foreach ($rows as $row) {
        $table->addRow([
          $row->getStatus()->labelConsole(),
          $row->getId(),
          $row->message(),
        ]);
      }

      $output->writeln('');
      $table->render();
      $output->writeln('');
    }
  }

}
