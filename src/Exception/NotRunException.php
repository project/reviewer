<?php

declare(strict_types=1);

namespace Drupal\reviewer\Exception;

/**
 * Thrown when an item's results are accessed before the item is run.
 */
class NotRunException extends \Exception {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    string $id,
    int $code = 0,
    ?\Throwable $previous = NULL,
  ) {
    parent::__construct(
      "$id must be run before results can be accessed.",
      $code,
      $previous,
    );
  }

}
