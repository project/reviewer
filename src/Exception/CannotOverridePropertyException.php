<?php

declare(strict_types=1);

namespace Drupal\reviewer\Exception;

/**
 * Thrown when a value is assigned to a property which already has a value.
 */
class CannotOverridePropertyException extends \Exception {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    string $property = '',
    string $class = '',
    int $code = 0,
    \Throwable|null $previous = NULL,
  ) {
    parent::__construct(
      "The property $$property of $class has a value and cannot be overwritten.",
      $code,
      $previous,
    );
  }

}
