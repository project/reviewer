<?php

declare(strict_types=1);

namespace Drupal\reviewer\Exception;

/**
 * Thrown when a review is run without checklists being defined.
 */
class NoChecklistsException extends \Exception {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    string $plugin_id,
    int $code = 0,
    ?\Throwable $previous = NULL,
  ) {
    parent::__construct(
      "$plugin_id cannot be run without checklists defined.",
      $code,
      $previous,
    );
  }

}
