<?php

declare(strict_types=1);

namespace Drupal\reviewer\Exception;

/**
 * Thrown when trying to retrieve ignored data for result which is not ignored.
 */
class NotIgnoredException extends \Exception {

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    string $id,
    int $code = 0,
    ?\Throwable $previous = NULL,
  ) {
    parent::__construct(
      "$id is not ignored.",
      $code,
      $previous,
    );
  }

}
