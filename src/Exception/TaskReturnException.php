<?php

declare(strict_types=1);

namespace Drupal\reviewer\Exception;

/**
 * Thrown when the return value of a task is not an invalid value.
 */
class TaskReturnException extends \Exception {

}
