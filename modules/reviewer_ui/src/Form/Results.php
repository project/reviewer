<?php

declare(strict_types=1);

namespace Drupal\reviewer_ui\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reviewer\Reviewer\IgnorerInterface;
use Drupal\reviewer\Reviewer\Result\ResultInterface;
use Drupal\reviewer\Reviewer\RunnerInterface;
use Drupal\reviewer\Reviewer\Status\Status;
use Drupal\reviewer\Reviewer\Status\StatusEvaluatorInterface;

/**
 * Form for viewing and ignoring results.
 */
final class Results extends FormBase {

  use AutowireTrait;

  // phpcs:ignore Drupal.Commenting.FunctionComment.Missing
  public function __construct(
    private readonly IgnorerInterface $ignorer,
    private readonly RunnerInterface $runner,
    private readonly StatusEvaluatorInterface $evaluator,
  ) {}

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFormId(): string {
    return 'reviewer_ui_reviews';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\reviewer\Exception\CannotOverridePropertyException
   * @throws \Drupal\reviewer\Exception\NoChecklistsException
   * @throws \Drupal\reviewer\Exception\NotIgnoredException
   * @throws \Drupal\reviewer\Exception\NotRunException
   */
  #[\Override]
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
  ): array {
    $form = [
      'filters' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Filter Results'),
        'show' => [
          '#type' => 'checkboxes',
          '#title' => $this->t('Show:'),
          '#options' => [
            'all' => $this->t('All Results'),
            $this->stateFromStatus(Status::Fail) => $this->t('Failed Results'),
            $this->stateFromStatus(Status::IgnoredFailure) => $this->t('Ignored Results'),
            $this->stateFromStatus(Status::Pass) => $this->t('Passed Results'),
            $this->stateFromStatus(Status::NotRun) => $this->t('Not Run'),
          ],
          '#default_value' => array_values(array_filter(
            (array) $this->getRequest()->getSession()->get('reviewer_ui.show', []),
            fn(mixed $value) => !\is_null($value),
          )) ?: ['failed'],
        ],
      ],
      '#attached' => [
        'library' => [
          'core/drupal.states',
          'reviewer_ui/admin',
        ],
      ],
    ];

    foreach ($this->runner->runPlugins() as $review) {
      $form[$review->getId()] = [
        '#type' => 'details',
        '#title' => "<span>{$review->result()->getStatus()->label()}</span> {$review->getLabel()}",
        '#attributes' => [
          'class' => [
            'js-form-wrapper',
            'reviewer-details',
            "reviewer--{$this->classFromResult($review->result())}",
          ],
        ],
        '#summary_attributes' => [
          'class' => [
            'reviewer-summary',
          ],
        ],
      ];

      $contained_statuses = [];
      foreach ($review->results()->individualResults() as $result) {
        if (!\in_array($result->getStatus(), $contained_statuses, TRUE)) {
          $contained_statuses[] = $result->getStatus();
        }
      }
      $states = [];
      foreach ($contained_statuses as $contained_status) {
        $states[] = 'or';
        $states[] = ["[name='show[{$this->stateFromStatus($contained_status)}]']" => ['checked' => TRUE]];
      }
      array_shift($states);
      // Ignore false positive.
      // @see https://github.com/phpstan/phpstan/issues/9691
      // @phpstan-ignore-next-line
      $form[$review->getId()]['#attributes']['data-drupal-states'] = Json::encode([
        'visible' => $states,
      ]);

      $form[$review->getId()][] = [
        '#type' => 'table',
        '#header' => [
          'status' => $this->t('Status'),
          'id' => $this->t('ID'),
          'message' => $this->t('Failure Message'),
          'ignore' => $this->t('Ignore Result'),
        ],
        '#rows' => $this->createRows($review->results()->individualResults()),
        '#attributes' => [
          'class' => [
            'reviewer-results-table',
          ],
        ],
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
      'ignore' => [
        '#type' => 'submit',
        '#button_type' => 'primary',
        '#value' => $this->t('Update Ignored'),
        '#name' => 'ignore',
      ],
      'unignore_all' => [
        '#type' => 'submit',
        '#button_type' => 'danger',
        '#value' => $this->t('Unignore All'),
        '#name' => 'unignore_all',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    $form_state->cleanValues();

    if ($show = $form_state->getUserInput()['show'] ?? []) {
      $this->getRequest()->getSession()->set('reviewer_ui.show', $show);
    }

    $trigger = $form_state->getTriggeringElement()['#name'] ?? '';
    if ($trigger === 'ignore') {
      $reasons = $form_state->getUserInput()['reasons'] ?? [];
      $ignored = [];
      foreach ($form_state->getUserInput()['ignored'] ?? [] as $hash => $value) {
        $id = base64_decode($hash);
        $ignored[] = $id;
        $this->ignorer->ignore($id, $reasons[$hash]);
      }

      // Checkboxes only submit values when they are checked, so get all the
      // config ignored result IDs and filter them against the submitted IDs to
      // figure out what ignored results should be removed.
      $unignored = array_map(
        fn(array $unignore) => $unignore['id'],
        $this->ignorer->getConfigIgnored(),
      );
      $unignored = array_filter(
        $unignored,
        fn(string $id) => !\in_array($id, $ignored, TRUE),
      );
      $this->ignorer->unignore($unignored);
    }

    if ($trigger === 'unignore_all') {
      $this->ignorer->unignoreAll();
    }
  }

  /**
   * Generate table rows for a result.
   *
   * @param \Drupal\reviewer\Reviewer\Result\ResultInterface[] $results
   *
   * @return mixed[]
   *
   * @throws \Drupal\reviewer\Exception\NotIgnoredException
   */
  private function createRows(array $results): array {
    $rows = [];

    foreach ($results as $result) {
      $id = base64_encode($result->getId());
      $state = $this->stateFromResult($result);

      $rows[$result->getId()] = [
        'class' => [
          'reviewer-result',
          "reviewer--{$this->classFromResult($result)}",
          'js-form-wrapper',
        ],
        'data-reviewer-status' => $this->classFromResult($result),
        'data-drupal-states' => Json::encode([
          'visible' => [
            ["[name='show[$state]']" => ['checked' => TRUE]],
          ],
        ]),
        'data' => [
          'status' => [
            'data' => [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#value' => $result->getStatus()->label(),
            ],
          ],
          'id' => $result->getId(),
          'message' => $result->getOriginalMessage(),
          'ignore' => '',
        ],
      ];

      if (
        !$this->evaluator->isPass($result->getStatus())
        && !$this->evaluator->isNotRun($result->getStatus())
      ) {
        $rows[$result->getId()]['data']['ignore'] = [
          'data' => [
            '#type' => 'checkbox',
            '#name' => "ignored[$id]",
            '#title' => $this->t('Ignore'),
            '#title_display' => 'invisible',
            '#attributes' => [
              'checked' => $this->ignorer->isIgnored($result),
              'disabled' => $this->ignorer->isPluginIgnored($result),
            ],
          ],
        ];

        $rows["{$result->getId()}_reason"] = [
          'class' => [
            'js-form-wrapper',
          ],
          'data-drupal-states' => Json::encode([
            'visible' => [
              ["[name='show[$state]']" => ['checked' => TRUE]],
            ],
          ]),
          'data' => [
            'reason' => [
              'data' => [
                '#type' => 'textfield',
                '#name' => "reasons[$id]",
                '#title' => $this->t('Reason'),
                '#maxlength' => 255,
                '#value' => $this->ignorer->isIgnored($result)
                  ? $this->ignorer->ignoredReason($result)
                  : '',
                '#attributes' => [
                  'disabled' => $this->ignorer->isPluginIgnored($result),
                ],
              ],
              'colspan' => 4,
              'class' => [
                'js-form-wrapper',
              ],
              'data-drupal-states' => Json::encode([
                'visible' => [
                  "[name='ignored[$id]']" => ['checked' => TRUE],
                ],
              ]),
            ],
          ],
        ];
      }
    }

    return $rows;
  }

  /**
   * Get a class string from a result.
   */
  private function classFromResult(ResultInterface $result): string {
    return Html::cleanCssIdentifier(match ($result->getStatus()) {
      Status::NotRun => 'not-run',
      Status::Pass => 'pass',
      Status::IgnoredFailure => 'ignored-failure',
      Status::IgnoredError => 'ignored-error',
      Status::Fail => 'fail',
      Status::Error => 'error',
    });
  }

  /**
   * Get a class string from a state.
   */
  private function stateFromResult(ResultInterface $result): string {
    return $this->stateFromStatus($result->getStatus());
  }

  /**
   * Get the state string from a status.
   */
  private function stateFromStatus(Status $status): string {
    return match ($status) {
      Status::NotRun => 'not-run',
      Status::IgnoredFailure, Status::IgnoredError => 'ignored',
      Status::Fail, Status::Error => 'failed',
      default => 'passed',
    };
  }

}
