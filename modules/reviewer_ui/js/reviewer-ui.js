/**
 * @file
 * JavaScript for the Reviewer admin UI.
 */

'use strict';

((Drupal, once) => {
  /**
   * Check if all statuses are selected.
   *
   * @param {HTMLInputElement[]} statuses
   *
   * @return {Boolean}
   */
  function allStatusesSelected(statuses) {
    return (
      Array.prototype.slice
        .call(statuses)
        .filter((status) => status.checked === true).length === statuses.length
    );
  }

  /**
   * Update a status checkboxes to the specified checked state.
   *
   * @param {HTMLInputElement[]} checkboxes
   * @param {Boolean} checkedState
   */
  function updateStatuses(checkboxes, checkedState) {
    checkboxes.forEach((checkbox) => {
      if (checkbox.checked !== checkedState) {
        checkbox.click();
      }
    });
  }

  /**
   * Behavior for the reviewer page results filter checkboxes.
   */
  Drupal.behaviors.reviewerUiResultsFilter = {
    attach: (context) => {
      const all = once(
        'reviewer-ui-show-all',
        '[name="show[all]"]',
        context,
      ).shift();
      /** @var {HTMLInputElement[]} statuses */
      const statuses = once(
        'data-reviewer-ui-show-subset',
        '[name^="show"]:not([name="show[all]"])',
        context,
      );

      if (!all || !statuses) {
        return;
      }

      all.addEventListener('click', (event) => {
        // All was checked, select all statuses.
        if (all.checked) {
          updateStatuses(statuses, true);
        }
        // All was unchecked and every status is selected. Clear all statuses.
        else if (allStatusesSelected(statuses)) {
          updateStatuses(statuses, false);
        }
        // All was unchecked and not every status is selected. Select all
        // statuses.
        else {
          event.preventDefault();
          updateStatuses(statuses, true);
        }
      });

      // Select or deselect all when checking a status.
      statuses.forEach((statusCheckbox) => {
        statusCheckbox.addEventListener('click', () => {
          all.checked = allStatusesSelected(statuses);
        });
      });
    },
  };

  /**
   * Behavior to focus the reason field when ignore is clicked.
   */
  Drupal.behaviors.reviewerUiIgnoreFocus = {
    attach: (context) => {
      once('reviewer-ui-ignore-focus', '[name^="ignore"]', context).forEach(
        (ignore) => {
          const id = ignore.getAttribute('name').replace(/ignored\[|]/g, '');
          const reason = context.querySelector(`[name="reasons[${id}]"]`);
          if (reason && !reason.disabled) {
            ignore.addEventListener('click', () => {
              if (ignore.checked) {
                // A delay is necessary so that Drupal.states evaluation is run
                // and the reason input is visible and focusable before focusing.
                setTimeout(() => reason.focus(), 1);
              }
            });
          }
        },
      );
    },
  };
})(Drupal, once);
